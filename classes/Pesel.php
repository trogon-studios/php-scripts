<?php
class Pesel{
	private $number;
	private $birthdate;
	private $sex;
	private $isValid;

	public function __construct($peselNumber){
		$this->number = strval($peselNumber);
	}

	public function getBirthdate(){
		if(empty($this->birthdate)){
			$day = intval(substr($this->number, 4, 2));
			$month = intval(substr($this->number, 2, 2));
			$monthsign = intval($month / 20);
			$month %= 20;
			$year = substr($this->number, 0, 2);
			
			$now = new \DateTime("now");
			$yearbase = intval(substr($now->format("Y"), 0, 2));
			$yearsign = $yearbase % 5 + 1;
			
			$yearparts = array(
				"-2" => ($yearsign+5-2)%5,
				"-1" => ($yearsign+5-1)%5,
				"0" => $yearsign,
				"1" => ($yearsign+1)%5,
				"2" => ($yearsign+2)%5,
			);
			
			$yearparts = array_flip($yearparts);

			$yearbase = $yearbase + $yearparts[$monthsign];
			
			$year = intval($yearbase . $year);
			
			if(checkdate($month, $day, $year)){
				$this->birthdate = new \DateTime($year . "-" . $month . "-" . $day);
			}
		}
		return $this->birthdate;
	}

	public function getSex(){
		if(empty($this->sex)){
			$this->sex = ($this->number[9] % 2) == 1;
		}
		return $this->sex;
	}

	public function isValid(){
		if(empty($this->isValid)){
			$p = $this->number;
			$hash = intval($p[0])+3*intval($p[1])+7*intval($p[2])+9*intval($p[3])+intval($p[4])+3*intval($p[5])+7*intval($p[6])+9*intval($p[7])+intval($p[8])+3*intval($p[9]);
			$this->isValid = (10-($hash%10))%10===intval($p[10]);
		}
		return $this->isValid;
		
	}

	public function __toString(){
		return $this->pesel;
	}
}