<?php


/**
 * Base class that represents a query for the 'bsfusn_news_cats' table.
 *
 *
 *
 * @method BsfusnNewsCatsQuery orderByNewsCatId($order = Criteria::ASC) Order by the news_cat_id column
 * @method BsfusnNewsCatsQuery orderByNewsCatName($order = Criteria::ASC) Order by the news_cat_name column
 * @method BsfusnNewsCatsQuery orderByNewsCatImage($order = Criteria::ASC) Order by the news_cat_image column
 *
 * @method BsfusnNewsCatsQuery groupByNewsCatId() Group by the news_cat_id column
 * @method BsfusnNewsCatsQuery groupByNewsCatName() Group by the news_cat_name column
 * @method BsfusnNewsCatsQuery groupByNewsCatImage() Group by the news_cat_image column
 *
 * @method BsfusnNewsCatsQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method BsfusnNewsCatsQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method BsfusnNewsCatsQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method BsfusnNewsCats findOne(PropelPDO $con = null) Return the first BsfusnNewsCats matching the query
 * @method BsfusnNewsCats findOneOrCreate(PropelPDO $con = null) Return the first BsfusnNewsCats matching the query, or a new BsfusnNewsCats object populated from the query conditions when no match is found
 *
 * @method BsfusnNewsCats findOneByNewsCatName(string $news_cat_name) Return the first BsfusnNewsCats filtered by the news_cat_name column
 * @method BsfusnNewsCats findOneByNewsCatImage(string $news_cat_image) Return the first BsfusnNewsCats filtered by the news_cat_image column
 *
 * @method array findByNewsCatId(int $news_cat_id) Return BsfusnNewsCats objects filtered by the news_cat_id column
 * @method array findByNewsCatName(string $news_cat_name) Return BsfusnNewsCats objects filtered by the news_cat_name column
 * @method array findByNewsCatImage(string $news_cat_image) Return BsfusnNewsCats objects filtered by the news_cat_image column
 *
 * @package    propel.generator.bookstore.om
 */
abstract class BaseBsfusnNewsCatsQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseBsfusnNewsCatsQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'bookstore';
        }
        if (null === $modelName) {
            $modelName = 'BsfusnNewsCats';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new BsfusnNewsCatsQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   BsfusnNewsCatsQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return BsfusnNewsCatsQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof BsfusnNewsCatsQuery) {
            return $criteria;
        }
        $query = new BsfusnNewsCatsQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   BsfusnNewsCats|BsfusnNewsCats[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = BsfusnNewsCatsPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(BsfusnNewsCatsPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 BsfusnNewsCats A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByNewsCatId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 BsfusnNewsCats A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `news_cat_id`, `news_cat_name`, `news_cat_image` FROM `bsfusn_news_cats` WHERE `news_cat_id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new BsfusnNewsCats();
            $obj->hydrate($row);
            BsfusnNewsCatsPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return BsfusnNewsCats|BsfusnNewsCats[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|BsfusnNewsCats[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return BsfusnNewsCatsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(BsfusnNewsCatsPeer::NEWS_CAT_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return BsfusnNewsCatsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(BsfusnNewsCatsPeer::NEWS_CAT_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the news_cat_id column
     *
     * Example usage:
     * <code>
     * $query->filterByNewsCatId(1234); // WHERE news_cat_id = 1234
     * $query->filterByNewsCatId(array(12, 34)); // WHERE news_cat_id IN (12, 34)
     * $query->filterByNewsCatId(array('min' => 12)); // WHERE news_cat_id >= 12
     * $query->filterByNewsCatId(array('max' => 12)); // WHERE news_cat_id <= 12
     * </code>
     *
     * @param     mixed $newsCatId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BsfusnNewsCatsQuery The current query, for fluid interface
     */
    public function filterByNewsCatId($newsCatId = null, $comparison = null)
    {
        if (is_array($newsCatId)) {
            $useMinMax = false;
            if (isset($newsCatId['min'])) {
                $this->addUsingAlias(BsfusnNewsCatsPeer::NEWS_CAT_ID, $newsCatId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($newsCatId['max'])) {
                $this->addUsingAlias(BsfusnNewsCatsPeer::NEWS_CAT_ID, $newsCatId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BsfusnNewsCatsPeer::NEWS_CAT_ID, $newsCatId, $comparison);
    }

    /**
     * Filter the query on the news_cat_name column
     *
     * Example usage:
     * <code>
     * $query->filterByNewsCatName('fooValue');   // WHERE news_cat_name = 'fooValue'
     * $query->filterByNewsCatName('%fooValue%'); // WHERE news_cat_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $newsCatName The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BsfusnNewsCatsQuery The current query, for fluid interface
     */
    public function filterByNewsCatName($newsCatName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($newsCatName)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $newsCatName)) {
                $newsCatName = str_replace('*', '%', $newsCatName);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(BsfusnNewsCatsPeer::NEWS_CAT_NAME, $newsCatName, $comparison);
    }

    /**
     * Filter the query on the news_cat_image column
     *
     * Example usage:
     * <code>
     * $query->filterByNewsCatImage('fooValue');   // WHERE news_cat_image = 'fooValue'
     * $query->filterByNewsCatImage('%fooValue%'); // WHERE news_cat_image LIKE '%fooValue%'
     * </code>
     *
     * @param     string $newsCatImage The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BsfusnNewsCatsQuery The current query, for fluid interface
     */
    public function filterByNewsCatImage($newsCatImage = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($newsCatImage)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $newsCatImage)) {
                $newsCatImage = str_replace('*', '%', $newsCatImage);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(BsfusnNewsCatsPeer::NEWS_CAT_IMAGE, $newsCatImage, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   BsfusnNewsCats $bsfusnNewsCats Object to remove from the list of results
     *
     * @return BsfusnNewsCatsQuery The current query, for fluid interface
     */
    public function prune($bsfusnNewsCats = null)
    {
        if ($bsfusnNewsCats) {
            $this->addUsingAlias(BsfusnNewsCatsPeer::NEWS_CAT_ID, $bsfusnNewsCats->getNewsCatId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
