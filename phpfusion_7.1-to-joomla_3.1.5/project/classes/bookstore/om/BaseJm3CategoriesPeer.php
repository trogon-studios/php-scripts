<?php


/**
 * Base static class for performing query and update operations on the 'jm3_categories' table.
 *
 *
 *
 * @package propel.generator.bookstore.om
 */
abstract class BaseJm3CategoriesPeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'bookstore';

    /** the table name for this class */
    const TABLE_NAME = 'jm3_categories';

    /** the related Propel class for this table */
    const OM_CLASS = 'Jm3Categories';

    /** the related TableMap class for this table */
    const TM_CLASS = 'Jm3CategoriesTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 27;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 27;

    /** the column name for the id field */
    const ID = 'jm3_categories.id';

    /** the column name for the asset_id field */
    const ASSET_ID = 'jm3_categories.asset_id';

    /** the column name for the parent_id field */
    const PARENT_ID = 'jm3_categories.parent_id';

    /** the column name for the lft field */
    const LFT = 'jm3_categories.lft';

    /** the column name for the rgt field */
    const RGT = 'jm3_categories.rgt';

    /** the column name for the level field */
    const LEVEL = 'jm3_categories.level';

    /** the column name for the path field */
    const PATH = 'jm3_categories.path';

    /** the column name for the extension field */
    const EXTENSION = 'jm3_categories.extension';

    /** the column name for the title field */
    const TITLE = 'jm3_categories.title';

    /** the column name for the alias field */
    const ALIAS = 'jm3_categories.alias';

    /** the column name for the note field */
    const NOTE = 'jm3_categories.note';

    /** the column name for the description field */
    const DESCRIPTION = 'jm3_categories.description';

    /** the column name for the published field */
    const PUBLISHED = 'jm3_categories.published';

    /** the column name for the checked_out field */
    const CHECKED_OUT = 'jm3_categories.checked_out';

    /** the column name for the checked_out_time field */
    const CHECKED_OUT_TIME = 'jm3_categories.checked_out_time';

    /** the column name for the access field */
    const ACCESS = 'jm3_categories.access';

    /** the column name for the params field */
    const PARAMS = 'jm3_categories.params';

    /** the column name for the metadesc field */
    const METADESC = 'jm3_categories.metadesc';

    /** the column name for the metakey field */
    const METAKEY = 'jm3_categories.metakey';

    /** the column name for the metadata field */
    const METADATA = 'jm3_categories.metadata';

    /** the column name for the created_user_id field */
    const CREATED_USER_ID = 'jm3_categories.created_user_id';

    /** the column name for the created_time field */
    const CREATED_TIME = 'jm3_categories.created_time';

    /** the column name for the modified_user_id field */
    const MODIFIED_USER_ID = 'jm3_categories.modified_user_id';

    /** the column name for the modified_time field */
    const MODIFIED_TIME = 'jm3_categories.modified_time';

    /** the column name for the hits field */
    const HITS = 'jm3_categories.hits';

    /** the column name for the language field */
    const LANGUAGE = 'jm3_categories.language';

    /** the column name for the version field */
    const VERSION = 'jm3_categories.version';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identity map to hold any loaded instances of Jm3Categories objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array Jm3Categories[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. Jm3CategoriesPeer::$fieldNames[Jm3CategoriesPeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('Id', 'AssetId', 'ParentId', 'Lft', 'Rgt', 'Level', 'Path', 'Extension', 'Title', 'Alias', 'Note', 'Description', 'Published', 'CheckedOut', 'CheckedOutTime', 'Access', 'Params', 'Metadesc', 'Metakey', 'Metadata', 'CreatedUserId', 'CreatedTime', 'ModifiedUserId', 'ModifiedTime', 'Hits', 'Language', 'Version', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id', 'assetId', 'parentId', 'lft', 'rgt', 'level', 'path', 'extension', 'title', 'alias', 'note', 'description', 'published', 'checkedOut', 'checkedOutTime', 'access', 'params', 'metadesc', 'metakey', 'metadata', 'createdUserId', 'createdTime', 'modifiedUserId', 'modifiedTime', 'hits', 'language', 'version', ),
        BasePeer::TYPE_COLNAME => array (Jm3CategoriesPeer::ID, Jm3CategoriesPeer::ASSET_ID, Jm3CategoriesPeer::PARENT_ID, Jm3CategoriesPeer::LFT, Jm3CategoriesPeer::RGT, Jm3CategoriesPeer::LEVEL, Jm3CategoriesPeer::PATH, Jm3CategoriesPeer::EXTENSION, Jm3CategoriesPeer::TITLE, Jm3CategoriesPeer::ALIAS, Jm3CategoriesPeer::NOTE, Jm3CategoriesPeer::DESCRIPTION, Jm3CategoriesPeer::PUBLISHED, Jm3CategoriesPeer::CHECKED_OUT, Jm3CategoriesPeer::CHECKED_OUT_TIME, Jm3CategoriesPeer::ACCESS, Jm3CategoriesPeer::PARAMS, Jm3CategoriesPeer::METADESC, Jm3CategoriesPeer::METAKEY, Jm3CategoriesPeer::METADATA, Jm3CategoriesPeer::CREATED_USER_ID, Jm3CategoriesPeer::CREATED_TIME, Jm3CategoriesPeer::MODIFIED_USER_ID, Jm3CategoriesPeer::MODIFIED_TIME, Jm3CategoriesPeer::HITS, Jm3CategoriesPeer::LANGUAGE, Jm3CategoriesPeer::VERSION, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID', 'ASSET_ID', 'PARENT_ID', 'LFT', 'RGT', 'LEVEL', 'PATH', 'EXTENSION', 'TITLE', 'ALIAS', 'NOTE', 'DESCRIPTION', 'PUBLISHED', 'CHECKED_OUT', 'CHECKED_OUT_TIME', 'ACCESS', 'PARAMS', 'METADESC', 'METAKEY', 'METADATA', 'CREATED_USER_ID', 'CREATED_TIME', 'MODIFIED_USER_ID', 'MODIFIED_TIME', 'HITS', 'LANGUAGE', 'VERSION', ),
        BasePeer::TYPE_FIELDNAME => array ('id', 'asset_id', 'parent_id', 'lft', 'rgt', 'level', 'path', 'extension', 'title', 'alias', 'note', 'description', 'published', 'checked_out', 'checked_out_time', 'access', 'params', 'metadesc', 'metakey', 'metadata', 'created_user_id', 'created_time', 'modified_user_id', 'modified_time', 'hits', 'language', 'version', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. Jm3CategoriesPeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'AssetId' => 1, 'ParentId' => 2, 'Lft' => 3, 'Rgt' => 4, 'Level' => 5, 'Path' => 6, 'Extension' => 7, 'Title' => 8, 'Alias' => 9, 'Note' => 10, 'Description' => 11, 'Published' => 12, 'CheckedOut' => 13, 'CheckedOutTime' => 14, 'Access' => 15, 'Params' => 16, 'Metadesc' => 17, 'Metakey' => 18, 'Metadata' => 19, 'CreatedUserId' => 20, 'CreatedTime' => 21, 'ModifiedUserId' => 22, 'ModifiedTime' => 23, 'Hits' => 24, 'Language' => 25, 'Version' => 26, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id' => 0, 'assetId' => 1, 'parentId' => 2, 'lft' => 3, 'rgt' => 4, 'level' => 5, 'path' => 6, 'extension' => 7, 'title' => 8, 'alias' => 9, 'note' => 10, 'description' => 11, 'published' => 12, 'checkedOut' => 13, 'checkedOutTime' => 14, 'access' => 15, 'params' => 16, 'metadesc' => 17, 'metakey' => 18, 'metadata' => 19, 'createdUserId' => 20, 'createdTime' => 21, 'modifiedUserId' => 22, 'modifiedTime' => 23, 'hits' => 24, 'language' => 25, 'version' => 26, ),
        BasePeer::TYPE_COLNAME => array (Jm3CategoriesPeer::ID => 0, Jm3CategoriesPeer::ASSET_ID => 1, Jm3CategoriesPeer::PARENT_ID => 2, Jm3CategoriesPeer::LFT => 3, Jm3CategoriesPeer::RGT => 4, Jm3CategoriesPeer::LEVEL => 5, Jm3CategoriesPeer::PATH => 6, Jm3CategoriesPeer::EXTENSION => 7, Jm3CategoriesPeer::TITLE => 8, Jm3CategoriesPeer::ALIAS => 9, Jm3CategoriesPeer::NOTE => 10, Jm3CategoriesPeer::DESCRIPTION => 11, Jm3CategoriesPeer::PUBLISHED => 12, Jm3CategoriesPeer::CHECKED_OUT => 13, Jm3CategoriesPeer::CHECKED_OUT_TIME => 14, Jm3CategoriesPeer::ACCESS => 15, Jm3CategoriesPeer::PARAMS => 16, Jm3CategoriesPeer::METADESC => 17, Jm3CategoriesPeer::METAKEY => 18, Jm3CategoriesPeer::METADATA => 19, Jm3CategoriesPeer::CREATED_USER_ID => 20, Jm3CategoriesPeer::CREATED_TIME => 21, Jm3CategoriesPeer::MODIFIED_USER_ID => 22, Jm3CategoriesPeer::MODIFIED_TIME => 23, Jm3CategoriesPeer::HITS => 24, Jm3CategoriesPeer::LANGUAGE => 25, Jm3CategoriesPeer::VERSION => 26, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID' => 0, 'ASSET_ID' => 1, 'PARENT_ID' => 2, 'LFT' => 3, 'RGT' => 4, 'LEVEL' => 5, 'PATH' => 6, 'EXTENSION' => 7, 'TITLE' => 8, 'ALIAS' => 9, 'NOTE' => 10, 'DESCRIPTION' => 11, 'PUBLISHED' => 12, 'CHECKED_OUT' => 13, 'CHECKED_OUT_TIME' => 14, 'ACCESS' => 15, 'PARAMS' => 16, 'METADESC' => 17, 'METAKEY' => 18, 'METADATA' => 19, 'CREATED_USER_ID' => 20, 'CREATED_TIME' => 21, 'MODIFIED_USER_ID' => 22, 'MODIFIED_TIME' => 23, 'HITS' => 24, 'LANGUAGE' => 25, 'VERSION' => 26, ),
        BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'asset_id' => 1, 'parent_id' => 2, 'lft' => 3, 'rgt' => 4, 'level' => 5, 'path' => 6, 'extension' => 7, 'title' => 8, 'alias' => 9, 'note' => 10, 'description' => 11, 'published' => 12, 'checked_out' => 13, 'checked_out_time' => 14, 'access' => 15, 'params' => 16, 'metadesc' => 17, 'metakey' => 18, 'metadata' => 19, 'created_user_id' => 20, 'created_time' => 21, 'modified_user_id' => 22, 'modified_time' => 23, 'hits' => 24, 'language' => 25, 'version' => 26, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, )
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = Jm3CategoriesPeer::getFieldNames($toType);
        $key = isset(Jm3CategoriesPeer::$fieldKeys[$fromType][$name]) ? Jm3CategoriesPeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(Jm3CategoriesPeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, Jm3CategoriesPeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return Jm3CategoriesPeer::$fieldNames[$type];
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. Jm3CategoriesPeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(Jm3CategoriesPeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(Jm3CategoriesPeer::ID);
            $criteria->addSelectColumn(Jm3CategoriesPeer::ASSET_ID);
            $criteria->addSelectColumn(Jm3CategoriesPeer::PARENT_ID);
            $criteria->addSelectColumn(Jm3CategoriesPeer::LFT);
            $criteria->addSelectColumn(Jm3CategoriesPeer::RGT);
            $criteria->addSelectColumn(Jm3CategoriesPeer::LEVEL);
            $criteria->addSelectColumn(Jm3CategoriesPeer::PATH);
            $criteria->addSelectColumn(Jm3CategoriesPeer::EXTENSION);
            $criteria->addSelectColumn(Jm3CategoriesPeer::TITLE);
            $criteria->addSelectColumn(Jm3CategoriesPeer::ALIAS);
            $criteria->addSelectColumn(Jm3CategoriesPeer::NOTE);
            $criteria->addSelectColumn(Jm3CategoriesPeer::DESCRIPTION);
            $criteria->addSelectColumn(Jm3CategoriesPeer::PUBLISHED);
            $criteria->addSelectColumn(Jm3CategoriesPeer::CHECKED_OUT);
            $criteria->addSelectColumn(Jm3CategoriesPeer::CHECKED_OUT_TIME);
            $criteria->addSelectColumn(Jm3CategoriesPeer::ACCESS);
            $criteria->addSelectColumn(Jm3CategoriesPeer::PARAMS);
            $criteria->addSelectColumn(Jm3CategoriesPeer::METADESC);
            $criteria->addSelectColumn(Jm3CategoriesPeer::METAKEY);
            $criteria->addSelectColumn(Jm3CategoriesPeer::METADATA);
            $criteria->addSelectColumn(Jm3CategoriesPeer::CREATED_USER_ID);
            $criteria->addSelectColumn(Jm3CategoriesPeer::CREATED_TIME);
            $criteria->addSelectColumn(Jm3CategoriesPeer::MODIFIED_USER_ID);
            $criteria->addSelectColumn(Jm3CategoriesPeer::MODIFIED_TIME);
            $criteria->addSelectColumn(Jm3CategoriesPeer::HITS);
            $criteria->addSelectColumn(Jm3CategoriesPeer::LANGUAGE);
            $criteria->addSelectColumn(Jm3CategoriesPeer::VERSION);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.asset_id');
            $criteria->addSelectColumn($alias . '.parent_id');
            $criteria->addSelectColumn($alias . '.lft');
            $criteria->addSelectColumn($alias . '.rgt');
            $criteria->addSelectColumn($alias . '.level');
            $criteria->addSelectColumn($alias . '.path');
            $criteria->addSelectColumn($alias . '.extension');
            $criteria->addSelectColumn($alias . '.title');
            $criteria->addSelectColumn($alias . '.alias');
            $criteria->addSelectColumn($alias . '.note');
            $criteria->addSelectColumn($alias . '.description');
            $criteria->addSelectColumn($alias . '.published');
            $criteria->addSelectColumn($alias . '.checked_out');
            $criteria->addSelectColumn($alias . '.checked_out_time');
            $criteria->addSelectColumn($alias . '.access');
            $criteria->addSelectColumn($alias . '.params');
            $criteria->addSelectColumn($alias . '.metadesc');
            $criteria->addSelectColumn($alias . '.metakey');
            $criteria->addSelectColumn($alias . '.metadata');
            $criteria->addSelectColumn($alias . '.created_user_id');
            $criteria->addSelectColumn($alias . '.created_time');
            $criteria->addSelectColumn($alias . '.modified_user_id');
            $criteria->addSelectColumn($alias . '.modified_time');
            $criteria->addSelectColumn($alias . '.hits');
            $criteria->addSelectColumn($alias . '.language');
            $criteria->addSelectColumn($alias . '.version');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(Jm3CategoriesPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            Jm3CategoriesPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(Jm3CategoriesPeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(Jm3CategoriesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return Jm3Categories
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = Jm3CategoriesPeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return Jm3CategoriesPeer::populateObjects(Jm3CategoriesPeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(Jm3CategoriesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            Jm3CategoriesPeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(Jm3CategoriesPeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param Jm3Categories $obj A Jm3Categories object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getId();
            } // if key === null
            Jm3CategoriesPeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A Jm3Categories object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof Jm3Categories) {
                $key = (string) $value->getId();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or Jm3Categories object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(Jm3CategoriesPeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return Jm3Categories Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(Jm3CategoriesPeer::$instances[$key])) {
                return Jm3CategoriesPeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }

    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references) {
        foreach (Jm3CategoriesPeer::$instances as $instance) {
          $instance->clearAllReferences(true);
        }
      }
        Jm3CategoriesPeer::$instances = array();
    }

    /**
     * Method to invalidate the instance pool of all tables related to jm3_categories
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null) {
            return null;
        }

        return (string) $row[$startcol];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (int) $row[$startcol];
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = Jm3CategoriesPeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = Jm3CategoriesPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = Jm3CategoriesPeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                Jm3CategoriesPeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (Jm3Categories object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = Jm3CategoriesPeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = Jm3CategoriesPeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + Jm3CategoriesPeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = Jm3CategoriesPeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            Jm3CategoriesPeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(Jm3CategoriesPeer::DATABASE_NAME)->getTable(Jm3CategoriesPeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseJm3CategoriesPeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseJm3CategoriesPeer::TABLE_NAME)) {
        $dbMap->addTableObject(new Jm3CategoriesTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass($row = 0, $colnum = 0)
    {
        return Jm3CategoriesPeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a Jm3Categories or Criteria object.
     *
     * @param      mixed $values Criteria or Jm3Categories object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(Jm3CategoriesPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from Jm3Categories object
        }

        if ($criteria->containsKey(Jm3CategoriesPeer::ID) && $criteria->keyContainsValue(Jm3CategoriesPeer::ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.Jm3CategoriesPeer::ID.')');
        }


        // Set the correct dbName
        $criteria->setDbName(Jm3CategoriesPeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a Jm3Categories or Criteria object.
     *
     * @param      mixed $values Criteria or Jm3Categories object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(Jm3CategoriesPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(Jm3CategoriesPeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(Jm3CategoriesPeer::ID);
            $value = $criteria->remove(Jm3CategoriesPeer::ID);
            if ($value) {
                $selectCriteria->add(Jm3CategoriesPeer::ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(Jm3CategoriesPeer::TABLE_NAME);
            }

        } else { // $values is Jm3Categories object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(Jm3CategoriesPeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the jm3_categories table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(Jm3CategoriesPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(Jm3CategoriesPeer::TABLE_NAME, $con, Jm3CategoriesPeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            Jm3CategoriesPeer::clearInstancePool();
            Jm3CategoriesPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a Jm3Categories or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or Jm3Categories object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(Jm3CategoriesPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            Jm3CategoriesPeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof Jm3Categories) { // it's a model object
            // invalidate the cache for this single object
            Jm3CategoriesPeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(Jm3CategoriesPeer::DATABASE_NAME);
            $criteria->add(Jm3CategoriesPeer::ID, (array) $values, Criteria::IN);
            // invalidate the cache for this object(s)
            foreach ((array) $values as $singleval) {
                Jm3CategoriesPeer::removeInstanceFromPool($singleval);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(Jm3CategoriesPeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();

            $affectedRows += BasePeer::doDelete($criteria, $con);
            Jm3CategoriesPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given Jm3Categories object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param Jm3Categories $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(Jm3CategoriesPeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(Jm3CategoriesPeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(Jm3CategoriesPeer::DATABASE_NAME, Jm3CategoriesPeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param int $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return Jm3Categories
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = Jm3CategoriesPeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(Jm3CategoriesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(Jm3CategoriesPeer::DATABASE_NAME);
        $criteria->add(Jm3CategoriesPeer::ID, $pk);

        $v = Jm3CategoriesPeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return Jm3Categories[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(Jm3CategoriesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(Jm3CategoriesPeer::DATABASE_NAME);
            $criteria->add(Jm3CategoriesPeer::ID, $pks, Criteria::IN);
            $objs = Jm3CategoriesPeer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BaseJm3CategoriesPeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseJm3CategoriesPeer::buildTableMap();

