<?php


/**
 * Base class that represents a query for the 'api_sessions' table.
 *
 *
 *
 * @method ApiSessionsQuery orderById($order = Criteria::ASC) Order by the id column
 * @method ApiSessionsQuery orderByCreated($order = Criteria::ASC) Order by the created column
 * @method ApiSessionsQuery orderByExpirience($order = Criteria::ASC) Order by the expirience column
 * @method ApiSessionsQuery orderBySessionUserid($order = Criteria::ASC) Order by the session_userId column
 *
 * @method ApiSessionsQuery groupById() Group by the id column
 * @method ApiSessionsQuery groupByCreated() Group by the created column
 * @method ApiSessionsQuery groupByExpirience() Group by the expirience column
 * @method ApiSessionsQuery groupBySessionUserid() Group by the session_userId column
 *
 * @method ApiSessionsQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method ApiSessionsQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method ApiSessionsQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method ApiSessions findOne(PropelPDO $con = null) Return the first ApiSessions matching the query
 * @method ApiSessions findOneOrCreate(PropelPDO $con = null) Return the first ApiSessions matching the query, or a new ApiSessions object populated from the query conditions when no match is found
 *
 * @method ApiSessions findOneByCreated(string $created) Return the first ApiSessions filtered by the created column
 * @method ApiSessions findOneByExpirience(string $expirience) Return the first ApiSessions filtered by the expirience column
 * @method ApiSessions findOneBySessionUserid(string $session_userId) Return the first ApiSessions filtered by the session_userId column
 *
 * @method array findById(string $id) Return ApiSessions objects filtered by the id column
 * @method array findByCreated(string $created) Return ApiSessions objects filtered by the created column
 * @method array findByExpirience(string $expirience) Return ApiSessions objects filtered by the expirience column
 * @method array findBySessionUserid(string $session_userId) Return ApiSessions objects filtered by the session_userId column
 *
 * @package    propel.generator.bookstore.om
 */
abstract class BaseApiSessionsQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseApiSessionsQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'bookstore';
        }
        if (null === $modelName) {
            $modelName = 'ApiSessions';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ApiSessionsQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   ApiSessionsQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return ApiSessionsQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof ApiSessionsQuery) {
            return $criteria;
        }
        $query = new ApiSessionsQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   ApiSessions|ApiSessions[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = ApiSessionsPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(ApiSessionsPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 ApiSessions A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 ApiSessions A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `created`, `expirience`, `session_userId` FROM `api_sessions` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new ApiSessions();
            $obj->hydrate($row);
            ApiSessionsPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return ApiSessions|ApiSessions[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|ApiSessions[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return ApiSessionsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ApiSessionsPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return ApiSessionsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ApiSessionsPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiSessionsQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ApiSessionsPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ApiSessionsPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiSessionsPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the created column
     *
     * Example usage:
     * <code>
     * $query->filterByCreated('2011-03-14'); // WHERE created = '2011-03-14'
     * $query->filterByCreated('now'); // WHERE created = '2011-03-14'
     * $query->filterByCreated(array('max' => 'yesterday')); // WHERE created < '2011-03-13'
     * </code>
     *
     * @param     mixed $created The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiSessionsQuery The current query, for fluid interface
     */
    public function filterByCreated($created = null, $comparison = null)
    {
        if (is_array($created)) {
            $useMinMax = false;
            if (isset($created['min'])) {
                $this->addUsingAlias(ApiSessionsPeer::CREATED, $created['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($created['max'])) {
                $this->addUsingAlias(ApiSessionsPeer::CREATED, $created['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiSessionsPeer::CREATED, $created, $comparison);
    }

    /**
     * Filter the query on the expirience column
     *
     * Example usage:
     * <code>
     * $query->filterByExpirience('2011-03-14'); // WHERE expirience = '2011-03-14'
     * $query->filterByExpirience('now'); // WHERE expirience = '2011-03-14'
     * $query->filterByExpirience(array('max' => 'yesterday')); // WHERE expirience < '2011-03-13'
     * </code>
     *
     * @param     mixed $expirience The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiSessionsQuery The current query, for fluid interface
     */
    public function filterByExpirience($expirience = null, $comparison = null)
    {
        if (is_array($expirience)) {
            $useMinMax = false;
            if (isset($expirience['min'])) {
                $this->addUsingAlias(ApiSessionsPeer::EXPIRIENCE, $expirience['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($expirience['max'])) {
                $this->addUsingAlias(ApiSessionsPeer::EXPIRIENCE, $expirience['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiSessionsPeer::EXPIRIENCE, $expirience, $comparison);
    }

    /**
     * Filter the query on the session_userId column
     *
     * Example usage:
     * <code>
     * $query->filterBySessionUserid(1234); // WHERE session_userId = 1234
     * $query->filterBySessionUserid(array(12, 34)); // WHERE session_userId IN (12, 34)
     * $query->filterBySessionUserid(array('min' => 12)); // WHERE session_userId >= 12
     * $query->filterBySessionUserid(array('max' => 12)); // WHERE session_userId <= 12
     * </code>
     *
     * @param     mixed $sessionUserid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiSessionsQuery The current query, for fluid interface
     */
    public function filterBySessionUserid($sessionUserid = null, $comparison = null)
    {
        if (is_array($sessionUserid)) {
            $useMinMax = false;
            if (isset($sessionUserid['min'])) {
                $this->addUsingAlias(ApiSessionsPeer::SESSION_USERID, $sessionUserid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($sessionUserid['max'])) {
                $this->addUsingAlias(ApiSessionsPeer::SESSION_USERID, $sessionUserid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiSessionsPeer::SESSION_USERID, $sessionUserid, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ApiSessions $apiSessions Object to remove from the list of results
     *
     * @return ApiSessionsQuery The current query, for fluid interface
     */
    public function prune($apiSessions = null)
    {
        if ($apiSessions) {
            $this->addUsingAlias(ApiSessionsPeer::ID, $apiSessions->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
