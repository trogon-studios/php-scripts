<?php


/**
 * Base class that represents a query for the 'api_units' table.
 *
 *
 *
 * @method ApiUnitsQuery orderById($order = Criteria::ASC) Order by the id column
 * @method ApiUnitsQuery orderByHealth($order = Criteria::ASC) Order by the health column
 * @method ApiUnitsQuery orderByUnitUnittype($order = Criteria::ASC) Order by the unit_unitType column
 *
 * @method ApiUnitsQuery groupById() Group by the id column
 * @method ApiUnitsQuery groupByHealth() Group by the health column
 * @method ApiUnitsQuery groupByUnitUnittype() Group by the unit_unitType column
 *
 * @method ApiUnitsQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method ApiUnitsQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method ApiUnitsQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method ApiUnits findOne(PropelPDO $con = null) Return the first ApiUnits matching the query
 * @method ApiUnits findOneOrCreate(PropelPDO $con = null) Return the first ApiUnits matching the query, or a new ApiUnits object populated from the query conditions when no match is found
 *
 * @method ApiUnits findOneByHealth(int $health) Return the first ApiUnits filtered by the health column
 * @method ApiUnits findOneByUnitUnittype(int $unit_unitType) Return the first ApiUnits filtered by the unit_unitType column
 *
 * @method array findById(string $id) Return ApiUnits objects filtered by the id column
 * @method array findByHealth(int $health) Return ApiUnits objects filtered by the health column
 * @method array findByUnitUnittype(int $unit_unitType) Return ApiUnits objects filtered by the unit_unitType column
 *
 * @package    propel.generator.bookstore.om
 */
abstract class BaseApiUnitsQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseApiUnitsQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'bookstore';
        }
        if (null === $modelName) {
            $modelName = 'ApiUnits';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ApiUnitsQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   ApiUnitsQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return ApiUnitsQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof ApiUnitsQuery) {
            return $criteria;
        }
        $query = new ApiUnitsQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   ApiUnits|ApiUnits[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = ApiUnitsPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(ApiUnitsPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 ApiUnits A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 ApiUnits A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `health`, `unit_unitType` FROM `api_units` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new ApiUnits();
            $obj->hydrate($row);
            ApiUnitsPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return ApiUnits|ApiUnits[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|ApiUnits[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return ApiUnitsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ApiUnitsPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return ApiUnitsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ApiUnitsPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiUnitsQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ApiUnitsPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ApiUnitsPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiUnitsPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the health column
     *
     * Example usage:
     * <code>
     * $query->filterByHealth(1234); // WHERE health = 1234
     * $query->filterByHealth(array(12, 34)); // WHERE health IN (12, 34)
     * $query->filterByHealth(array('min' => 12)); // WHERE health >= 12
     * $query->filterByHealth(array('max' => 12)); // WHERE health <= 12
     * </code>
     *
     * @param     mixed $health The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiUnitsQuery The current query, for fluid interface
     */
    public function filterByHealth($health = null, $comparison = null)
    {
        if (is_array($health)) {
            $useMinMax = false;
            if (isset($health['min'])) {
                $this->addUsingAlias(ApiUnitsPeer::HEALTH, $health['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($health['max'])) {
                $this->addUsingAlias(ApiUnitsPeer::HEALTH, $health['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiUnitsPeer::HEALTH, $health, $comparison);
    }

    /**
     * Filter the query on the unit_unitType column
     *
     * Example usage:
     * <code>
     * $query->filterByUnitUnittype(1234); // WHERE unit_unitType = 1234
     * $query->filterByUnitUnittype(array(12, 34)); // WHERE unit_unitType IN (12, 34)
     * $query->filterByUnitUnittype(array('min' => 12)); // WHERE unit_unitType >= 12
     * $query->filterByUnitUnittype(array('max' => 12)); // WHERE unit_unitType <= 12
     * </code>
     *
     * @param     mixed $unitUnittype The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiUnitsQuery The current query, for fluid interface
     */
    public function filterByUnitUnittype($unitUnittype = null, $comparison = null)
    {
        if (is_array($unitUnittype)) {
            $useMinMax = false;
            if (isset($unitUnittype['min'])) {
                $this->addUsingAlias(ApiUnitsPeer::UNIT_UNITTYPE, $unitUnittype['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($unitUnittype['max'])) {
                $this->addUsingAlias(ApiUnitsPeer::UNIT_UNITTYPE, $unitUnittype['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiUnitsPeer::UNIT_UNITTYPE, $unitUnittype, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ApiUnits $apiUnits Object to remove from the list of results
     *
     * @return ApiUnitsQuery The current query, for fluid interface
     */
    public function prune($apiUnits = null)
    {
        if ($apiUnits) {
            $this->addUsingAlias(ApiUnitsPeer::ID, $apiUnits->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
