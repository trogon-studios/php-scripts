<?php


/**
 * Base class that represents a query for the 'api_vehicles' table.
 *
 *
 *
 * @method ApiVehiclesQuery orderById($order = Criteria::ASC) Order by the id column
 * @method ApiVehiclesQuery orderByHealth($order = Criteria::ASC) Order by the health column
 * @method ApiVehiclesQuery orderByVehicleVehicletype($order = Criteria::ASC) Order by the vehicle_vehicleType column
 *
 * @method ApiVehiclesQuery groupById() Group by the id column
 * @method ApiVehiclesQuery groupByHealth() Group by the health column
 * @method ApiVehiclesQuery groupByVehicleVehicletype() Group by the vehicle_vehicleType column
 *
 * @method ApiVehiclesQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method ApiVehiclesQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method ApiVehiclesQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method ApiVehicles findOne(PropelPDO $con = null) Return the first ApiVehicles matching the query
 * @method ApiVehicles findOneOrCreate(PropelPDO $con = null) Return the first ApiVehicles matching the query, or a new ApiVehicles object populated from the query conditions when no match is found
 *
 * @method ApiVehicles findOneByHealth(int $health) Return the first ApiVehicles filtered by the health column
 * @method ApiVehicles findOneByVehicleVehicletype(int $vehicle_vehicleType) Return the first ApiVehicles filtered by the vehicle_vehicleType column
 *
 * @method array findById(string $id) Return ApiVehicles objects filtered by the id column
 * @method array findByHealth(int $health) Return ApiVehicles objects filtered by the health column
 * @method array findByVehicleVehicletype(int $vehicle_vehicleType) Return ApiVehicles objects filtered by the vehicle_vehicleType column
 *
 * @package    propel.generator.bookstore.om
 */
abstract class BaseApiVehiclesQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseApiVehiclesQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'bookstore';
        }
        if (null === $modelName) {
            $modelName = 'ApiVehicles';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ApiVehiclesQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   ApiVehiclesQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return ApiVehiclesQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof ApiVehiclesQuery) {
            return $criteria;
        }
        $query = new ApiVehiclesQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   ApiVehicles|ApiVehicles[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = ApiVehiclesPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(ApiVehiclesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 ApiVehicles A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 ApiVehicles A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `health`, `vehicle_vehicleType` FROM `api_vehicles` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new ApiVehicles();
            $obj->hydrate($row);
            ApiVehiclesPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return ApiVehicles|ApiVehicles[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|ApiVehicles[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return ApiVehiclesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ApiVehiclesPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return ApiVehiclesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ApiVehiclesPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiVehiclesQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ApiVehiclesPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ApiVehiclesPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiVehiclesPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the health column
     *
     * Example usage:
     * <code>
     * $query->filterByHealth(1234); // WHERE health = 1234
     * $query->filterByHealth(array(12, 34)); // WHERE health IN (12, 34)
     * $query->filterByHealth(array('min' => 12)); // WHERE health >= 12
     * $query->filterByHealth(array('max' => 12)); // WHERE health <= 12
     * </code>
     *
     * @param     mixed $health The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiVehiclesQuery The current query, for fluid interface
     */
    public function filterByHealth($health = null, $comparison = null)
    {
        if (is_array($health)) {
            $useMinMax = false;
            if (isset($health['min'])) {
                $this->addUsingAlias(ApiVehiclesPeer::HEALTH, $health['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($health['max'])) {
                $this->addUsingAlias(ApiVehiclesPeer::HEALTH, $health['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiVehiclesPeer::HEALTH, $health, $comparison);
    }

    /**
     * Filter the query on the vehicle_vehicleType column
     *
     * Example usage:
     * <code>
     * $query->filterByVehicleVehicletype(1234); // WHERE vehicle_vehicleType = 1234
     * $query->filterByVehicleVehicletype(array(12, 34)); // WHERE vehicle_vehicleType IN (12, 34)
     * $query->filterByVehicleVehicletype(array('min' => 12)); // WHERE vehicle_vehicleType >= 12
     * $query->filterByVehicleVehicletype(array('max' => 12)); // WHERE vehicle_vehicleType <= 12
     * </code>
     *
     * @param     mixed $vehicleVehicletype The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiVehiclesQuery The current query, for fluid interface
     */
    public function filterByVehicleVehicletype($vehicleVehicletype = null, $comparison = null)
    {
        if (is_array($vehicleVehicletype)) {
            $useMinMax = false;
            if (isset($vehicleVehicletype['min'])) {
                $this->addUsingAlias(ApiVehiclesPeer::VEHICLE_VEHICLETYPE, $vehicleVehicletype['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($vehicleVehicletype['max'])) {
                $this->addUsingAlias(ApiVehiclesPeer::VEHICLE_VEHICLETYPE, $vehicleVehicletype['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiVehiclesPeer::VEHICLE_VEHICLETYPE, $vehicleVehicletype, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ApiVehicles $apiVehicles Object to remove from the list of results
     *
     * @return ApiVehiclesQuery The current query, for fluid interface
     */
    public function prune($apiVehicles = null)
    {
        if ($apiVehicles) {
            $this->addUsingAlias(ApiVehiclesPeer::ID, $apiVehicles->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
