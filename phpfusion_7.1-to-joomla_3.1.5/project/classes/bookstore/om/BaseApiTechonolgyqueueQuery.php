<?php


/**
 * Base class that represents a query for the 'api_techonolgyQueue' table.
 *
 *
 *
 * @method ApiTechonolgyqueueQuery orderById($order = Criteria::ASC) Order by the id column
 * @method ApiTechonolgyqueueQuery orderByDateoffinish($order = Criteria::ASC) Order by the dateOfFinish column
 * @method ApiTechonolgyqueueQuery orderByTechonolgyqueueTechonolgytype($order = Criteria::ASC) Order by the techonolgyQueue_techonolgyType column
 * @method ApiTechonolgyqueueQuery orderByTechonolgyqueuePlayer($order = Criteria::ASC) Order by the techonolgyQueue_player column
 * @method ApiTechonolgyqueueQuery orderByTechonolgyqueuePlot($order = Criteria::ASC) Order by the techonolgyQueue_plot column
 * @method ApiTechonolgyqueueQuery orderByTechonolgyqueueBuilding($order = Criteria::ASC) Order by the techonolgyQueue_building column
 *
 * @method ApiTechonolgyqueueQuery groupById() Group by the id column
 * @method ApiTechonolgyqueueQuery groupByDateoffinish() Group by the dateOfFinish column
 * @method ApiTechonolgyqueueQuery groupByTechonolgyqueueTechonolgytype() Group by the techonolgyQueue_techonolgyType column
 * @method ApiTechonolgyqueueQuery groupByTechonolgyqueuePlayer() Group by the techonolgyQueue_player column
 * @method ApiTechonolgyqueueQuery groupByTechonolgyqueuePlot() Group by the techonolgyQueue_plot column
 * @method ApiTechonolgyqueueQuery groupByTechonolgyqueueBuilding() Group by the techonolgyQueue_building column
 *
 * @method ApiTechonolgyqueueQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method ApiTechonolgyqueueQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method ApiTechonolgyqueueQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method ApiTechonolgyqueue findOne(PropelPDO $con = null) Return the first ApiTechonolgyqueue matching the query
 * @method ApiTechonolgyqueue findOneOrCreate(PropelPDO $con = null) Return the first ApiTechonolgyqueue matching the query, or a new ApiTechonolgyqueue object populated from the query conditions when no match is found
 *
 * @method ApiTechonolgyqueue findOneByDateoffinish(string $dateOfFinish) Return the first ApiTechonolgyqueue filtered by the dateOfFinish column
 * @method ApiTechonolgyqueue findOneByTechonolgyqueueTechonolgytype(string $techonolgyQueue_techonolgyType) Return the first ApiTechonolgyqueue filtered by the techonolgyQueue_techonolgyType column
 * @method ApiTechonolgyqueue findOneByTechonolgyqueuePlayer(string $techonolgyQueue_player) Return the first ApiTechonolgyqueue filtered by the techonolgyQueue_player column
 * @method ApiTechonolgyqueue findOneByTechonolgyqueuePlot(string $techonolgyQueue_plot) Return the first ApiTechonolgyqueue filtered by the techonolgyQueue_plot column
 * @method ApiTechonolgyqueue findOneByTechonolgyqueueBuilding(string $techonolgyQueue_building) Return the first ApiTechonolgyqueue filtered by the techonolgyQueue_building column
 *
 * @method array findById(int $id) Return ApiTechonolgyqueue objects filtered by the id column
 * @method array findByDateoffinish(string $dateOfFinish) Return ApiTechonolgyqueue objects filtered by the dateOfFinish column
 * @method array findByTechonolgyqueueTechonolgytype(string $techonolgyQueue_techonolgyType) Return ApiTechonolgyqueue objects filtered by the techonolgyQueue_techonolgyType column
 * @method array findByTechonolgyqueuePlayer(string $techonolgyQueue_player) Return ApiTechonolgyqueue objects filtered by the techonolgyQueue_player column
 * @method array findByTechonolgyqueuePlot(string $techonolgyQueue_plot) Return ApiTechonolgyqueue objects filtered by the techonolgyQueue_plot column
 * @method array findByTechonolgyqueueBuilding(string $techonolgyQueue_building) Return ApiTechonolgyqueue objects filtered by the techonolgyQueue_building column
 *
 * @package    propel.generator.bookstore.om
 */
abstract class BaseApiTechonolgyqueueQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseApiTechonolgyqueueQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'bookstore';
        }
        if (null === $modelName) {
            $modelName = 'ApiTechonolgyqueue';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ApiTechonolgyqueueQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   ApiTechonolgyqueueQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return ApiTechonolgyqueueQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof ApiTechonolgyqueueQuery) {
            return $criteria;
        }
        $query = new ApiTechonolgyqueueQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   ApiTechonolgyqueue|ApiTechonolgyqueue[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = ApiTechonolgyqueuePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(ApiTechonolgyqueuePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 ApiTechonolgyqueue A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 ApiTechonolgyqueue A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `dateOfFinish`, `techonolgyQueue_techonolgyType`, `techonolgyQueue_player`, `techonolgyQueue_plot`, `techonolgyQueue_building` FROM `api_techonolgyQueue` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new ApiTechonolgyqueue();
            $obj->hydrate($row);
            ApiTechonolgyqueuePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return ApiTechonolgyqueue|ApiTechonolgyqueue[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|ApiTechonolgyqueue[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return ApiTechonolgyqueueQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ApiTechonolgyqueuePeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return ApiTechonolgyqueueQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ApiTechonolgyqueuePeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiTechonolgyqueueQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ApiTechonolgyqueuePeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ApiTechonolgyqueuePeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiTechonolgyqueuePeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the dateOfFinish column
     *
     * Example usage:
     * <code>
     * $query->filterByDateoffinish('2011-03-14'); // WHERE dateOfFinish = '2011-03-14'
     * $query->filterByDateoffinish('now'); // WHERE dateOfFinish = '2011-03-14'
     * $query->filterByDateoffinish(array('max' => 'yesterday')); // WHERE dateOfFinish < '2011-03-13'
     * </code>
     *
     * @param     mixed $dateoffinish The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiTechonolgyqueueQuery The current query, for fluid interface
     */
    public function filterByDateoffinish($dateoffinish = null, $comparison = null)
    {
        if (is_array($dateoffinish)) {
            $useMinMax = false;
            if (isset($dateoffinish['min'])) {
                $this->addUsingAlias(ApiTechonolgyqueuePeer::DATEOFFINISH, $dateoffinish['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateoffinish['max'])) {
                $this->addUsingAlias(ApiTechonolgyqueuePeer::DATEOFFINISH, $dateoffinish['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiTechonolgyqueuePeer::DATEOFFINISH, $dateoffinish, $comparison);
    }

    /**
     * Filter the query on the techonolgyQueue_techonolgyType column
     *
     * Example usage:
     * <code>
     * $query->filterByTechonolgyqueueTechonolgytype(1234); // WHERE techonolgyQueue_techonolgyType = 1234
     * $query->filterByTechonolgyqueueTechonolgytype(array(12, 34)); // WHERE techonolgyQueue_techonolgyType IN (12, 34)
     * $query->filterByTechonolgyqueueTechonolgytype(array('min' => 12)); // WHERE techonolgyQueue_techonolgyType >= 12
     * $query->filterByTechonolgyqueueTechonolgytype(array('max' => 12)); // WHERE techonolgyQueue_techonolgyType <= 12
     * </code>
     *
     * @param     mixed $techonolgyqueueTechonolgytype The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiTechonolgyqueueQuery The current query, for fluid interface
     */
    public function filterByTechonolgyqueueTechonolgytype($techonolgyqueueTechonolgytype = null, $comparison = null)
    {
        if (is_array($techonolgyqueueTechonolgytype)) {
            $useMinMax = false;
            if (isset($techonolgyqueueTechonolgytype['min'])) {
                $this->addUsingAlias(ApiTechonolgyqueuePeer::TECHONOLGYQUEUE_TECHONOLGYTYPE, $techonolgyqueueTechonolgytype['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($techonolgyqueueTechonolgytype['max'])) {
                $this->addUsingAlias(ApiTechonolgyqueuePeer::TECHONOLGYQUEUE_TECHONOLGYTYPE, $techonolgyqueueTechonolgytype['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiTechonolgyqueuePeer::TECHONOLGYQUEUE_TECHONOLGYTYPE, $techonolgyqueueTechonolgytype, $comparison);
    }

    /**
     * Filter the query on the techonolgyQueue_player column
     *
     * Example usage:
     * <code>
     * $query->filterByTechonolgyqueuePlayer(1234); // WHERE techonolgyQueue_player = 1234
     * $query->filterByTechonolgyqueuePlayer(array(12, 34)); // WHERE techonolgyQueue_player IN (12, 34)
     * $query->filterByTechonolgyqueuePlayer(array('min' => 12)); // WHERE techonolgyQueue_player >= 12
     * $query->filterByTechonolgyqueuePlayer(array('max' => 12)); // WHERE techonolgyQueue_player <= 12
     * </code>
     *
     * @param     mixed $techonolgyqueuePlayer The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiTechonolgyqueueQuery The current query, for fluid interface
     */
    public function filterByTechonolgyqueuePlayer($techonolgyqueuePlayer = null, $comparison = null)
    {
        if (is_array($techonolgyqueuePlayer)) {
            $useMinMax = false;
            if (isset($techonolgyqueuePlayer['min'])) {
                $this->addUsingAlias(ApiTechonolgyqueuePeer::TECHONOLGYQUEUE_PLAYER, $techonolgyqueuePlayer['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($techonolgyqueuePlayer['max'])) {
                $this->addUsingAlias(ApiTechonolgyqueuePeer::TECHONOLGYQUEUE_PLAYER, $techonolgyqueuePlayer['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiTechonolgyqueuePeer::TECHONOLGYQUEUE_PLAYER, $techonolgyqueuePlayer, $comparison);
    }

    /**
     * Filter the query on the techonolgyQueue_plot column
     *
     * Example usage:
     * <code>
     * $query->filterByTechonolgyqueuePlot(1234); // WHERE techonolgyQueue_plot = 1234
     * $query->filterByTechonolgyqueuePlot(array(12, 34)); // WHERE techonolgyQueue_plot IN (12, 34)
     * $query->filterByTechonolgyqueuePlot(array('min' => 12)); // WHERE techonolgyQueue_plot >= 12
     * $query->filterByTechonolgyqueuePlot(array('max' => 12)); // WHERE techonolgyQueue_plot <= 12
     * </code>
     *
     * @param     mixed $techonolgyqueuePlot The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiTechonolgyqueueQuery The current query, for fluid interface
     */
    public function filterByTechonolgyqueuePlot($techonolgyqueuePlot = null, $comparison = null)
    {
        if (is_array($techonolgyqueuePlot)) {
            $useMinMax = false;
            if (isset($techonolgyqueuePlot['min'])) {
                $this->addUsingAlias(ApiTechonolgyqueuePeer::TECHONOLGYQUEUE_PLOT, $techonolgyqueuePlot['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($techonolgyqueuePlot['max'])) {
                $this->addUsingAlias(ApiTechonolgyqueuePeer::TECHONOLGYQUEUE_PLOT, $techonolgyqueuePlot['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiTechonolgyqueuePeer::TECHONOLGYQUEUE_PLOT, $techonolgyqueuePlot, $comparison);
    }

    /**
     * Filter the query on the techonolgyQueue_building column
     *
     * Example usage:
     * <code>
     * $query->filterByTechonolgyqueueBuilding(1234); // WHERE techonolgyQueue_building = 1234
     * $query->filterByTechonolgyqueueBuilding(array(12, 34)); // WHERE techonolgyQueue_building IN (12, 34)
     * $query->filterByTechonolgyqueueBuilding(array('min' => 12)); // WHERE techonolgyQueue_building >= 12
     * $query->filterByTechonolgyqueueBuilding(array('max' => 12)); // WHERE techonolgyQueue_building <= 12
     * </code>
     *
     * @param     mixed $techonolgyqueueBuilding The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiTechonolgyqueueQuery The current query, for fluid interface
     */
    public function filterByTechonolgyqueueBuilding($techonolgyqueueBuilding = null, $comparison = null)
    {
        if (is_array($techonolgyqueueBuilding)) {
            $useMinMax = false;
            if (isset($techonolgyqueueBuilding['min'])) {
                $this->addUsingAlias(ApiTechonolgyqueuePeer::TECHONOLGYQUEUE_BUILDING, $techonolgyqueueBuilding['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($techonolgyqueueBuilding['max'])) {
                $this->addUsingAlias(ApiTechonolgyqueuePeer::TECHONOLGYQUEUE_BUILDING, $techonolgyqueueBuilding['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiTechonolgyqueuePeer::TECHONOLGYQUEUE_BUILDING, $techonolgyqueueBuilding, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ApiTechonolgyqueue $apiTechonolgyqueue Object to remove from the list of results
     *
     * @return ApiTechonolgyqueueQuery The current query, for fluid interface
     */
    public function prune($apiTechonolgyqueue = null)
    {
        if ($apiTechonolgyqueue) {
            $this->addUsingAlias(ApiTechonolgyqueuePeer::ID, $apiTechonolgyqueue->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
