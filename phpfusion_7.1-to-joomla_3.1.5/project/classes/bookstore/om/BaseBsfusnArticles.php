<?php


/**
 * Base class that represents a row from the 'bsfusn_articles' table.
 *
 *
 *
 * @package    propel.generator.bookstore.om
 */
abstract class BaseBsfusnArticles extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'BsfusnArticlesPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        BsfusnArticlesPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the article_id field.
     * @var        int
     */
    protected $article_id;

    /**
     * The value for the article_cat field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $article_cat;

    /**
     * The value for the article_subject field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $article_subject;

    /**
     * The value for the article_snippet field.
     * @var        string
     */
    protected $article_snippet;

    /**
     * The value for the article_article field.
     * @var        string
     */
    protected $article_article;

    /**
     * The value for the article_draft field.
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $article_draft;

    /**
     * The value for the article_breaks field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $article_breaks;

    /**
     * The value for the article_name field.
     * Note: this column has a database default value of: 1
     * @var        int
     */
    protected $article_name;

    /**
     * The value for the article_datestamp field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $article_datestamp;

    /**
     * The value for the article_reads field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $article_reads;

    /**
     * The value for the article_allow_comments field.
     * Note: this column has a database default value of: true
     * @var        boolean
     */
    protected $article_allow_comments;

    /**
     * The value for the article_allow_ratings field.
     * Note: this column has a database default value of: true
     * @var        boolean
     */
    protected $article_allow_ratings;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->article_cat = 0;
        $this->article_subject = '';
        $this->article_draft = false;
        $this->article_breaks = '';
        $this->article_name = 1;
        $this->article_datestamp = 0;
        $this->article_reads = 0;
        $this->article_allow_comments = true;
        $this->article_allow_ratings = true;
    }

    /**
     * Initializes internal state of BaseBsfusnArticles object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [article_id] column value.
     *
     * @return int
     */
    public function getArticleId()
    {

        return $this->article_id;
    }

    /**
     * Get the [article_cat] column value.
     *
     * @return int
     */
    public function getArticleCat()
    {

        return $this->article_cat;
    }

    /**
     * Get the [article_subject] column value.
     *
     * @return string
     */
    public function getArticleSubject()
    {

        return $this->article_subject;
    }

    /**
     * Get the [article_snippet] column value.
     *
     * @return string
     */
    public function getArticleSnippet()
    {

        return $this->article_snippet;
    }

    /**
     * Get the [article_article] column value.
     *
     * @return string
     */
    public function getArticleArticle()
    {

        return $this->article_article;
    }

    /**
     * Get the [article_draft] column value.
     *
     * @return boolean
     */
    public function getArticleDraft()
    {

        return $this->article_draft;
    }

    /**
     * Get the [article_breaks] column value.
     *
     * @return string
     */
    public function getArticleBreaks()
    {

        return $this->article_breaks;
    }

    /**
     * Get the [article_name] column value.
     *
     * @return int
     */
    public function getArticleName()
    {

        return $this->article_name;
    }

    /**
     * Get the [article_datestamp] column value.
     *
     * @return int
     */
    public function getArticleDatestamp()
    {

        return $this->article_datestamp;
    }

    /**
     * Get the [article_reads] column value.
     *
     * @return int
     */
    public function getArticleReads()
    {

        return $this->article_reads;
    }

    /**
     * Get the [article_allow_comments] column value.
     *
     * @return boolean
     */
    public function getArticleAllowComments()
    {

        return $this->article_allow_comments;
    }

    /**
     * Get the [article_allow_ratings] column value.
     *
     * @return boolean
     */
    public function getArticleAllowRatings()
    {

        return $this->article_allow_ratings;
    }

    /**
     * Set the value of [article_id] column.
     *
     * @param  int $v new value
     * @return BsfusnArticles The current object (for fluent API support)
     */
    public function setArticleId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->article_id !== $v) {
            $this->article_id = $v;
            $this->modifiedColumns[] = BsfusnArticlesPeer::ARTICLE_ID;
        }


        return $this;
    } // setArticleId()

    /**
     * Set the value of [article_cat] column.
     *
     * @param  int $v new value
     * @return BsfusnArticles The current object (for fluent API support)
     */
    public function setArticleCat($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->article_cat !== $v) {
            $this->article_cat = $v;
            $this->modifiedColumns[] = BsfusnArticlesPeer::ARTICLE_CAT;
        }


        return $this;
    } // setArticleCat()

    /**
     * Set the value of [article_subject] column.
     *
     * @param  string $v new value
     * @return BsfusnArticles The current object (for fluent API support)
     */
    public function setArticleSubject($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->article_subject !== $v) {
            $this->article_subject = $v;
            $this->modifiedColumns[] = BsfusnArticlesPeer::ARTICLE_SUBJECT;
        }


        return $this;
    } // setArticleSubject()

    /**
     * Set the value of [article_snippet] column.
     *
     * @param  string $v new value
     * @return BsfusnArticles The current object (for fluent API support)
     */
    public function setArticleSnippet($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->article_snippet !== $v) {
            $this->article_snippet = $v;
            $this->modifiedColumns[] = BsfusnArticlesPeer::ARTICLE_SNIPPET;
        }


        return $this;
    } // setArticleSnippet()

    /**
     * Set the value of [article_article] column.
     *
     * @param  string $v new value
     * @return BsfusnArticles The current object (for fluent API support)
     */
    public function setArticleArticle($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->article_article !== $v) {
            $this->article_article = $v;
            $this->modifiedColumns[] = BsfusnArticlesPeer::ARTICLE_ARTICLE;
        }


        return $this;
    } // setArticleArticle()

    /**
     * Sets the value of the [article_draft] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return BsfusnArticles The current object (for fluent API support)
     */
    public function setArticleDraft($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->article_draft !== $v) {
            $this->article_draft = $v;
            $this->modifiedColumns[] = BsfusnArticlesPeer::ARTICLE_DRAFT;
        }


        return $this;
    } // setArticleDraft()

    /**
     * Set the value of [article_breaks] column.
     *
     * @param  string $v new value
     * @return BsfusnArticles The current object (for fluent API support)
     */
    public function setArticleBreaks($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->article_breaks !== $v) {
            $this->article_breaks = $v;
            $this->modifiedColumns[] = BsfusnArticlesPeer::ARTICLE_BREAKS;
        }


        return $this;
    } // setArticleBreaks()

    /**
     * Set the value of [article_name] column.
     *
     * @param  int $v new value
     * @return BsfusnArticles The current object (for fluent API support)
     */
    public function setArticleName($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->article_name !== $v) {
            $this->article_name = $v;
            $this->modifiedColumns[] = BsfusnArticlesPeer::ARTICLE_NAME;
        }


        return $this;
    } // setArticleName()

    /**
     * Set the value of [article_datestamp] column.
     *
     * @param  int $v new value
     * @return BsfusnArticles The current object (for fluent API support)
     */
    public function setArticleDatestamp($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->article_datestamp !== $v) {
            $this->article_datestamp = $v;
            $this->modifiedColumns[] = BsfusnArticlesPeer::ARTICLE_DATESTAMP;
        }


        return $this;
    } // setArticleDatestamp()

    /**
     * Set the value of [article_reads] column.
     *
     * @param  int $v new value
     * @return BsfusnArticles The current object (for fluent API support)
     */
    public function setArticleReads($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->article_reads !== $v) {
            $this->article_reads = $v;
            $this->modifiedColumns[] = BsfusnArticlesPeer::ARTICLE_READS;
        }


        return $this;
    } // setArticleReads()

    /**
     * Sets the value of the [article_allow_comments] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return BsfusnArticles The current object (for fluent API support)
     */
    public function setArticleAllowComments($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->article_allow_comments !== $v) {
            $this->article_allow_comments = $v;
            $this->modifiedColumns[] = BsfusnArticlesPeer::ARTICLE_ALLOW_COMMENTS;
        }


        return $this;
    } // setArticleAllowComments()

    /**
     * Sets the value of the [article_allow_ratings] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return BsfusnArticles The current object (for fluent API support)
     */
    public function setArticleAllowRatings($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->article_allow_ratings !== $v) {
            $this->article_allow_ratings = $v;
            $this->modifiedColumns[] = BsfusnArticlesPeer::ARTICLE_ALLOW_RATINGS;
        }


        return $this;
    } // setArticleAllowRatings()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->article_cat !== 0) {
                return false;
            }

            if ($this->article_subject !== '') {
                return false;
            }

            if ($this->article_draft !== false) {
                return false;
            }

            if ($this->article_breaks !== '') {
                return false;
            }

            if ($this->article_name !== 1) {
                return false;
            }

            if ($this->article_datestamp !== 0) {
                return false;
            }

            if ($this->article_reads !== 0) {
                return false;
            }

            if ($this->article_allow_comments !== true) {
                return false;
            }

            if ($this->article_allow_ratings !== true) {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->article_id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->article_cat = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->article_subject = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->article_snippet = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->article_article = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->article_draft = ($row[$startcol + 5] !== null) ? (boolean) $row[$startcol + 5] : null;
            $this->article_breaks = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->article_name = ($row[$startcol + 7] !== null) ? (int) $row[$startcol + 7] : null;
            $this->article_datestamp = ($row[$startcol + 8] !== null) ? (int) $row[$startcol + 8] : null;
            $this->article_reads = ($row[$startcol + 9] !== null) ? (int) $row[$startcol + 9] : null;
            $this->article_allow_comments = ($row[$startcol + 10] !== null) ? (boolean) $row[$startcol + 10] : null;
            $this->article_allow_ratings = ($row[$startcol + 11] !== null) ? (boolean) $row[$startcol + 11] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 12; // 12 = BsfusnArticlesPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating BsfusnArticles object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(BsfusnArticlesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = BsfusnArticlesPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(BsfusnArticlesPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = BsfusnArticlesQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(BsfusnArticlesPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                BsfusnArticlesPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = BsfusnArticlesPeer::ARTICLE_ID;
        if (null !== $this->article_id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . BsfusnArticlesPeer::ARTICLE_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(BsfusnArticlesPeer::ARTICLE_ID)) {
            $modifiedColumns[':p' . $index++]  = '`article_id`';
        }
        if ($this->isColumnModified(BsfusnArticlesPeer::ARTICLE_CAT)) {
            $modifiedColumns[':p' . $index++]  = '`article_cat`';
        }
        if ($this->isColumnModified(BsfusnArticlesPeer::ARTICLE_SUBJECT)) {
            $modifiedColumns[':p' . $index++]  = '`article_subject`';
        }
        if ($this->isColumnModified(BsfusnArticlesPeer::ARTICLE_SNIPPET)) {
            $modifiedColumns[':p' . $index++]  = '`article_snippet`';
        }
        if ($this->isColumnModified(BsfusnArticlesPeer::ARTICLE_ARTICLE)) {
            $modifiedColumns[':p' . $index++]  = '`article_article`';
        }
        if ($this->isColumnModified(BsfusnArticlesPeer::ARTICLE_DRAFT)) {
            $modifiedColumns[':p' . $index++]  = '`article_draft`';
        }
        if ($this->isColumnModified(BsfusnArticlesPeer::ARTICLE_BREAKS)) {
            $modifiedColumns[':p' . $index++]  = '`article_breaks`';
        }
        if ($this->isColumnModified(BsfusnArticlesPeer::ARTICLE_NAME)) {
            $modifiedColumns[':p' . $index++]  = '`article_name`';
        }
        if ($this->isColumnModified(BsfusnArticlesPeer::ARTICLE_DATESTAMP)) {
            $modifiedColumns[':p' . $index++]  = '`article_datestamp`';
        }
        if ($this->isColumnModified(BsfusnArticlesPeer::ARTICLE_READS)) {
            $modifiedColumns[':p' . $index++]  = '`article_reads`';
        }
        if ($this->isColumnModified(BsfusnArticlesPeer::ARTICLE_ALLOW_COMMENTS)) {
            $modifiedColumns[':p' . $index++]  = '`article_allow_comments`';
        }
        if ($this->isColumnModified(BsfusnArticlesPeer::ARTICLE_ALLOW_RATINGS)) {
            $modifiedColumns[':p' . $index++]  = '`article_allow_ratings`';
        }

        $sql = sprintf(
            'INSERT INTO `bsfusn_articles` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`article_id`':
                        $stmt->bindValue($identifier, $this->article_id, PDO::PARAM_INT);
                        break;
                    case '`article_cat`':
                        $stmt->bindValue($identifier, $this->article_cat, PDO::PARAM_INT);
                        break;
                    case '`article_subject`':
                        $stmt->bindValue($identifier, $this->article_subject, PDO::PARAM_STR);
                        break;
                    case '`article_snippet`':
                        $stmt->bindValue($identifier, $this->article_snippet, PDO::PARAM_STR);
                        break;
                    case '`article_article`':
                        $stmt->bindValue($identifier, $this->article_article, PDO::PARAM_STR);
                        break;
                    case '`article_draft`':
                        $stmt->bindValue($identifier, (int) $this->article_draft, PDO::PARAM_INT);
                        break;
                    case '`article_breaks`':
                        $stmt->bindValue($identifier, $this->article_breaks, PDO::PARAM_STR);
                        break;
                    case '`article_name`':
                        $stmt->bindValue($identifier, $this->article_name, PDO::PARAM_INT);
                        break;
                    case '`article_datestamp`':
                        $stmt->bindValue($identifier, $this->article_datestamp, PDO::PARAM_INT);
                        break;
                    case '`article_reads`':
                        $stmt->bindValue($identifier, $this->article_reads, PDO::PARAM_INT);
                        break;
                    case '`article_allow_comments`':
                        $stmt->bindValue($identifier, (int) $this->article_allow_comments, PDO::PARAM_INT);
                        break;
                    case '`article_allow_ratings`':
                        $stmt->bindValue($identifier, (int) $this->article_allow_ratings, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setArticleId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            if (($retval = BsfusnArticlesPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }



            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = BsfusnArticlesPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getArticleId();
                break;
            case 1:
                return $this->getArticleCat();
                break;
            case 2:
                return $this->getArticleSubject();
                break;
            case 3:
                return $this->getArticleSnippet();
                break;
            case 4:
                return $this->getArticleArticle();
                break;
            case 5:
                return $this->getArticleDraft();
                break;
            case 6:
                return $this->getArticleBreaks();
                break;
            case 7:
                return $this->getArticleName();
                break;
            case 8:
                return $this->getArticleDatestamp();
                break;
            case 9:
                return $this->getArticleReads();
                break;
            case 10:
                return $this->getArticleAllowComments();
                break;
            case 11:
                return $this->getArticleAllowRatings();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array())
    {
        if (isset($alreadyDumpedObjects['BsfusnArticles'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['BsfusnArticles'][$this->getPrimaryKey()] = true;
        $keys = BsfusnArticlesPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getArticleId(),
            $keys[1] => $this->getArticleCat(),
            $keys[2] => $this->getArticleSubject(),
            $keys[3] => $this->getArticleSnippet(),
            $keys[4] => $this->getArticleArticle(),
            $keys[5] => $this->getArticleDraft(),
            $keys[6] => $this->getArticleBreaks(),
            $keys[7] => $this->getArticleName(),
            $keys[8] => $this->getArticleDatestamp(),
            $keys[9] => $this->getArticleReads(),
            $keys[10] => $this->getArticleAllowComments(),
            $keys[11] => $this->getArticleAllowRatings(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach($virtualColumns as $key => $virtualColumn)
        {
            $result[$key] = $virtualColumn;
        }


        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = BsfusnArticlesPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setArticleId($value);
                break;
            case 1:
                $this->setArticleCat($value);
                break;
            case 2:
                $this->setArticleSubject($value);
                break;
            case 3:
                $this->setArticleSnippet($value);
                break;
            case 4:
                $this->setArticleArticle($value);
                break;
            case 5:
                $this->setArticleDraft($value);
                break;
            case 6:
                $this->setArticleBreaks($value);
                break;
            case 7:
                $this->setArticleName($value);
                break;
            case 8:
                $this->setArticleDatestamp($value);
                break;
            case 9:
                $this->setArticleReads($value);
                break;
            case 10:
                $this->setArticleAllowComments($value);
                break;
            case 11:
                $this->setArticleAllowRatings($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = BsfusnArticlesPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setArticleId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setArticleCat($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setArticleSubject($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setArticleSnippet($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setArticleArticle($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setArticleDraft($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setArticleBreaks($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setArticleName($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setArticleDatestamp($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setArticleReads($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setArticleAllowComments($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setArticleAllowRatings($arr[$keys[11]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(BsfusnArticlesPeer::DATABASE_NAME);

        if ($this->isColumnModified(BsfusnArticlesPeer::ARTICLE_ID)) $criteria->add(BsfusnArticlesPeer::ARTICLE_ID, $this->article_id);
        if ($this->isColumnModified(BsfusnArticlesPeer::ARTICLE_CAT)) $criteria->add(BsfusnArticlesPeer::ARTICLE_CAT, $this->article_cat);
        if ($this->isColumnModified(BsfusnArticlesPeer::ARTICLE_SUBJECT)) $criteria->add(BsfusnArticlesPeer::ARTICLE_SUBJECT, $this->article_subject);
        if ($this->isColumnModified(BsfusnArticlesPeer::ARTICLE_SNIPPET)) $criteria->add(BsfusnArticlesPeer::ARTICLE_SNIPPET, $this->article_snippet);
        if ($this->isColumnModified(BsfusnArticlesPeer::ARTICLE_ARTICLE)) $criteria->add(BsfusnArticlesPeer::ARTICLE_ARTICLE, $this->article_article);
        if ($this->isColumnModified(BsfusnArticlesPeer::ARTICLE_DRAFT)) $criteria->add(BsfusnArticlesPeer::ARTICLE_DRAFT, $this->article_draft);
        if ($this->isColumnModified(BsfusnArticlesPeer::ARTICLE_BREAKS)) $criteria->add(BsfusnArticlesPeer::ARTICLE_BREAKS, $this->article_breaks);
        if ($this->isColumnModified(BsfusnArticlesPeer::ARTICLE_NAME)) $criteria->add(BsfusnArticlesPeer::ARTICLE_NAME, $this->article_name);
        if ($this->isColumnModified(BsfusnArticlesPeer::ARTICLE_DATESTAMP)) $criteria->add(BsfusnArticlesPeer::ARTICLE_DATESTAMP, $this->article_datestamp);
        if ($this->isColumnModified(BsfusnArticlesPeer::ARTICLE_READS)) $criteria->add(BsfusnArticlesPeer::ARTICLE_READS, $this->article_reads);
        if ($this->isColumnModified(BsfusnArticlesPeer::ARTICLE_ALLOW_COMMENTS)) $criteria->add(BsfusnArticlesPeer::ARTICLE_ALLOW_COMMENTS, $this->article_allow_comments);
        if ($this->isColumnModified(BsfusnArticlesPeer::ARTICLE_ALLOW_RATINGS)) $criteria->add(BsfusnArticlesPeer::ARTICLE_ALLOW_RATINGS, $this->article_allow_ratings);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(BsfusnArticlesPeer::DATABASE_NAME);
        $criteria->add(BsfusnArticlesPeer::ARTICLE_ID, $this->article_id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getArticleId();
    }

    /**
     * Generic method to set the primary key (article_id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setArticleId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getArticleId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of BsfusnArticles (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setArticleCat($this->getArticleCat());
        $copyObj->setArticleSubject($this->getArticleSubject());
        $copyObj->setArticleSnippet($this->getArticleSnippet());
        $copyObj->setArticleArticle($this->getArticleArticle());
        $copyObj->setArticleDraft($this->getArticleDraft());
        $copyObj->setArticleBreaks($this->getArticleBreaks());
        $copyObj->setArticleName($this->getArticleName());
        $copyObj->setArticleDatestamp($this->getArticleDatestamp());
        $copyObj->setArticleReads($this->getArticleReads());
        $copyObj->setArticleAllowComments($this->getArticleAllowComments());
        $copyObj->setArticleAllowRatings($this->getArticleAllowRatings());
        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setArticleId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return BsfusnArticles Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return BsfusnArticlesPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new BsfusnArticlesPeer();
        }

        return self::$peer;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->article_id = null;
        $this->article_cat = null;
        $this->article_subject = null;
        $this->article_snippet = null;
        $this->article_article = null;
        $this->article_draft = null;
        $this->article_breaks = null;
        $this->article_name = null;
        $this->article_datestamp = null;
        $this->article_reads = null;
        $this->article_allow_comments = null;
        $this->article_allow_ratings = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(BsfusnArticlesPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
