<?php


/**
 * Base class that represents a query for the 'api_news' table.
 *
 *
 *
 * @method ApiNewsQuery orderById($order = Criteria::ASC) Order by the id column
 * @method ApiNewsQuery orderByTitle($order = Criteria::ASC) Order by the title column
 * @method ApiNewsQuery orderByContent($order = Criteria::ASC) Order by the content column
 * @method ApiNewsQuery orderByAuthor($order = Criteria::ASC) Order by the author column
 * @method ApiNewsQuery orderByPublisheddate($order = Criteria::ASC) Order by the publishedDate column
 *
 * @method ApiNewsQuery groupById() Group by the id column
 * @method ApiNewsQuery groupByTitle() Group by the title column
 * @method ApiNewsQuery groupByContent() Group by the content column
 * @method ApiNewsQuery groupByAuthor() Group by the author column
 * @method ApiNewsQuery groupByPublisheddate() Group by the publishedDate column
 *
 * @method ApiNewsQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method ApiNewsQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method ApiNewsQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method ApiNews findOne(PropelPDO $con = null) Return the first ApiNews matching the query
 * @method ApiNews findOneOrCreate(PropelPDO $con = null) Return the first ApiNews matching the query, or a new ApiNews object populated from the query conditions when no match is found
 *
 * @method ApiNews findOneByTitle(string $title) Return the first ApiNews filtered by the title column
 * @method ApiNews findOneByContent(string $content) Return the first ApiNews filtered by the content column
 * @method ApiNews findOneByAuthor(string $author) Return the first ApiNews filtered by the author column
 * @method ApiNews findOneByPublisheddate(string $publishedDate) Return the first ApiNews filtered by the publishedDate column
 *
 * @method array findById(string $id) Return ApiNews objects filtered by the id column
 * @method array findByTitle(string $title) Return ApiNews objects filtered by the title column
 * @method array findByContent(string $content) Return ApiNews objects filtered by the content column
 * @method array findByAuthor(string $author) Return ApiNews objects filtered by the author column
 * @method array findByPublisheddate(string $publishedDate) Return ApiNews objects filtered by the publishedDate column
 *
 * @package    propel.generator.bookstore.om
 */
abstract class BaseApiNewsQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseApiNewsQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'bookstore';
        }
        if (null === $modelName) {
            $modelName = 'ApiNews';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ApiNewsQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   ApiNewsQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return ApiNewsQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof ApiNewsQuery) {
            return $criteria;
        }
        $query = new ApiNewsQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   ApiNews|ApiNews[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = ApiNewsPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(ApiNewsPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 ApiNews A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 ApiNews A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `title`, `content`, `author`, `publishedDate` FROM `api_news` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new ApiNews();
            $obj->hydrate($row);
            ApiNewsPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return ApiNews|ApiNews[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|ApiNews[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return ApiNewsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ApiNewsPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return ApiNewsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ApiNewsPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiNewsQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ApiNewsPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ApiNewsPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiNewsPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the title column
     *
     * Example usage:
     * <code>
     * $query->filterByTitle('fooValue');   // WHERE title = 'fooValue'
     * $query->filterByTitle('%fooValue%'); // WHERE title LIKE '%fooValue%'
     * </code>
     *
     * @param     string $title The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiNewsQuery The current query, for fluid interface
     */
    public function filterByTitle($title = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($title)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $title)) {
                $title = str_replace('*', '%', $title);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ApiNewsPeer::TITLE, $title, $comparison);
    }

    /**
     * Filter the query on the content column
     *
     * Example usage:
     * <code>
     * $query->filterByContent('fooValue');   // WHERE content = 'fooValue'
     * $query->filterByContent('%fooValue%'); // WHERE content LIKE '%fooValue%'
     * </code>
     *
     * @param     string $content The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiNewsQuery The current query, for fluid interface
     */
    public function filterByContent($content = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($content)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $content)) {
                $content = str_replace('*', '%', $content);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ApiNewsPeer::CONTENT, $content, $comparison);
    }

    /**
     * Filter the query on the author column
     *
     * Example usage:
     * <code>
     * $query->filterByAuthor('fooValue');   // WHERE author = 'fooValue'
     * $query->filterByAuthor('%fooValue%'); // WHERE author LIKE '%fooValue%'
     * </code>
     *
     * @param     string $author The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiNewsQuery The current query, for fluid interface
     */
    public function filterByAuthor($author = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($author)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $author)) {
                $author = str_replace('*', '%', $author);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ApiNewsPeer::AUTHOR, $author, $comparison);
    }

    /**
     * Filter the query on the publishedDate column
     *
     * Example usage:
     * <code>
     * $query->filterByPublisheddate('2011-03-14'); // WHERE publishedDate = '2011-03-14'
     * $query->filterByPublisheddate('now'); // WHERE publishedDate = '2011-03-14'
     * $query->filterByPublisheddate(array('max' => 'yesterday')); // WHERE publishedDate < '2011-03-13'
     * </code>
     *
     * @param     mixed $publisheddate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiNewsQuery The current query, for fluid interface
     */
    public function filterByPublisheddate($publisheddate = null, $comparison = null)
    {
        if (is_array($publisheddate)) {
            $useMinMax = false;
            if (isset($publisheddate['min'])) {
                $this->addUsingAlias(ApiNewsPeer::PUBLISHEDDATE, $publisheddate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($publisheddate['max'])) {
                $this->addUsingAlias(ApiNewsPeer::PUBLISHEDDATE, $publisheddate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiNewsPeer::PUBLISHEDDATE, $publisheddate, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ApiNews $apiNews Object to remove from the list of results
     *
     * @return ApiNewsQuery The current query, for fluid interface
     */
    public function prune($apiNews = null)
    {
        if ($apiNews) {
            $this->addUsingAlias(ApiNewsPeer::ID, $apiNews->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
