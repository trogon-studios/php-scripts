<?php


/**
 * Base class that represents a row from the 'bsfusn_news' table.
 *
 *
 *
 * @package    propel.generator.bookstore.om
 */
abstract class BaseBsfusnNews extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'BsfusnNewsPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        BsfusnNewsPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the news_id field.
     * @var        int
     */
    protected $news_id;

    /**
     * The value for the news_subject field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $news_subject;

    /**
     * The value for the news_cat field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $news_cat;

    /**
     * The value for the news_image field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $news_image;

    /**
     * The value for the news_image_t1 field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $news_image_t1;

    /**
     * The value for the news_image_t2 field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $news_image_t2;

    /**
     * The value for the news_news field.
     * @var        string
     */
    protected $news_news;

    /**
     * The value for the news_extended field.
     * @var        string
     */
    protected $news_extended;

    /**
     * The value for the news_breaks field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $news_breaks;

    /**
     * The value for the news_name field.
     * Note: this column has a database default value of: 1
     * @var        int
     */
    protected $news_name;

    /**
     * The value for the news_datestamp field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $news_datestamp;

    /**
     * The value for the news_start field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $news_start;

    /**
     * The value for the news_end field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $news_end;

    /**
     * The value for the news_visibility field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $news_visibility;

    /**
     * The value for the news_reads field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $news_reads;

    /**
     * The value for the news_draft field.
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $news_draft;

    /**
     * The value for the news_sticky field.
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $news_sticky;

    /**
     * The value for the news_allow_comments field.
     * Note: this column has a database default value of: true
     * @var        boolean
     */
    protected $news_allow_comments;

    /**
     * The value for the news_allow_ratings field.
     * Note: this column has a database default value of: true
     * @var        boolean
     */
    protected $news_allow_ratings;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->news_subject = '';
        $this->news_cat = 0;
        $this->news_image = '';
        $this->news_image_t1 = '';
        $this->news_image_t2 = '';
        $this->news_breaks = '';
        $this->news_name = 1;
        $this->news_datestamp = 0;
        $this->news_start = 0;
        $this->news_end = 0;
        $this->news_visibility = 0;
        $this->news_reads = 0;
        $this->news_draft = false;
        $this->news_sticky = false;
        $this->news_allow_comments = true;
        $this->news_allow_ratings = true;
    }

    /**
     * Initializes internal state of BaseBsfusnNews object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [news_id] column value.
     *
     * @return int
     */
    public function getNewsId()
    {

        return $this->news_id;
    }

    /**
     * Get the [news_subject] column value.
     *
     * @return string
     */
    public function getNewsSubject()
    {

        return $this->news_subject;
    }

    /**
     * Get the [news_cat] column value.
     *
     * @return int
     */
    public function getNewsCat()
    {

        return $this->news_cat;
    }

    /**
     * Get the [news_image] column value.
     *
     * @return string
     */
    public function getNewsImage()
    {

        return $this->news_image;
    }

    /**
     * Get the [news_image_t1] column value.
     *
     * @return string
     */
    public function getNewsImageT1()
    {

        return $this->news_image_t1;
    }

    /**
     * Get the [news_image_t2] column value.
     *
     * @return string
     */
    public function getNewsImageT2()
    {

        return $this->news_image_t2;
    }

    /**
     * Get the [news_news] column value.
     *
     * @return string
     */
    public function getNewsNews()
    {

        return $this->news_news;
    }

    /**
     * Get the [news_extended] column value.
     *
     * @return string
     */
    public function getNewsExtended()
    {

        return $this->news_extended;
    }

    /**
     * Get the [news_breaks] column value.
     *
     * @return string
     */
    public function getNewsBreaks()
    {

        return $this->news_breaks;
    }

    /**
     * Get the [news_name] column value.
     *
     * @return int
     */
    public function getNewsName()
    {

        return $this->news_name;
    }

    /**
     * Get the [news_datestamp] column value.
     *
     * @return int
     */
    public function getNewsDatestamp()
    {

        return $this->news_datestamp;
    }

    /**
     * Get the [news_start] column value.
     *
     * @return int
     */
    public function getNewsStart()
    {

        return $this->news_start;
    }

    /**
     * Get the [news_end] column value.
     *
     * @return int
     */
    public function getNewsEnd()
    {

        return $this->news_end;
    }

    /**
     * Get the [news_visibility] column value.
     *
     * @return int
     */
    public function getNewsVisibility()
    {

        return $this->news_visibility;
    }

    /**
     * Get the [news_reads] column value.
     *
     * @return int
     */
    public function getNewsReads()
    {

        return $this->news_reads;
    }

    /**
     * Get the [news_draft] column value.
     *
     * @return boolean
     */
    public function getNewsDraft()
    {

        return $this->news_draft;
    }

    /**
     * Get the [news_sticky] column value.
     *
     * @return boolean
     */
    public function getNewsSticky()
    {

        return $this->news_sticky;
    }

    /**
     * Get the [news_allow_comments] column value.
     *
     * @return boolean
     */
    public function getNewsAllowComments()
    {

        return $this->news_allow_comments;
    }

    /**
     * Get the [news_allow_ratings] column value.
     *
     * @return boolean
     */
    public function getNewsAllowRatings()
    {

        return $this->news_allow_ratings;
    }

    /**
     * Set the value of [news_id] column.
     *
     * @param  int $v new value
     * @return BsfusnNews The current object (for fluent API support)
     */
    public function setNewsId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->news_id !== $v) {
            $this->news_id = $v;
            $this->modifiedColumns[] = BsfusnNewsPeer::NEWS_ID;
        }


        return $this;
    } // setNewsId()

    /**
     * Set the value of [news_subject] column.
     *
     * @param  string $v new value
     * @return BsfusnNews The current object (for fluent API support)
     */
    public function setNewsSubject($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->news_subject !== $v) {
            $this->news_subject = $v;
            $this->modifiedColumns[] = BsfusnNewsPeer::NEWS_SUBJECT;
        }


        return $this;
    } // setNewsSubject()

    /**
     * Set the value of [news_cat] column.
     *
     * @param  int $v new value
     * @return BsfusnNews The current object (for fluent API support)
     */
    public function setNewsCat($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->news_cat !== $v) {
            $this->news_cat = $v;
            $this->modifiedColumns[] = BsfusnNewsPeer::NEWS_CAT;
        }


        return $this;
    } // setNewsCat()

    /**
     * Set the value of [news_image] column.
     *
     * @param  string $v new value
     * @return BsfusnNews The current object (for fluent API support)
     */
    public function setNewsImage($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->news_image !== $v) {
            $this->news_image = $v;
            $this->modifiedColumns[] = BsfusnNewsPeer::NEWS_IMAGE;
        }


        return $this;
    } // setNewsImage()

    /**
     * Set the value of [news_image_t1] column.
     *
     * @param  string $v new value
     * @return BsfusnNews The current object (for fluent API support)
     */
    public function setNewsImageT1($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->news_image_t1 !== $v) {
            $this->news_image_t1 = $v;
            $this->modifiedColumns[] = BsfusnNewsPeer::NEWS_IMAGE_T1;
        }


        return $this;
    } // setNewsImageT1()

    /**
     * Set the value of [news_image_t2] column.
     *
     * @param  string $v new value
     * @return BsfusnNews The current object (for fluent API support)
     */
    public function setNewsImageT2($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->news_image_t2 !== $v) {
            $this->news_image_t2 = $v;
            $this->modifiedColumns[] = BsfusnNewsPeer::NEWS_IMAGE_T2;
        }


        return $this;
    } // setNewsImageT2()

    /**
     * Set the value of [news_news] column.
     *
     * @param  string $v new value
     * @return BsfusnNews The current object (for fluent API support)
     */
    public function setNewsNews($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->news_news !== $v) {
            $this->news_news = $v;
            $this->modifiedColumns[] = BsfusnNewsPeer::NEWS_NEWS;
        }


        return $this;
    } // setNewsNews()

    /**
     * Set the value of [news_extended] column.
     *
     * @param  string $v new value
     * @return BsfusnNews The current object (for fluent API support)
     */
    public function setNewsExtended($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->news_extended !== $v) {
            $this->news_extended = $v;
            $this->modifiedColumns[] = BsfusnNewsPeer::NEWS_EXTENDED;
        }


        return $this;
    } // setNewsExtended()

    /**
     * Set the value of [news_breaks] column.
     *
     * @param  string $v new value
     * @return BsfusnNews The current object (for fluent API support)
     */
    public function setNewsBreaks($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->news_breaks !== $v) {
            $this->news_breaks = $v;
            $this->modifiedColumns[] = BsfusnNewsPeer::NEWS_BREAKS;
        }


        return $this;
    } // setNewsBreaks()

    /**
     * Set the value of [news_name] column.
     *
     * @param  int $v new value
     * @return BsfusnNews The current object (for fluent API support)
     */
    public function setNewsName($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->news_name !== $v) {
            $this->news_name = $v;
            $this->modifiedColumns[] = BsfusnNewsPeer::NEWS_NAME;
        }


        return $this;
    } // setNewsName()

    /**
     * Set the value of [news_datestamp] column.
     *
     * @param  int $v new value
     * @return BsfusnNews The current object (for fluent API support)
     */
    public function setNewsDatestamp($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->news_datestamp !== $v) {
            $this->news_datestamp = $v;
            $this->modifiedColumns[] = BsfusnNewsPeer::NEWS_DATESTAMP;
        }


        return $this;
    } // setNewsDatestamp()

    /**
     * Set the value of [news_start] column.
     *
     * @param  int $v new value
     * @return BsfusnNews The current object (for fluent API support)
     */
    public function setNewsStart($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->news_start !== $v) {
            $this->news_start = $v;
            $this->modifiedColumns[] = BsfusnNewsPeer::NEWS_START;
        }


        return $this;
    } // setNewsStart()

    /**
     * Set the value of [news_end] column.
     *
     * @param  int $v new value
     * @return BsfusnNews The current object (for fluent API support)
     */
    public function setNewsEnd($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->news_end !== $v) {
            $this->news_end = $v;
            $this->modifiedColumns[] = BsfusnNewsPeer::NEWS_END;
        }


        return $this;
    } // setNewsEnd()

    /**
     * Set the value of [news_visibility] column.
     *
     * @param  int $v new value
     * @return BsfusnNews The current object (for fluent API support)
     */
    public function setNewsVisibility($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->news_visibility !== $v) {
            $this->news_visibility = $v;
            $this->modifiedColumns[] = BsfusnNewsPeer::NEWS_VISIBILITY;
        }


        return $this;
    } // setNewsVisibility()

    /**
     * Set the value of [news_reads] column.
     *
     * @param  int $v new value
     * @return BsfusnNews The current object (for fluent API support)
     */
    public function setNewsReads($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->news_reads !== $v) {
            $this->news_reads = $v;
            $this->modifiedColumns[] = BsfusnNewsPeer::NEWS_READS;
        }


        return $this;
    } // setNewsReads()

    /**
     * Sets the value of the [news_draft] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return BsfusnNews The current object (for fluent API support)
     */
    public function setNewsDraft($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->news_draft !== $v) {
            $this->news_draft = $v;
            $this->modifiedColumns[] = BsfusnNewsPeer::NEWS_DRAFT;
        }


        return $this;
    } // setNewsDraft()

    /**
     * Sets the value of the [news_sticky] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return BsfusnNews The current object (for fluent API support)
     */
    public function setNewsSticky($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->news_sticky !== $v) {
            $this->news_sticky = $v;
            $this->modifiedColumns[] = BsfusnNewsPeer::NEWS_STICKY;
        }


        return $this;
    } // setNewsSticky()

    /**
     * Sets the value of the [news_allow_comments] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return BsfusnNews The current object (for fluent API support)
     */
    public function setNewsAllowComments($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->news_allow_comments !== $v) {
            $this->news_allow_comments = $v;
            $this->modifiedColumns[] = BsfusnNewsPeer::NEWS_ALLOW_COMMENTS;
        }


        return $this;
    } // setNewsAllowComments()

    /**
     * Sets the value of the [news_allow_ratings] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return BsfusnNews The current object (for fluent API support)
     */
    public function setNewsAllowRatings($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->news_allow_ratings !== $v) {
            $this->news_allow_ratings = $v;
            $this->modifiedColumns[] = BsfusnNewsPeer::NEWS_ALLOW_RATINGS;
        }


        return $this;
    } // setNewsAllowRatings()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->news_subject !== '') {
                return false;
            }

            if ($this->news_cat !== 0) {
                return false;
            }

            if ($this->news_image !== '') {
                return false;
            }

            if ($this->news_image_t1 !== '') {
                return false;
            }

            if ($this->news_image_t2 !== '') {
                return false;
            }

            if ($this->news_breaks !== '') {
                return false;
            }

            if ($this->news_name !== 1) {
                return false;
            }

            if ($this->news_datestamp !== 0) {
                return false;
            }

            if ($this->news_start !== 0) {
                return false;
            }

            if ($this->news_end !== 0) {
                return false;
            }

            if ($this->news_visibility !== 0) {
                return false;
            }

            if ($this->news_reads !== 0) {
                return false;
            }

            if ($this->news_draft !== false) {
                return false;
            }

            if ($this->news_sticky !== false) {
                return false;
            }

            if ($this->news_allow_comments !== true) {
                return false;
            }

            if ($this->news_allow_ratings !== true) {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->news_id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->news_subject = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->news_cat = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
            $this->news_image = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->news_image_t1 = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->news_image_t2 = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->news_news = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->news_extended = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->news_breaks = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->news_name = ($row[$startcol + 9] !== null) ? (int) $row[$startcol + 9] : null;
            $this->news_datestamp = ($row[$startcol + 10] !== null) ? (int) $row[$startcol + 10] : null;
            $this->news_start = ($row[$startcol + 11] !== null) ? (int) $row[$startcol + 11] : null;
            $this->news_end = ($row[$startcol + 12] !== null) ? (int) $row[$startcol + 12] : null;
            $this->news_visibility = ($row[$startcol + 13] !== null) ? (int) $row[$startcol + 13] : null;
            $this->news_reads = ($row[$startcol + 14] !== null) ? (int) $row[$startcol + 14] : null;
            $this->news_draft = ($row[$startcol + 15] !== null) ? (boolean) $row[$startcol + 15] : null;
            $this->news_sticky = ($row[$startcol + 16] !== null) ? (boolean) $row[$startcol + 16] : null;
            $this->news_allow_comments = ($row[$startcol + 17] !== null) ? (boolean) $row[$startcol + 17] : null;
            $this->news_allow_ratings = ($row[$startcol + 18] !== null) ? (boolean) $row[$startcol + 18] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 19; // 19 = BsfusnNewsPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating BsfusnNews object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(BsfusnNewsPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = BsfusnNewsPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(BsfusnNewsPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = BsfusnNewsQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(BsfusnNewsPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                BsfusnNewsPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = BsfusnNewsPeer::NEWS_ID;
        if (null !== $this->news_id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . BsfusnNewsPeer::NEWS_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(BsfusnNewsPeer::NEWS_ID)) {
            $modifiedColumns[':p' . $index++]  = '`news_id`';
        }
        if ($this->isColumnModified(BsfusnNewsPeer::NEWS_SUBJECT)) {
            $modifiedColumns[':p' . $index++]  = '`news_subject`';
        }
        if ($this->isColumnModified(BsfusnNewsPeer::NEWS_CAT)) {
            $modifiedColumns[':p' . $index++]  = '`news_cat`';
        }
        if ($this->isColumnModified(BsfusnNewsPeer::NEWS_IMAGE)) {
            $modifiedColumns[':p' . $index++]  = '`news_image`';
        }
        if ($this->isColumnModified(BsfusnNewsPeer::NEWS_IMAGE_T1)) {
            $modifiedColumns[':p' . $index++]  = '`news_image_t1`';
        }
        if ($this->isColumnModified(BsfusnNewsPeer::NEWS_IMAGE_T2)) {
            $modifiedColumns[':p' . $index++]  = '`news_image_t2`';
        }
        if ($this->isColumnModified(BsfusnNewsPeer::NEWS_NEWS)) {
            $modifiedColumns[':p' . $index++]  = '`news_news`';
        }
        if ($this->isColumnModified(BsfusnNewsPeer::NEWS_EXTENDED)) {
            $modifiedColumns[':p' . $index++]  = '`news_extended`';
        }
        if ($this->isColumnModified(BsfusnNewsPeer::NEWS_BREAKS)) {
            $modifiedColumns[':p' . $index++]  = '`news_breaks`';
        }
        if ($this->isColumnModified(BsfusnNewsPeer::NEWS_NAME)) {
            $modifiedColumns[':p' . $index++]  = '`news_name`';
        }
        if ($this->isColumnModified(BsfusnNewsPeer::NEWS_DATESTAMP)) {
            $modifiedColumns[':p' . $index++]  = '`news_datestamp`';
        }
        if ($this->isColumnModified(BsfusnNewsPeer::NEWS_START)) {
            $modifiedColumns[':p' . $index++]  = '`news_start`';
        }
        if ($this->isColumnModified(BsfusnNewsPeer::NEWS_END)) {
            $modifiedColumns[':p' . $index++]  = '`news_end`';
        }
        if ($this->isColumnModified(BsfusnNewsPeer::NEWS_VISIBILITY)) {
            $modifiedColumns[':p' . $index++]  = '`news_visibility`';
        }
        if ($this->isColumnModified(BsfusnNewsPeer::NEWS_READS)) {
            $modifiedColumns[':p' . $index++]  = '`news_reads`';
        }
        if ($this->isColumnModified(BsfusnNewsPeer::NEWS_DRAFT)) {
            $modifiedColumns[':p' . $index++]  = '`news_draft`';
        }
        if ($this->isColumnModified(BsfusnNewsPeer::NEWS_STICKY)) {
            $modifiedColumns[':p' . $index++]  = '`news_sticky`';
        }
        if ($this->isColumnModified(BsfusnNewsPeer::NEWS_ALLOW_COMMENTS)) {
            $modifiedColumns[':p' . $index++]  = '`news_allow_comments`';
        }
        if ($this->isColumnModified(BsfusnNewsPeer::NEWS_ALLOW_RATINGS)) {
            $modifiedColumns[':p' . $index++]  = '`news_allow_ratings`';
        }

        $sql = sprintf(
            'INSERT INTO `bsfusn_news` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`news_id`':
                        $stmt->bindValue($identifier, $this->news_id, PDO::PARAM_INT);
                        break;
                    case '`news_subject`':
                        $stmt->bindValue($identifier, $this->news_subject, PDO::PARAM_STR);
                        break;
                    case '`news_cat`':
                        $stmt->bindValue($identifier, $this->news_cat, PDO::PARAM_INT);
                        break;
                    case '`news_image`':
                        $stmt->bindValue($identifier, $this->news_image, PDO::PARAM_STR);
                        break;
                    case '`news_image_t1`':
                        $stmt->bindValue($identifier, $this->news_image_t1, PDO::PARAM_STR);
                        break;
                    case '`news_image_t2`':
                        $stmt->bindValue($identifier, $this->news_image_t2, PDO::PARAM_STR);
                        break;
                    case '`news_news`':
                        $stmt->bindValue($identifier, $this->news_news, PDO::PARAM_STR);
                        break;
                    case '`news_extended`':
                        $stmt->bindValue($identifier, $this->news_extended, PDO::PARAM_STR);
                        break;
                    case '`news_breaks`':
                        $stmt->bindValue($identifier, $this->news_breaks, PDO::PARAM_STR);
                        break;
                    case '`news_name`':
                        $stmt->bindValue($identifier, $this->news_name, PDO::PARAM_INT);
                        break;
                    case '`news_datestamp`':
                        $stmt->bindValue($identifier, $this->news_datestamp, PDO::PARAM_INT);
                        break;
                    case '`news_start`':
                        $stmt->bindValue($identifier, $this->news_start, PDO::PARAM_INT);
                        break;
                    case '`news_end`':
                        $stmt->bindValue($identifier, $this->news_end, PDO::PARAM_INT);
                        break;
                    case '`news_visibility`':
                        $stmt->bindValue($identifier, $this->news_visibility, PDO::PARAM_INT);
                        break;
                    case '`news_reads`':
                        $stmt->bindValue($identifier, $this->news_reads, PDO::PARAM_INT);
                        break;
                    case '`news_draft`':
                        $stmt->bindValue($identifier, (int) $this->news_draft, PDO::PARAM_INT);
                        break;
                    case '`news_sticky`':
                        $stmt->bindValue($identifier, (int) $this->news_sticky, PDO::PARAM_INT);
                        break;
                    case '`news_allow_comments`':
                        $stmt->bindValue($identifier, (int) $this->news_allow_comments, PDO::PARAM_INT);
                        break;
                    case '`news_allow_ratings`':
                        $stmt->bindValue($identifier, (int) $this->news_allow_ratings, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setNewsId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            if (($retval = BsfusnNewsPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }



            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = BsfusnNewsPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getNewsId();
                break;
            case 1:
                return $this->getNewsSubject();
                break;
            case 2:
                return $this->getNewsCat();
                break;
            case 3:
                return $this->getNewsImage();
                break;
            case 4:
                return $this->getNewsImageT1();
                break;
            case 5:
                return $this->getNewsImageT2();
                break;
            case 6:
                return $this->getNewsNews();
                break;
            case 7:
                return $this->getNewsExtended();
                break;
            case 8:
                return $this->getNewsBreaks();
                break;
            case 9:
                return $this->getNewsName();
                break;
            case 10:
                return $this->getNewsDatestamp();
                break;
            case 11:
                return $this->getNewsStart();
                break;
            case 12:
                return $this->getNewsEnd();
                break;
            case 13:
                return $this->getNewsVisibility();
                break;
            case 14:
                return $this->getNewsReads();
                break;
            case 15:
                return $this->getNewsDraft();
                break;
            case 16:
                return $this->getNewsSticky();
                break;
            case 17:
                return $this->getNewsAllowComments();
                break;
            case 18:
                return $this->getNewsAllowRatings();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array())
    {
        if (isset($alreadyDumpedObjects['BsfusnNews'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['BsfusnNews'][$this->getPrimaryKey()] = true;
        $keys = BsfusnNewsPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getNewsId(),
            $keys[1] => $this->getNewsSubject(),
            $keys[2] => $this->getNewsCat(),
            $keys[3] => $this->getNewsImage(),
            $keys[4] => $this->getNewsImageT1(),
            $keys[5] => $this->getNewsImageT2(),
            $keys[6] => $this->getNewsNews(),
            $keys[7] => $this->getNewsExtended(),
            $keys[8] => $this->getNewsBreaks(),
            $keys[9] => $this->getNewsName(),
            $keys[10] => $this->getNewsDatestamp(),
            $keys[11] => $this->getNewsStart(),
            $keys[12] => $this->getNewsEnd(),
            $keys[13] => $this->getNewsVisibility(),
            $keys[14] => $this->getNewsReads(),
            $keys[15] => $this->getNewsDraft(),
            $keys[16] => $this->getNewsSticky(),
            $keys[17] => $this->getNewsAllowComments(),
            $keys[18] => $this->getNewsAllowRatings(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach($virtualColumns as $key => $virtualColumn)
        {
            $result[$key] = $virtualColumn;
        }


        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = BsfusnNewsPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setNewsId($value);
                break;
            case 1:
                $this->setNewsSubject($value);
                break;
            case 2:
                $this->setNewsCat($value);
                break;
            case 3:
                $this->setNewsImage($value);
                break;
            case 4:
                $this->setNewsImageT1($value);
                break;
            case 5:
                $this->setNewsImageT2($value);
                break;
            case 6:
                $this->setNewsNews($value);
                break;
            case 7:
                $this->setNewsExtended($value);
                break;
            case 8:
                $this->setNewsBreaks($value);
                break;
            case 9:
                $this->setNewsName($value);
                break;
            case 10:
                $this->setNewsDatestamp($value);
                break;
            case 11:
                $this->setNewsStart($value);
                break;
            case 12:
                $this->setNewsEnd($value);
                break;
            case 13:
                $this->setNewsVisibility($value);
                break;
            case 14:
                $this->setNewsReads($value);
                break;
            case 15:
                $this->setNewsDraft($value);
                break;
            case 16:
                $this->setNewsSticky($value);
                break;
            case 17:
                $this->setNewsAllowComments($value);
                break;
            case 18:
                $this->setNewsAllowRatings($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = BsfusnNewsPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setNewsId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setNewsSubject($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setNewsCat($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setNewsImage($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setNewsImageT1($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setNewsImageT2($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setNewsNews($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setNewsExtended($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setNewsBreaks($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setNewsName($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setNewsDatestamp($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setNewsStart($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setNewsEnd($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setNewsVisibility($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr)) $this->setNewsReads($arr[$keys[14]]);
        if (array_key_exists($keys[15], $arr)) $this->setNewsDraft($arr[$keys[15]]);
        if (array_key_exists($keys[16], $arr)) $this->setNewsSticky($arr[$keys[16]]);
        if (array_key_exists($keys[17], $arr)) $this->setNewsAllowComments($arr[$keys[17]]);
        if (array_key_exists($keys[18], $arr)) $this->setNewsAllowRatings($arr[$keys[18]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(BsfusnNewsPeer::DATABASE_NAME);

        if ($this->isColumnModified(BsfusnNewsPeer::NEWS_ID)) $criteria->add(BsfusnNewsPeer::NEWS_ID, $this->news_id);
        if ($this->isColumnModified(BsfusnNewsPeer::NEWS_SUBJECT)) $criteria->add(BsfusnNewsPeer::NEWS_SUBJECT, $this->news_subject);
        if ($this->isColumnModified(BsfusnNewsPeer::NEWS_CAT)) $criteria->add(BsfusnNewsPeer::NEWS_CAT, $this->news_cat);
        if ($this->isColumnModified(BsfusnNewsPeer::NEWS_IMAGE)) $criteria->add(BsfusnNewsPeer::NEWS_IMAGE, $this->news_image);
        if ($this->isColumnModified(BsfusnNewsPeer::NEWS_IMAGE_T1)) $criteria->add(BsfusnNewsPeer::NEWS_IMAGE_T1, $this->news_image_t1);
        if ($this->isColumnModified(BsfusnNewsPeer::NEWS_IMAGE_T2)) $criteria->add(BsfusnNewsPeer::NEWS_IMAGE_T2, $this->news_image_t2);
        if ($this->isColumnModified(BsfusnNewsPeer::NEWS_NEWS)) $criteria->add(BsfusnNewsPeer::NEWS_NEWS, $this->news_news);
        if ($this->isColumnModified(BsfusnNewsPeer::NEWS_EXTENDED)) $criteria->add(BsfusnNewsPeer::NEWS_EXTENDED, $this->news_extended);
        if ($this->isColumnModified(BsfusnNewsPeer::NEWS_BREAKS)) $criteria->add(BsfusnNewsPeer::NEWS_BREAKS, $this->news_breaks);
        if ($this->isColumnModified(BsfusnNewsPeer::NEWS_NAME)) $criteria->add(BsfusnNewsPeer::NEWS_NAME, $this->news_name);
        if ($this->isColumnModified(BsfusnNewsPeer::NEWS_DATESTAMP)) $criteria->add(BsfusnNewsPeer::NEWS_DATESTAMP, $this->news_datestamp);
        if ($this->isColumnModified(BsfusnNewsPeer::NEWS_START)) $criteria->add(BsfusnNewsPeer::NEWS_START, $this->news_start);
        if ($this->isColumnModified(BsfusnNewsPeer::NEWS_END)) $criteria->add(BsfusnNewsPeer::NEWS_END, $this->news_end);
        if ($this->isColumnModified(BsfusnNewsPeer::NEWS_VISIBILITY)) $criteria->add(BsfusnNewsPeer::NEWS_VISIBILITY, $this->news_visibility);
        if ($this->isColumnModified(BsfusnNewsPeer::NEWS_READS)) $criteria->add(BsfusnNewsPeer::NEWS_READS, $this->news_reads);
        if ($this->isColumnModified(BsfusnNewsPeer::NEWS_DRAFT)) $criteria->add(BsfusnNewsPeer::NEWS_DRAFT, $this->news_draft);
        if ($this->isColumnModified(BsfusnNewsPeer::NEWS_STICKY)) $criteria->add(BsfusnNewsPeer::NEWS_STICKY, $this->news_sticky);
        if ($this->isColumnModified(BsfusnNewsPeer::NEWS_ALLOW_COMMENTS)) $criteria->add(BsfusnNewsPeer::NEWS_ALLOW_COMMENTS, $this->news_allow_comments);
        if ($this->isColumnModified(BsfusnNewsPeer::NEWS_ALLOW_RATINGS)) $criteria->add(BsfusnNewsPeer::NEWS_ALLOW_RATINGS, $this->news_allow_ratings);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(BsfusnNewsPeer::DATABASE_NAME);
        $criteria->add(BsfusnNewsPeer::NEWS_ID, $this->news_id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getNewsId();
    }

    /**
     * Generic method to set the primary key (news_id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setNewsId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getNewsId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of BsfusnNews (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setNewsSubject($this->getNewsSubject());
        $copyObj->setNewsCat($this->getNewsCat());
        $copyObj->setNewsImage($this->getNewsImage());
        $copyObj->setNewsImageT1($this->getNewsImageT1());
        $copyObj->setNewsImageT2($this->getNewsImageT2());
        $copyObj->setNewsNews($this->getNewsNews());
        $copyObj->setNewsExtended($this->getNewsExtended());
        $copyObj->setNewsBreaks($this->getNewsBreaks());
        $copyObj->setNewsName($this->getNewsName());
        $copyObj->setNewsDatestamp($this->getNewsDatestamp());
        $copyObj->setNewsStart($this->getNewsStart());
        $copyObj->setNewsEnd($this->getNewsEnd());
        $copyObj->setNewsVisibility($this->getNewsVisibility());
        $copyObj->setNewsReads($this->getNewsReads());
        $copyObj->setNewsDraft($this->getNewsDraft());
        $copyObj->setNewsSticky($this->getNewsSticky());
        $copyObj->setNewsAllowComments($this->getNewsAllowComments());
        $copyObj->setNewsAllowRatings($this->getNewsAllowRatings());
        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setNewsId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return BsfusnNews Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return BsfusnNewsPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new BsfusnNewsPeer();
        }

        return self::$peer;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->news_id = null;
        $this->news_subject = null;
        $this->news_cat = null;
        $this->news_image = null;
        $this->news_image_t1 = null;
        $this->news_image_t2 = null;
        $this->news_news = null;
        $this->news_extended = null;
        $this->news_breaks = null;
        $this->news_name = null;
        $this->news_datestamp = null;
        $this->news_start = null;
        $this->news_end = null;
        $this->news_visibility = null;
        $this->news_reads = null;
        $this->news_draft = null;
        $this->news_sticky = null;
        $this->news_allow_comments = null;
        $this->news_allow_ratings = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(BsfusnNewsPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
