<?php


/**
 * Base class that represents a query for the 'jm3_content' table.
 *
 *
 *
 * @method Jm3ContentQuery orderById($order = Criteria::ASC) Order by the id column
 * @method Jm3ContentQuery orderByAssetId($order = Criteria::ASC) Order by the asset_id column
 * @method Jm3ContentQuery orderByTitle($order = Criteria::ASC) Order by the title column
 * @method Jm3ContentQuery orderByAlias($order = Criteria::ASC) Order by the alias column
 * @method Jm3ContentQuery orderByIntrotext($order = Criteria::ASC) Order by the introtext column
 * @method Jm3ContentQuery orderByFulltext($order = Criteria::ASC) Order by the fulltext column
 * @method Jm3ContentQuery orderByState($order = Criteria::ASC) Order by the state column
 * @method Jm3ContentQuery orderByCatid($order = Criteria::ASC) Order by the catid column
 * @method Jm3ContentQuery orderByCreated($order = Criteria::ASC) Order by the created column
 * @method Jm3ContentQuery orderByCreatedBy($order = Criteria::ASC) Order by the created_by column
 * @method Jm3ContentQuery orderByCreatedByAlias($order = Criteria::ASC) Order by the created_by_alias column
 * @method Jm3ContentQuery orderByModified($order = Criteria::ASC) Order by the modified column
 * @method Jm3ContentQuery orderByModifiedBy($order = Criteria::ASC) Order by the modified_by column
 * @method Jm3ContentQuery orderByCheckedOut($order = Criteria::ASC) Order by the checked_out column
 * @method Jm3ContentQuery orderByCheckedOutTime($order = Criteria::ASC) Order by the checked_out_time column
 * @method Jm3ContentQuery orderByPublishUp($order = Criteria::ASC) Order by the publish_up column
 * @method Jm3ContentQuery orderByPublishDown($order = Criteria::ASC) Order by the publish_down column
 * @method Jm3ContentQuery orderByImages($order = Criteria::ASC) Order by the images column
 * @method Jm3ContentQuery orderByUrls($order = Criteria::ASC) Order by the urls column
 * @method Jm3ContentQuery orderByAttribs($order = Criteria::ASC) Order by the attribs column
 * @method Jm3ContentQuery orderByVersion($order = Criteria::ASC) Order by the version column
 * @method Jm3ContentQuery orderByOrdering($order = Criteria::ASC) Order by the ordering column
 * @method Jm3ContentQuery orderByMetakey($order = Criteria::ASC) Order by the metakey column
 * @method Jm3ContentQuery orderByMetadesc($order = Criteria::ASC) Order by the metadesc column
 * @method Jm3ContentQuery orderByAccess($order = Criteria::ASC) Order by the access column
 * @method Jm3ContentQuery orderByHits($order = Criteria::ASC) Order by the hits column
 * @method Jm3ContentQuery orderByMetadata($order = Criteria::ASC) Order by the metadata column
 * @method Jm3ContentQuery orderByFeatured($order = Criteria::ASC) Order by the featured column
 * @method Jm3ContentQuery orderByLanguage($order = Criteria::ASC) Order by the language column
 * @method Jm3ContentQuery orderByXreference($order = Criteria::ASC) Order by the xreference column
 *
 * @method Jm3ContentQuery groupById() Group by the id column
 * @method Jm3ContentQuery groupByAssetId() Group by the asset_id column
 * @method Jm3ContentQuery groupByTitle() Group by the title column
 * @method Jm3ContentQuery groupByAlias() Group by the alias column
 * @method Jm3ContentQuery groupByIntrotext() Group by the introtext column
 * @method Jm3ContentQuery groupByFulltext() Group by the fulltext column
 * @method Jm3ContentQuery groupByState() Group by the state column
 * @method Jm3ContentQuery groupByCatid() Group by the catid column
 * @method Jm3ContentQuery groupByCreated() Group by the created column
 * @method Jm3ContentQuery groupByCreatedBy() Group by the created_by column
 * @method Jm3ContentQuery groupByCreatedByAlias() Group by the created_by_alias column
 * @method Jm3ContentQuery groupByModified() Group by the modified column
 * @method Jm3ContentQuery groupByModifiedBy() Group by the modified_by column
 * @method Jm3ContentQuery groupByCheckedOut() Group by the checked_out column
 * @method Jm3ContentQuery groupByCheckedOutTime() Group by the checked_out_time column
 * @method Jm3ContentQuery groupByPublishUp() Group by the publish_up column
 * @method Jm3ContentQuery groupByPublishDown() Group by the publish_down column
 * @method Jm3ContentQuery groupByImages() Group by the images column
 * @method Jm3ContentQuery groupByUrls() Group by the urls column
 * @method Jm3ContentQuery groupByAttribs() Group by the attribs column
 * @method Jm3ContentQuery groupByVersion() Group by the version column
 * @method Jm3ContentQuery groupByOrdering() Group by the ordering column
 * @method Jm3ContentQuery groupByMetakey() Group by the metakey column
 * @method Jm3ContentQuery groupByMetadesc() Group by the metadesc column
 * @method Jm3ContentQuery groupByAccess() Group by the access column
 * @method Jm3ContentQuery groupByHits() Group by the hits column
 * @method Jm3ContentQuery groupByMetadata() Group by the metadata column
 * @method Jm3ContentQuery groupByFeatured() Group by the featured column
 * @method Jm3ContentQuery groupByLanguage() Group by the language column
 * @method Jm3ContentQuery groupByXreference() Group by the xreference column
 *
 * @method Jm3ContentQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method Jm3ContentQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method Jm3ContentQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method Jm3Content findOne(PropelPDO $con = null) Return the first Jm3Content matching the query
 * @method Jm3Content findOneOrCreate(PropelPDO $con = null) Return the first Jm3Content matching the query, or a new Jm3Content object populated from the query conditions when no match is found
 *
 * @method Jm3Content findOneByAssetId(int $asset_id) Return the first Jm3Content filtered by the asset_id column
 * @method Jm3Content findOneByTitle(string $title) Return the first Jm3Content filtered by the title column
 * @method Jm3Content findOneByAlias(string $alias) Return the first Jm3Content filtered by the alias column
 * @method Jm3Content findOneByIntrotext(string $introtext) Return the first Jm3Content filtered by the introtext column
 * @method Jm3Content findOneByFulltext(string $fulltext) Return the first Jm3Content filtered by the fulltext column
 * @method Jm3Content findOneByState(int $state) Return the first Jm3Content filtered by the state column
 * @method Jm3Content findOneByCatid(int $catid) Return the first Jm3Content filtered by the catid column
 * @method Jm3Content findOneByCreated(string $created) Return the first Jm3Content filtered by the created column
 * @method Jm3Content findOneByCreatedBy(int $created_by) Return the first Jm3Content filtered by the created_by column
 * @method Jm3Content findOneByCreatedByAlias(string $created_by_alias) Return the first Jm3Content filtered by the created_by_alias column
 * @method Jm3Content findOneByModified(string $modified) Return the first Jm3Content filtered by the modified column
 * @method Jm3Content findOneByModifiedBy(int $modified_by) Return the first Jm3Content filtered by the modified_by column
 * @method Jm3Content findOneByCheckedOut(int $checked_out) Return the first Jm3Content filtered by the checked_out column
 * @method Jm3Content findOneByCheckedOutTime(string $checked_out_time) Return the first Jm3Content filtered by the checked_out_time column
 * @method Jm3Content findOneByPublishUp(string $publish_up) Return the first Jm3Content filtered by the publish_up column
 * @method Jm3Content findOneByPublishDown(string $publish_down) Return the first Jm3Content filtered by the publish_down column
 * @method Jm3Content findOneByImages(string $images) Return the first Jm3Content filtered by the images column
 * @method Jm3Content findOneByUrls(string $urls) Return the first Jm3Content filtered by the urls column
 * @method Jm3Content findOneByAttribs(string $attribs) Return the first Jm3Content filtered by the attribs column
 * @method Jm3Content findOneByVersion(int $version) Return the first Jm3Content filtered by the version column
 * @method Jm3Content findOneByOrdering(int $ordering) Return the first Jm3Content filtered by the ordering column
 * @method Jm3Content findOneByMetakey(string $metakey) Return the first Jm3Content filtered by the metakey column
 * @method Jm3Content findOneByMetadesc(string $metadesc) Return the first Jm3Content filtered by the metadesc column
 * @method Jm3Content findOneByAccess(int $access) Return the first Jm3Content filtered by the access column
 * @method Jm3Content findOneByHits(int $hits) Return the first Jm3Content filtered by the hits column
 * @method Jm3Content findOneByMetadata(string $metadata) Return the first Jm3Content filtered by the metadata column
 * @method Jm3Content findOneByFeatured(int $featured) Return the first Jm3Content filtered by the featured column
 * @method Jm3Content findOneByLanguage(string $language) Return the first Jm3Content filtered by the language column
 * @method Jm3Content findOneByXreference(string $xreference) Return the first Jm3Content filtered by the xreference column
 *
 * @method array findById(int $id) Return Jm3Content objects filtered by the id column
 * @method array findByAssetId(int $asset_id) Return Jm3Content objects filtered by the asset_id column
 * @method array findByTitle(string $title) Return Jm3Content objects filtered by the title column
 * @method array findByAlias(string $alias) Return Jm3Content objects filtered by the alias column
 * @method array findByIntrotext(string $introtext) Return Jm3Content objects filtered by the introtext column
 * @method array findByFulltext(string $fulltext) Return Jm3Content objects filtered by the fulltext column
 * @method array findByState(int $state) Return Jm3Content objects filtered by the state column
 * @method array findByCatid(int $catid) Return Jm3Content objects filtered by the catid column
 * @method array findByCreated(string $created) Return Jm3Content objects filtered by the created column
 * @method array findByCreatedBy(int $created_by) Return Jm3Content objects filtered by the created_by column
 * @method array findByCreatedByAlias(string $created_by_alias) Return Jm3Content objects filtered by the created_by_alias column
 * @method array findByModified(string $modified) Return Jm3Content objects filtered by the modified column
 * @method array findByModifiedBy(int $modified_by) Return Jm3Content objects filtered by the modified_by column
 * @method array findByCheckedOut(int $checked_out) Return Jm3Content objects filtered by the checked_out column
 * @method array findByCheckedOutTime(string $checked_out_time) Return Jm3Content objects filtered by the checked_out_time column
 * @method array findByPublishUp(string $publish_up) Return Jm3Content objects filtered by the publish_up column
 * @method array findByPublishDown(string $publish_down) Return Jm3Content objects filtered by the publish_down column
 * @method array findByImages(string $images) Return Jm3Content objects filtered by the images column
 * @method array findByUrls(string $urls) Return Jm3Content objects filtered by the urls column
 * @method array findByAttribs(string $attribs) Return Jm3Content objects filtered by the attribs column
 * @method array findByVersion(int $version) Return Jm3Content objects filtered by the version column
 * @method array findByOrdering(int $ordering) Return Jm3Content objects filtered by the ordering column
 * @method array findByMetakey(string $metakey) Return Jm3Content objects filtered by the metakey column
 * @method array findByMetadesc(string $metadesc) Return Jm3Content objects filtered by the metadesc column
 * @method array findByAccess(int $access) Return Jm3Content objects filtered by the access column
 * @method array findByHits(int $hits) Return Jm3Content objects filtered by the hits column
 * @method array findByMetadata(string $metadata) Return Jm3Content objects filtered by the metadata column
 * @method array findByFeatured(int $featured) Return Jm3Content objects filtered by the featured column
 * @method array findByLanguage(string $language) Return Jm3Content objects filtered by the language column
 * @method array findByXreference(string $xreference) Return Jm3Content objects filtered by the xreference column
 *
 * @package    propel.generator.bookstore.om
 */
abstract class BaseJm3ContentQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseJm3ContentQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'bookstore';
        }
        if (null === $modelName) {
            $modelName = 'Jm3Content';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new Jm3ContentQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   Jm3ContentQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return Jm3ContentQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof Jm3ContentQuery) {
            return $criteria;
        }
        $query = new Jm3ContentQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Jm3Content|Jm3Content[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = Jm3ContentPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(Jm3ContentPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Jm3Content A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Jm3Content A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `asset_id`, `title`, `alias`, `introtext`, `fulltext`, `state`, `catid`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `images`, `urls`, `attribs`, `version`, `ordering`, `metakey`, `metadesc`, `access`, `hits`, `metadata`, `featured`, `language`, `xreference` FROM `jm3_content` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Jm3Content();
            $obj->hydrate($row);
            Jm3ContentPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Jm3Content|Jm3Content[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Jm3Content[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return Jm3ContentQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(Jm3ContentPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return Jm3ContentQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(Jm3ContentPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3ContentQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(Jm3ContentPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(Jm3ContentPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Jm3ContentPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the asset_id column
     *
     * Example usage:
     * <code>
     * $query->filterByAssetId(1234); // WHERE asset_id = 1234
     * $query->filterByAssetId(array(12, 34)); // WHERE asset_id IN (12, 34)
     * $query->filterByAssetId(array('min' => 12)); // WHERE asset_id >= 12
     * $query->filterByAssetId(array('max' => 12)); // WHERE asset_id <= 12
     * </code>
     *
     * @param     mixed $assetId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3ContentQuery The current query, for fluid interface
     */
    public function filterByAssetId($assetId = null, $comparison = null)
    {
        if (is_array($assetId)) {
            $useMinMax = false;
            if (isset($assetId['min'])) {
                $this->addUsingAlias(Jm3ContentPeer::ASSET_ID, $assetId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($assetId['max'])) {
                $this->addUsingAlias(Jm3ContentPeer::ASSET_ID, $assetId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Jm3ContentPeer::ASSET_ID, $assetId, $comparison);
    }

    /**
     * Filter the query on the title column
     *
     * Example usage:
     * <code>
     * $query->filterByTitle('fooValue');   // WHERE title = 'fooValue'
     * $query->filterByTitle('%fooValue%'); // WHERE title LIKE '%fooValue%'
     * </code>
     *
     * @param     string $title The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3ContentQuery The current query, for fluid interface
     */
    public function filterByTitle($title = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($title)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $title)) {
                $title = str_replace('*', '%', $title);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(Jm3ContentPeer::TITLE, $title, $comparison);
    }

    /**
     * Filter the query on the alias column
     *
     * Example usage:
     * <code>
     * $query->filterByAlias('fooValue');   // WHERE alias = 'fooValue'
     * $query->filterByAlias('%fooValue%'); // WHERE alias LIKE '%fooValue%'
     * </code>
     *
     * @param     string $alias The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3ContentQuery The current query, for fluid interface
     */
    public function filterByAlias($alias = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($alias)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $alias)) {
                $alias = str_replace('*', '%', $alias);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(Jm3ContentPeer::ALIAS, $alias, $comparison);
    }

    /**
     * Filter the query on the introtext column
     *
     * Example usage:
     * <code>
     * $query->filterByIntrotext('fooValue');   // WHERE introtext = 'fooValue'
     * $query->filterByIntrotext('%fooValue%'); // WHERE introtext LIKE '%fooValue%'
     * </code>
     *
     * @param     string $introtext The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3ContentQuery The current query, for fluid interface
     */
    public function filterByIntrotext($introtext = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($introtext)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $introtext)) {
                $introtext = str_replace('*', '%', $introtext);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(Jm3ContentPeer::INTROTEXT, $introtext, $comparison);
    }

    /**
     * Filter the query on the fulltext column
     *
     * Example usage:
     * <code>
     * $query->filterByFulltext('fooValue');   // WHERE fulltext = 'fooValue'
     * $query->filterByFulltext('%fooValue%'); // WHERE fulltext LIKE '%fooValue%'
     * </code>
     *
     * @param     string $fulltext The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3ContentQuery The current query, for fluid interface
     */
    public function filterByFulltext($fulltext = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($fulltext)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $fulltext)) {
                $fulltext = str_replace('*', '%', $fulltext);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(Jm3ContentPeer::FULLTEXT, $fulltext, $comparison);
    }

    /**
     * Filter the query on the state column
     *
     * Example usage:
     * <code>
     * $query->filterByState(1234); // WHERE state = 1234
     * $query->filterByState(array(12, 34)); // WHERE state IN (12, 34)
     * $query->filterByState(array('min' => 12)); // WHERE state >= 12
     * $query->filterByState(array('max' => 12)); // WHERE state <= 12
     * </code>
     *
     * @param     mixed $state The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3ContentQuery The current query, for fluid interface
     */
    public function filterByState($state = null, $comparison = null)
    {
        if (is_array($state)) {
            $useMinMax = false;
            if (isset($state['min'])) {
                $this->addUsingAlias(Jm3ContentPeer::STATE, $state['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($state['max'])) {
                $this->addUsingAlias(Jm3ContentPeer::STATE, $state['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Jm3ContentPeer::STATE, $state, $comparison);
    }

    /**
     * Filter the query on the catid column
     *
     * Example usage:
     * <code>
     * $query->filterByCatid(1234); // WHERE catid = 1234
     * $query->filterByCatid(array(12, 34)); // WHERE catid IN (12, 34)
     * $query->filterByCatid(array('min' => 12)); // WHERE catid >= 12
     * $query->filterByCatid(array('max' => 12)); // WHERE catid <= 12
     * </code>
     *
     * @param     mixed $catid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3ContentQuery The current query, for fluid interface
     */
    public function filterByCatid($catid = null, $comparison = null)
    {
        if (is_array($catid)) {
            $useMinMax = false;
            if (isset($catid['min'])) {
                $this->addUsingAlias(Jm3ContentPeer::CATID, $catid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($catid['max'])) {
                $this->addUsingAlias(Jm3ContentPeer::CATID, $catid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Jm3ContentPeer::CATID, $catid, $comparison);
    }

    /**
     * Filter the query on the created column
     *
     * Example usage:
     * <code>
     * $query->filterByCreated('2011-03-14'); // WHERE created = '2011-03-14'
     * $query->filterByCreated('now'); // WHERE created = '2011-03-14'
     * $query->filterByCreated(array('max' => 'yesterday')); // WHERE created < '2011-03-13'
     * </code>
     *
     * @param     mixed $created The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3ContentQuery The current query, for fluid interface
     */
    public function filterByCreated($created = null, $comparison = null)
    {
        if (is_array($created)) {
            $useMinMax = false;
            if (isset($created['min'])) {
                $this->addUsingAlias(Jm3ContentPeer::CREATED, $created['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($created['max'])) {
                $this->addUsingAlias(Jm3ContentPeer::CREATED, $created['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Jm3ContentPeer::CREATED, $created, $comparison);
    }

    /**
     * Filter the query on the created_by column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedBy(1234); // WHERE created_by = 1234
     * $query->filterByCreatedBy(array(12, 34)); // WHERE created_by IN (12, 34)
     * $query->filterByCreatedBy(array('min' => 12)); // WHERE created_by >= 12
     * $query->filterByCreatedBy(array('max' => 12)); // WHERE created_by <= 12
     * </code>
     *
     * @param     mixed $createdBy The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3ContentQuery The current query, for fluid interface
     */
    public function filterByCreatedBy($createdBy = null, $comparison = null)
    {
        if (is_array($createdBy)) {
            $useMinMax = false;
            if (isset($createdBy['min'])) {
                $this->addUsingAlias(Jm3ContentPeer::CREATED_BY, $createdBy['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdBy['max'])) {
                $this->addUsingAlias(Jm3ContentPeer::CREATED_BY, $createdBy['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Jm3ContentPeer::CREATED_BY, $createdBy, $comparison);
    }

    /**
     * Filter the query on the created_by_alias column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedByAlias('fooValue');   // WHERE created_by_alias = 'fooValue'
     * $query->filterByCreatedByAlias('%fooValue%'); // WHERE created_by_alias LIKE '%fooValue%'
     * </code>
     *
     * @param     string $createdByAlias The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3ContentQuery The current query, for fluid interface
     */
    public function filterByCreatedByAlias($createdByAlias = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($createdByAlias)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $createdByAlias)) {
                $createdByAlias = str_replace('*', '%', $createdByAlias);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(Jm3ContentPeer::CREATED_BY_ALIAS, $createdByAlias, $comparison);
    }

    /**
     * Filter the query on the modified column
     *
     * Example usage:
     * <code>
     * $query->filterByModified('2011-03-14'); // WHERE modified = '2011-03-14'
     * $query->filterByModified('now'); // WHERE modified = '2011-03-14'
     * $query->filterByModified(array('max' => 'yesterday')); // WHERE modified < '2011-03-13'
     * </code>
     *
     * @param     mixed $modified The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3ContentQuery The current query, for fluid interface
     */
    public function filterByModified($modified = null, $comparison = null)
    {
        if (is_array($modified)) {
            $useMinMax = false;
            if (isset($modified['min'])) {
                $this->addUsingAlias(Jm3ContentPeer::MODIFIED, $modified['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($modified['max'])) {
                $this->addUsingAlias(Jm3ContentPeer::MODIFIED, $modified['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Jm3ContentPeer::MODIFIED, $modified, $comparison);
    }

    /**
     * Filter the query on the modified_by column
     *
     * Example usage:
     * <code>
     * $query->filterByModifiedBy(1234); // WHERE modified_by = 1234
     * $query->filterByModifiedBy(array(12, 34)); // WHERE modified_by IN (12, 34)
     * $query->filterByModifiedBy(array('min' => 12)); // WHERE modified_by >= 12
     * $query->filterByModifiedBy(array('max' => 12)); // WHERE modified_by <= 12
     * </code>
     *
     * @param     mixed $modifiedBy The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3ContentQuery The current query, for fluid interface
     */
    public function filterByModifiedBy($modifiedBy = null, $comparison = null)
    {
        if (is_array($modifiedBy)) {
            $useMinMax = false;
            if (isset($modifiedBy['min'])) {
                $this->addUsingAlias(Jm3ContentPeer::MODIFIED_BY, $modifiedBy['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($modifiedBy['max'])) {
                $this->addUsingAlias(Jm3ContentPeer::MODIFIED_BY, $modifiedBy['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Jm3ContentPeer::MODIFIED_BY, $modifiedBy, $comparison);
    }

    /**
     * Filter the query on the checked_out column
     *
     * Example usage:
     * <code>
     * $query->filterByCheckedOut(1234); // WHERE checked_out = 1234
     * $query->filterByCheckedOut(array(12, 34)); // WHERE checked_out IN (12, 34)
     * $query->filterByCheckedOut(array('min' => 12)); // WHERE checked_out >= 12
     * $query->filterByCheckedOut(array('max' => 12)); // WHERE checked_out <= 12
     * </code>
     *
     * @param     mixed $checkedOut The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3ContentQuery The current query, for fluid interface
     */
    public function filterByCheckedOut($checkedOut = null, $comparison = null)
    {
        if (is_array($checkedOut)) {
            $useMinMax = false;
            if (isset($checkedOut['min'])) {
                $this->addUsingAlias(Jm3ContentPeer::CHECKED_OUT, $checkedOut['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($checkedOut['max'])) {
                $this->addUsingAlias(Jm3ContentPeer::CHECKED_OUT, $checkedOut['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Jm3ContentPeer::CHECKED_OUT, $checkedOut, $comparison);
    }

    /**
     * Filter the query on the checked_out_time column
     *
     * Example usage:
     * <code>
     * $query->filterByCheckedOutTime('2011-03-14'); // WHERE checked_out_time = '2011-03-14'
     * $query->filterByCheckedOutTime('now'); // WHERE checked_out_time = '2011-03-14'
     * $query->filterByCheckedOutTime(array('max' => 'yesterday')); // WHERE checked_out_time < '2011-03-13'
     * </code>
     *
     * @param     mixed $checkedOutTime The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3ContentQuery The current query, for fluid interface
     */
    public function filterByCheckedOutTime($checkedOutTime = null, $comparison = null)
    {
        if (is_array($checkedOutTime)) {
            $useMinMax = false;
            if (isset($checkedOutTime['min'])) {
                $this->addUsingAlias(Jm3ContentPeer::CHECKED_OUT_TIME, $checkedOutTime['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($checkedOutTime['max'])) {
                $this->addUsingAlias(Jm3ContentPeer::CHECKED_OUT_TIME, $checkedOutTime['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Jm3ContentPeer::CHECKED_OUT_TIME, $checkedOutTime, $comparison);
    }

    /**
     * Filter the query on the publish_up column
     *
     * Example usage:
     * <code>
     * $query->filterByPublishUp('2011-03-14'); // WHERE publish_up = '2011-03-14'
     * $query->filterByPublishUp('now'); // WHERE publish_up = '2011-03-14'
     * $query->filterByPublishUp(array('max' => 'yesterday')); // WHERE publish_up < '2011-03-13'
     * </code>
     *
     * @param     mixed $publishUp The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3ContentQuery The current query, for fluid interface
     */
    public function filterByPublishUp($publishUp = null, $comparison = null)
    {
        if (is_array($publishUp)) {
            $useMinMax = false;
            if (isset($publishUp['min'])) {
                $this->addUsingAlias(Jm3ContentPeer::PUBLISH_UP, $publishUp['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($publishUp['max'])) {
                $this->addUsingAlias(Jm3ContentPeer::PUBLISH_UP, $publishUp['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Jm3ContentPeer::PUBLISH_UP, $publishUp, $comparison);
    }

    /**
     * Filter the query on the publish_down column
     *
     * Example usage:
     * <code>
     * $query->filterByPublishDown('2011-03-14'); // WHERE publish_down = '2011-03-14'
     * $query->filterByPublishDown('now'); // WHERE publish_down = '2011-03-14'
     * $query->filterByPublishDown(array('max' => 'yesterday')); // WHERE publish_down < '2011-03-13'
     * </code>
     *
     * @param     mixed $publishDown The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3ContentQuery The current query, for fluid interface
     */
    public function filterByPublishDown($publishDown = null, $comparison = null)
    {
        if (is_array($publishDown)) {
            $useMinMax = false;
            if (isset($publishDown['min'])) {
                $this->addUsingAlias(Jm3ContentPeer::PUBLISH_DOWN, $publishDown['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($publishDown['max'])) {
                $this->addUsingAlias(Jm3ContentPeer::PUBLISH_DOWN, $publishDown['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Jm3ContentPeer::PUBLISH_DOWN, $publishDown, $comparison);
    }

    /**
     * Filter the query on the images column
     *
     * Example usage:
     * <code>
     * $query->filterByImages('fooValue');   // WHERE images = 'fooValue'
     * $query->filterByImages('%fooValue%'); // WHERE images LIKE '%fooValue%'
     * </code>
     *
     * @param     string $images The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3ContentQuery The current query, for fluid interface
     */
    public function filterByImages($images = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($images)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $images)) {
                $images = str_replace('*', '%', $images);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(Jm3ContentPeer::IMAGES, $images, $comparison);
    }

    /**
     * Filter the query on the urls column
     *
     * Example usage:
     * <code>
     * $query->filterByUrls('fooValue');   // WHERE urls = 'fooValue'
     * $query->filterByUrls('%fooValue%'); // WHERE urls LIKE '%fooValue%'
     * </code>
     *
     * @param     string $urls The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3ContentQuery The current query, for fluid interface
     */
    public function filterByUrls($urls = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($urls)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $urls)) {
                $urls = str_replace('*', '%', $urls);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(Jm3ContentPeer::URLS, $urls, $comparison);
    }

    /**
     * Filter the query on the attribs column
     *
     * Example usage:
     * <code>
     * $query->filterByAttribs('fooValue');   // WHERE attribs = 'fooValue'
     * $query->filterByAttribs('%fooValue%'); // WHERE attribs LIKE '%fooValue%'
     * </code>
     *
     * @param     string $attribs The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3ContentQuery The current query, for fluid interface
     */
    public function filterByAttribs($attribs = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($attribs)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $attribs)) {
                $attribs = str_replace('*', '%', $attribs);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(Jm3ContentPeer::ATTRIBS, $attribs, $comparison);
    }

    /**
     * Filter the query on the version column
     *
     * Example usage:
     * <code>
     * $query->filterByVersion(1234); // WHERE version = 1234
     * $query->filterByVersion(array(12, 34)); // WHERE version IN (12, 34)
     * $query->filterByVersion(array('min' => 12)); // WHERE version >= 12
     * $query->filterByVersion(array('max' => 12)); // WHERE version <= 12
     * </code>
     *
     * @param     mixed $version The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3ContentQuery The current query, for fluid interface
     */
    public function filterByVersion($version = null, $comparison = null)
    {
        if (is_array($version)) {
            $useMinMax = false;
            if (isset($version['min'])) {
                $this->addUsingAlias(Jm3ContentPeer::VERSION, $version['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($version['max'])) {
                $this->addUsingAlias(Jm3ContentPeer::VERSION, $version['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Jm3ContentPeer::VERSION, $version, $comparison);
    }

    /**
     * Filter the query on the ordering column
     *
     * Example usage:
     * <code>
     * $query->filterByOrdering(1234); // WHERE ordering = 1234
     * $query->filterByOrdering(array(12, 34)); // WHERE ordering IN (12, 34)
     * $query->filterByOrdering(array('min' => 12)); // WHERE ordering >= 12
     * $query->filterByOrdering(array('max' => 12)); // WHERE ordering <= 12
     * </code>
     *
     * @param     mixed $ordering The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3ContentQuery The current query, for fluid interface
     */
    public function filterByOrdering($ordering = null, $comparison = null)
    {
        if (is_array($ordering)) {
            $useMinMax = false;
            if (isset($ordering['min'])) {
                $this->addUsingAlias(Jm3ContentPeer::ORDERING, $ordering['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($ordering['max'])) {
                $this->addUsingAlias(Jm3ContentPeer::ORDERING, $ordering['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Jm3ContentPeer::ORDERING, $ordering, $comparison);
    }

    /**
     * Filter the query on the metakey column
     *
     * Example usage:
     * <code>
     * $query->filterByMetakey('fooValue');   // WHERE metakey = 'fooValue'
     * $query->filterByMetakey('%fooValue%'); // WHERE metakey LIKE '%fooValue%'
     * </code>
     *
     * @param     string $metakey The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3ContentQuery The current query, for fluid interface
     */
    public function filterByMetakey($metakey = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($metakey)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $metakey)) {
                $metakey = str_replace('*', '%', $metakey);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(Jm3ContentPeer::METAKEY, $metakey, $comparison);
    }

    /**
     * Filter the query on the metadesc column
     *
     * Example usage:
     * <code>
     * $query->filterByMetadesc('fooValue');   // WHERE metadesc = 'fooValue'
     * $query->filterByMetadesc('%fooValue%'); // WHERE metadesc LIKE '%fooValue%'
     * </code>
     *
     * @param     string $metadesc The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3ContentQuery The current query, for fluid interface
     */
    public function filterByMetadesc($metadesc = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($metadesc)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $metadesc)) {
                $metadesc = str_replace('*', '%', $metadesc);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(Jm3ContentPeer::METADESC, $metadesc, $comparison);
    }

    /**
     * Filter the query on the access column
     *
     * Example usage:
     * <code>
     * $query->filterByAccess(1234); // WHERE access = 1234
     * $query->filterByAccess(array(12, 34)); // WHERE access IN (12, 34)
     * $query->filterByAccess(array('min' => 12)); // WHERE access >= 12
     * $query->filterByAccess(array('max' => 12)); // WHERE access <= 12
     * </code>
     *
     * @param     mixed $access The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3ContentQuery The current query, for fluid interface
     */
    public function filterByAccess($access = null, $comparison = null)
    {
        if (is_array($access)) {
            $useMinMax = false;
            if (isset($access['min'])) {
                $this->addUsingAlias(Jm3ContentPeer::ACCESS, $access['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($access['max'])) {
                $this->addUsingAlias(Jm3ContentPeer::ACCESS, $access['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Jm3ContentPeer::ACCESS, $access, $comparison);
    }

    /**
     * Filter the query on the hits column
     *
     * Example usage:
     * <code>
     * $query->filterByHits(1234); // WHERE hits = 1234
     * $query->filterByHits(array(12, 34)); // WHERE hits IN (12, 34)
     * $query->filterByHits(array('min' => 12)); // WHERE hits >= 12
     * $query->filterByHits(array('max' => 12)); // WHERE hits <= 12
     * </code>
     *
     * @param     mixed $hits The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3ContentQuery The current query, for fluid interface
     */
    public function filterByHits($hits = null, $comparison = null)
    {
        if (is_array($hits)) {
            $useMinMax = false;
            if (isset($hits['min'])) {
                $this->addUsingAlias(Jm3ContentPeer::HITS, $hits['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($hits['max'])) {
                $this->addUsingAlias(Jm3ContentPeer::HITS, $hits['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Jm3ContentPeer::HITS, $hits, $comparison);
    }

    /**
     * Filter the query on the metadata column
     *
     * Example usage:
     * <code>
     * $query->filterByMetadata('fooValue');   // WHERE metadata = 'fooValue'
     * $query->filterByMetadata('%fooValue%'); // WHERE metadata LIKE '%fooValue%'
     * </code>
     *
     * @param     string $metadata The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3ContentQuery The current query, for fluid interface
     */
    public function filterByMetadata($metadata = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($metadata)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $metadata)) {
                $metadata = str_replace('*', '%', $metadata);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(Jm3ContentPeer::METADATA, $metadata, $comparison);
    }

    /**
     * Filter the query on the featured column
     *
     * Example usage:
     * <code>
     * $query->filterByFeatured(1234); // WHERE featured = 1234
     * $query->filterByFeatured(array(12, 34)); // WHERE featured IN (12, 34)
     * $query->filterByFeatured(array('min' => 12)); // WHERE featured >= 12
     * $query->filterByFeatured(array('max' => 12)); // WHERE featured <= 12
     * </code>
     *
     * @param     mixed $featured The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3ContentQuery The current query, for fluid interface
     */
    public function filterByFeatured($featured = null, $comparison = null)
    {
        if (is_array($featured)) {
            $useMinMax = false;
            if (isset($featured['min'])) {
                $this->addUsingAlias(Jm3ContentPeer::FEATURED, $featured['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($featured['max'])) {
                $this->addUsingAlias(Jm3ContentPeer::FEATURED, $featured['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Jm3ContentPeer::FEATURED, $featured, $comparison);
    }

    /**
     * Filter the query on the language column
     *
     * Example usage:
     * <code>
     * $query->filterByLanguage('fooValue');   // WHERE language = 'fooValue'
     * $query->filterByLanguage('%fooValue%'); // WHERE language LIKE '%fooValue%'
     * </code>
     *
     * @param     string $language The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3ContentQuery The current query, for fluid interface
     */
    public function filterByLanguage($language = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($language)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $language)) {
                $language = str_replace('*', '%', $language);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(Jm3ContentPeer::LANGUAGE, $language, $comparison);
    }

    /**
     * Filter the query on the xreference column
     *
     * Example usage:
     * <code>
     * $query->filterByXreference('fooValue');   // WHERE xreference = 'fooValue'
     * $query->filterByXreference('%fooValue%'); // WHERE xreference LIKE '%fooValue%'
     * </code>
     *
     * @param     string $xreference The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3ContentQuery The current query, for fluid interface
     */
    public function filterByXreference($xreference = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($xreference)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $xreference)) {
                $xreference = str_replace('*', '%', $xreference);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(Jm3ContentPeer::XREFERENCE, $xreference, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   Jm3Content $jm3Content Object to remove from the list of results
     *
     * @return Jm3ContentQuery The current query, for fluid interface
     */
    public function prune($jm3Content = null)
    {
        if ($jm3Content) {
            $this->addUsingAlias(Jm3ContentPeer::ID, $jm3Content->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
