<?php


/**
 * Base class that represents a query for the 'jm3_categories' table.
 *
 *
 *
 * @method Jm3CategoriesQuery orderById($order = Criteria::ASC) Order by the id column
 * @method Jm3CategoriesQuery orderByAssetId($order = Criteria::ASC) Order by the asset_id column
 * @method Jm3CategoriesQuery orderByParentId($order = Criteria::ASC) Order by the parent_id column
 * @method Jm3CategoriesQuery orderByLft($order = Criteria::ASC) Order by the lft column
 * @method Jm3CategoriesQuery orderByRgt($order = Criteria::ASC) Order by the rgt column
 * @method Jm3CategoriesQuery orderByLevel($order = Criteria::ASC) Order by the level column
 * @method Jm3CategoriesQuery orderByPath($order = Criteria::ASC) Order by the path column
 * @method Jm3CategoriesQuery orderByExtension($order = Criteria::ASC) Order by the extension column
 * @method Jm3CategoriesQuery orderByTitle($order = Criteria::ASC) Order by the title column
 * @method Jm3CategoriesQuery orderByAlias($order = Criteria::ASC) Order by the alias column
 * @method Jm3CategoriesQuery orderByNote($order = Criteria::ASC) Order by the note column
 * @method Jm3CategoriesQuery orderByDescription($order = Criteria::ASC) Order by the description column
 * @method Jm3CategoriesQuery orderByPublished($order = Criteria::ASC) Order by the published column
 * @method Jm3CategoriesQuery orderByCheckedOut($order = Criteria::ASC) Order by the checked_out column
 * @method Jm3CategoriesQuery orderByCheckedOutTime($order = Criteria::ASC) Order by the checked_out_time column
 * @method Jm3CategoriesQuery orderByAccess($order = Criteria::ASC) Order by the access column
 * @method Jm3CategoriesQuery orderByParams($order = Criteria::ASC) Order by the params column
 * @method Jm3CategoriesQuery orderByMetadesc($order = Criteria::ASC) Order by the metadesc column
 * @method Jm3CategoriesQuery orderByMetakey($order = Criteria::ASC) Order by the metakey column
 * @method Jm3CategoriesQuery orderByMetadata($order = Criteria::ASC) Order by the metadata column
 * @method Jm3CategoriesQuery orderByCreatedUserId($order = Criteria::ASC) Order by the created_user_id column
 * @method Jm3CategoriesQuery orderByCreatedTime($order = Criteria::ASC) Order by the created_time column
 * @method Jm3CategoriesQuery orderByModifiedUserId($order = Criteria::ASC) Order by the modified_user_id column
 * @method Jm3CategoriesQuery orderByModifiedTime($order = Criteria::ASC) Order by the modified_time column
 * @method Jm3CategoriesQuery orderByHits($order = Criteria::ASC) Order by the hits column
 * @method Jm3CategoriesQuery orderByLanguage($order = Criteria::ASC) Order by the language column
 * @method Jm3CategoriesQuery orderByVersion($order = Criteria::ASC) Order by the version column
 *
 * @method Jm3CategoriesQuery groupById() Group by the id column
 * @method Jm3CategoriesQuery groupByAssetId() Group by the asset_id column
 * @method Jm3CategoriesQuery groupByParentId() Group by the parent_id column
 * @method Jm3CategoriesQuery groupByLft() Group by the lft column
 * @method Jm3CategoriesQuery groupByRgt() Group by the rgt column
 * @method Jm3CategoriesQuery groupByLevel() Group by the level column
 * @method Jm3CategoriesQuery groupByPath() Group by the path column
 * @method Jm3CategoriesQuery groupByExtension() Group by the extension column
 * @method Jm3CategoriesQuery groupByTitle() Group by the title column
 * @method Jm3CategoriesQuery groupByAlias() Group by the alias column
 * @method Jm3CategoriesQuery groupByNote() Group by the note column
 * @method Jm3CategoriesQuery groupByDescription() Group by the description column
 * @method Jm3CategoriesQuery groupByPublished() Group by the published column
 * @method Jm3CategoriesQuery groupByCheckedOut() Group by the checked_out column
 * @method Jm3CategoriesQuery groupByCheckedOutTime() Group by the checked_out_time column
 * @method Jm3CategoriesQuery groupByAccess() Group by the access column
 * @method Jm3CategoriesQuery groupByParams() Group by the params column
 * @method Jm3CategoriesQuery groupByMetadesc() Group by the metadesc column
 * @method Jm3CategoriesQuery groupByMetakey() Group by the metakey column
 * @method Jm3CategoriesQuery groupByMetadata() Group by the metadata column
 * @method Jm3CategoriesQuery groupByCreatedUserId() Group by the created_user_id column
 * @method Jm3CategoriesQuery groupByCreatedTime() Group by the created_time column
 * @method Jm3CategoriesQuery groupByModifiedUserId() Group by the modified_user_id column
 * @method Jm3CategoriesQuery groupByModifiedTime() Group by the modified_time column
 * @method Jm3CategoriesQuery groupByHits() Group by the hits column
 * @method Jm3CategoriesQuery groupByLanguage() Group by the language column
 * @method Jm3CategoriesQuery groupByVersion() Group by the version column
 *
 * @method Jm3CategoriesQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method Jm3CategoriesQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method Jm3CategoriesQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method Jm3Categories findOne(PropelPDO $con = null) Return the first Jm3Categories matching the query
 * @method Jm3Categories findOneOrCreate(PropelPDO $con = null) Return the first Jm3Categories matching the query, or a new Jm3Categories object populated from the query conditions when no match is found
 *
 * @method Jm3Categories findOneByAssetId(int $asset_id) Return the first Jm3Categories filtered by the asset_id column
 * @method Jm3Categories findOneByParentId(int $parent_id) Return the first Jm3Categories filtered by the parent_id column
 * @method Jm3Categories findOneByLft(int $lft) Return the first Jm3Categories filtered by the lft column
 * @method Jm3Categories findOneByRgt(int $rgt) Return the first Jm3Categories filtered by the rgt column
 * @method Jm3Categories findOneByLevel(int $level) Return the first Jm3Categories filtered by the level column
 * @method Jm3Categories findOneByPath(string $path) Return the first Jm3Categories filtered by the path column
 * @method Jm3Categories findOneByExtension(string $extension) Return the first Jm3Categories filtered by the extension column
 * @method Jm3Categories findOneByTitle(string $title) Return the first Jm3Categories filtered by the title column
 * @method Jm3Categories findOneByAlias(string $alias) Return the first Jm3Categories filtered by the alias column
 * @method Jm3Categories findOneByNote(string $note) Return the first Jm3Categories filtered by the note column
 * @method Jm3Categories findOneByDescription(string $description) Return the first Jm3Categories filtered by the description column
 * @method Jm3Categories findOneByPublished(boolean $published) Return the first Jm3Categories filtered by the published column
 * @method Jm3Categories findOneByCheckedOut(int $checked_out) Return the first Jm3Categories filtered by the checked_out column
 * @method Jm3Categories findOneByCheckedOutTime(string $checked_out_time) Return the first Jm3Categories filtered by the checked_out_time column
 * @method Jm3Categories findOneByAccess(int $access) Return the first Jm3Categories filtered by the access column
 * @method Jm3Categories findOneByParams(string $params) Return the first Jm3Categories filtered by the params column
 * @method Jm3Categories findOneByMetadesc(string $metadesc) Return the first Jm3Categories filtered by the metadesc column
 * @method Jm3Categories findOneByMetakey(string $metakey) Return the first Jm3Categories filtered by the metakey column
 * @method Jm3Categories findOneByMetadata(string $metadata) Return the first Jm3Categories filtered by the metadata column
 * @method Jm3Categories findOneByCreatedUserId(int $created_user_id) Return the first Jm3Categories filtered by the created_user_id column
 * @method Jm3Categories findOneByCreatedTime(string $created_time) Return the first Jm3Categories filtered by the created_time column
 * @method Jm3Categories findOneByModifiedUserId(int $modified_user_id) Return the first Jm3Categories filtered by the modified_user_id column
 * @method Jm3Categories findOneByModifiedTime(string $modified_time) Return the first Jm3Categories filtered by the modified_time column
 * @method Jm3Categories findOneByHits(int $hits) Return the first Jm3Categories filtered by the hits column
 * @method Jm3Categories findOneByLanguage(string $language) Return the first Jm3Categories filtered by the language column
 * @method Jm3Categories findOneByVersion(int $version) Return the first Jm3Categories filtered by the version column
 *
 * @method array findById(int $id) Return Jm3Categories objects filtered by the id column
 * @method array findByAssetId(int $asset_id) Return Jm3Categories objects filtered by the asset_id column
 * @method array findByParentId(int $parent_id) Return Jm3Categories objects filtered by the parent_id column
 * @method array findByLft(int $lft) Return Jm3Categories objects filtered by the lft column
 * @method array findByRgt(int $rgt) Return Jm3Categories objects filtered by the rgt column
 * @method array findByLevel(int $level) Return Jm3Categories objects filtered by the level column
 * @method array findByPath(string $path) Return Jm3Categories objects filtered by the path column
 * @method array findByExtension(string $extension) Return Jm3Categories objects filtered by the extension column
 * @method array findByTitle(string $title) Return Jm3Categories objects filtered by the title column
 * @method array findByAlias(string $alias) Return Jm3Categories objects filtered by the alias column
 * @method array findByNote(string $note) Return Jm3Categories objects filtered by the note column
 * @method array findByDescription(string $description) Return Jm3Categories objects filtered by the description column
 * @method array findByPublished(boolean $published) Return Jm3Categories objects filtered by the published column
 * @method array findByCheckedOut(int $checked_out) Return Jm3Categories objects filtered by the checked_out column
 * @method array findByCheckedOutTime(string $checked_out_time) Return Jm3Categories objects filtered by the checked_out_time column
 * @method array findByAccess(int $access) Return Jm3Categories objects filtered by the access column
 * @method array findByParams(string $params) Return Jm3Categories objects filtered by the params column
 * @method array findByMetadesc(string $metadesc) Return Jm3Categories objects filtered by the metadesc column
 * @method array findByMetakey(string $metakey) Return Jm3Categories objects filtered by the metakey column
 * @method array findByMetadata(string $metadata) Return Jm3Categories objects filtered by the metadata column
 * @method array findByCreatedUserId(int $created_user_id) Return Jm3Categories objects filtered by the created_user_id column
 * @method array findByCreatedTime(string $created_time) Return Jm3Categories objects filtered by the created_time column
 * @method array findByModifiedUserId(int $modified_user_id) Return Jm3Categories objects filtered by the modified_user_id column
 * @method array findByModifiedTime(string $modified_time) Return Jm3Categories objects filtered by the modified_time column
 * @method array findByHits(int $hits) Return Jm3Categories objects filtered by the hits column
 * @method array findByLanguage(string $language) Return Jm3Categories objects filtered by the language column
 * @method array findByVersion(int $version) Return Jm3Categories objects filtered by the version column
 *
 * @package    propel.generator.bookstore.om
 */
abstract class BaseJm3CategoriesQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseJm3CategoriesQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'bookstore';
        }
        if (null === $modelName) {
            $modelName = 'Jm3Categories';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new Jm3CategoriesQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   Jm3CategoriesQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return Jm3CategoriesQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof Jm3CategoriesQuery) {
            return $criteria;
        }
        $query = new Jm3CategoriesQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Jm3Categories|Jm3Categories[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = Jm3CategoriesPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(Jm3CategoriesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Jm3Categories A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Jm3Categories A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `asset_id`, `parent_id`, `lft`, `rgt`, `level`, `path`, `extension`, `title`, `alias`, `note`, `description`, `published`, `checked_out`, `checked_out_time`, `access`, `params`, `metadesc`, `metakey`, `metadata`, `created_user_id`, `created_time`, `modified_user_id`, `modified_time`, `hits`, `language`, `version` FROM `jm3_categories` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Jm3Categories();
            $obj->hydrate($row);
            Jm3CategoriesPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Jm3Categories|Jm3Categories[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Jm3Categories[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return Jm3CategoriesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(Jm3CategoriesPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return Jm3CategoriesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(Jm3CategoriesPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3CategoriesQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(Jm3CategoriesPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(Jm3CategoriesPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Jm3CategoriesPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the asset_id column
     *
     * Example usage:
     * <code>
     * $query->filterByAssetId(1234); // WHERE asset_id = 1234
     * $query->filterByAssetId(array(12, 34)); // WHERE asset_id IN (12, 34)
     * $query->filterByAssetId(array('min' => 12)); // WHERE asset_id >= 12
     * $query->filterByAssetId(array('max' => 12)); // WHERE asset_id <= 12
     * </code>
     *
     * @param     mixed $assetId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3CategoriesQuery The current query, for fluid interface
     */
    public function filterByAssetId($assetId = null, $comparison = null)
    {
        if (is_array($assetId)) {
            $useMinMax = false;
            if (isset($assetId['min'])) {
                $this->addUsingAlias(Jm3CategoriesPeer::ASSET_ID, $assetId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($assetId['max'])) {
                $this->addUsingAlias(Jm3CategoriesPeer::ASSET_ID, $assetId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Jm3CategoriesPeer::ASSET_ID, $assetId, $comparison);
    }

    /**
     * Filter the query on the parent_id column
     *
     * Example usage:
     * <code>
     * $query->filterByParentId(1234); // WHERE parent_id = 1234
     * $query->filterByParentId(array(12, 34)); // WHERE parent_id IN (12, 34)
     * $query->filterByParentId(array('min' => 12)); // WHERE parent_id >= 12
     * $query->filterByParentId(array('max' => 12)); // WHERE parent_id <= 12
     * </code>
     *
     * @param     mixed $parentId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3CategoriesQuery The current query, for fluid interface
     */
    public function filterByParentId($parentId = null, $comparison = null)
    {
        if (is_array($parentId)) {
            $useMinMax = false;
            if (isset($parentId['min'])) {
                $this->addUsingAlias(Jm3CategoriesPeer::PARENT_ID, $parentId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($parentId['max'])) {
                $this->addUsingAlias(Jm3CategoriesPeer::PARENT_ID, $parentId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Jm3CategoriesPeer::PARENT_ID, $parentId, $comparison);
    }

    /**
     * Filter the query on the lft column
     *
     * Example usage:
     * <code>
     * $query->filterByLft(1234); // WHERE lft = 1234
     * $query->filterByLft(array(12, 34)); // WHERE lft IN (12, 34)
     * $query->filterByLft(array('min' => 12)); // WHERE lft >= 12
     * $query->filterByLft(array('max' => 12)); // WHERE lft <= 12
     * </code>
     *
     * @param     mixed $lft The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3CategoriesQuery The current query, for fluid interface
     */
    public function filterByLft($lft = null, $comparison = null)
    {
        if (is_array($lft)) {
            $useMinMax = false;
            if (isset($lft['min'])) {
                $this->addUsingAlias(Jm3CategoriesPeer::LFT, $lft['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lft['max'])) {
                $this->addUsingAlias(Jm3CategoriesPeer::LFT, $lft['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Jm3CategoriesPeer::LFT, $lft, $comparison);
    }

    /**
     * Filter the query on the rgt column
     *
     * Example usage:
     * <code>
     * $query->filterByRgt(1234); // WHERE rgt = 1234
     * $query->filterByRgt(array(12, 34)); // WHERE rgt IN (12, 34)
     * $query->filterByRgt(array('min' => 12)); // WHERE rgt >= 12
     * $query->filterByRgt(array('max' => 12)); // WHERE rgt <= 12
     * </code>
     *
     * @param     mixed $rgt The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3CategoriesQuery The current query, for fluid interface
     */
    public function filterByRgt($rgt = null, $comparison = null)
    {
        if (is_array($rgt)) {
            $useMinMax = false;
            if (isset($rgt['min'])) {
                $this->addUsingAlias(Jm3CategoriesPeer::RGT, $rgt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rgt['max'])) {
                $this->addUsingAlias(Jm3CategoriesPeer::RGT, $rgt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Jm3CategoriesPeer::RGT, $rgt, $comparison);
    }

    /**
     * Filter the query on the level column
     *
     * Example usage:
     * <code>
     * $query->filterByLevel(1234); // WHERE level = 1234
     * $query->filterByLevel(array(12, 34)); // WHERE level IN (12, 34)
     * $query->filterByLevel(array('min' => 12)); // WHERE level >= 12
     * $query->filterByLevel(array('max' => 12)); // WHERE level <= 12
     * </code>
     *
     * @param     mixed $level The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3CategoriesQuery The current query, for fluid interface
     */
    public function filterByLevel($level = null, $comparison = null)
    {
        if (is_array($level)) {
            $useMinMax = false;
            if (isset($level['min'])) {
                $this->addUsingAlias(Jm3CategoriesPeer::LEVEL, $level['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($level['max'])) {
                $this->addUsingAlias(Jm3CategoriesPeer::LEVEL, $level['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Jm3CategoriesPeer::LEVEL, $level, $comparison);
    }

    /**
     * Filter the query on the path column
     *
     * Example usage:
     * <code>
     * $query->filterByPath('fooValue');   // WHERE path = 'fooValue'
     * $query->filterByPath('%fooValue%'); // WHERE path LIKE '%fooValue%'
     * </code>
     *
     * @param     string $path The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3CategoriesQuery The current query, for fluid interface
     */
    public function filterByPath($path = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($path)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $path)) {
                $path = str_replace('*', '%', $path);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(Jm3CategoriesPeer::PATH, $path, $comparison);
    }

    /**
     * Filter the query on the extension column
     *
     * Example usage:
     * <code>
     * $query->filterByExtension('fooValue');   // WHERE extension = 'fooValue'
     * $query->filterByExtension('%fooValue%'); // WHERE extension LIKE '%fooValue%'
     * </code>
     *
     * @param     string $extension The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3CategoriesQuery The current query, for fluid interface
     */
    public function filterByExtension($extension = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($extension)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $extension)) {
                $extension = str_replace('*', '%', $extension);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(Jm3CategoriesPeer::EXTENSION, $extension, $comparison);
    }

    /**
     * Filter the query on the title column
     *
     * Example usage:
     * <code>
     * $query->filterByTitle('fooValue');   // WHERE title = 'fooValue'
     * $query->filterByTitle('%fooValue%'); // WHERE title LIKE '%fooValue%'
     * </code>
     *
     * @param     string $title The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3CategoriesQuery The current query, for fluid interface
     */
    public function filterByTitle($title = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($title)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $title)) {
                $title = str_replace('*', '%', $title);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(Jm3CategoriesPeer::TITLE, $title, $comparison);
    }

    /**
     * Filter the query on the alias column
     *
     * Example usage:
     * <code>
     * $query->filterByAlias('fooValue');   // WHERE alias = 'fooValue'
     * $query->filterByAlias('%fooValue%'); // WHERE alias LIKE '%fooValue%'
     * </code>
     *
     * @param     string $alias The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3CategoriesQuery The current query, for fluid interface
     */
    public function filterByAlias($alias = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($alias)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $alias)) {
                $alias = str_replace('*', '%', $alias);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(Jm3CategoriesPeer::ALIAS, $alias, $comparison);
    }

    /**
     * Filter the query on the note column
     *
     * Example usage:
     * <code>
     * $query->filterByNote('fooValue');   // WHERE note = 'fooValue'
     * $query->filterByNote('%fooValue%'); // WHERE note LIKE '%fooValue%'
     * </code>
     *
     * @param     string $note The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3CategoriesQuery The current query, for fluid interface
     */
    public function filterByNote($note = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($note)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $note)) {
                $note = str_replace('*', '%', $note);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(Jm3CategoriesPeer::NOTE, $note, $comparison);
    }

    /**
     * Filter the query on the description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE description = 'fooValue'
     * $query->filterByDescription('%fooValue%'); // WHERE description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3CategoriesQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $description)) {
                $description = str_replace('*', '%', $description);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(Jm3CategoriesPeer::DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the published column
     *
     * Example usage:
     * <code>
     * $query->filterByPublished(true); // WHERE published = true
     * $query->filterByPublished('yes'); // WHERE published = true
     * </code>
     *
     * @param     boolean|string $published The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3CategoriesQuery The current query, for fluid interface
     */
    public function filterByPublished($published = null, $comparison = null)
    {
        if (is_string($published)) {
            $published = in_array(strtolower($published), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(Jm3CategoriesPeer::PUBLISHED, $published, $comparison);
    }

    /**
     * Filter the query on the checked_out column
     *
     * Example usage:
     * <code>
     * $query->filterByCheckedOut(1234); // WHERE checked_out = 1234
     * $query->filterByCheckedOut(array(12, 34)); // WHERE checked_out IN (12, 34)
     * $query->filterByCheckedOut(array('min' => 12)); // WHERE checked_out >= 12
     * $query->filterByCheckedOut(array('max' => 12)); // WHERE checked_out <= 12
     * </code>
     *
     * @param     mixed $checkedOut The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3CategoriesQuery The current query, for fluid interface
     */
    public function filterByCheckedOut($checkedOut = null, $comparison = null)
    {
        if (is_array($checkedOut)) {
            $useMinMax = false;
            if (isset($checkedOut['min'])) {
                $this->addUsingAlias(Jm3CategoriesPeer::CHECKED_OUT, $checkedOut['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($checkedOut['max'])) {
                $this->addUsingAlias(Jm3CategoriesPeer::CHECKED_OUT, $checkedOut['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Jm3CategoriesPeer::CHECKED_OUT, $checkedOut, $comparison);
    }

    /**
     * Filter the query on the checked_out_time column
     *
     * Example usage:
     * <code>
     * $query->filterByCheckedOutTime('2011-03-14'); // WHERE checked_out_time = '2011-03-14'
     * $query->filterByCheckedOutTime('now'); // WHERE checked_out_time = '2011-03-14'
     * $query->filterByCheckedOutTime(array('max' => 'yesterday')); // WHERE checked_out_time < '2011-03-13'
     * </code>
     *
     * @param     mixed $checkedOutTime The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3CategoriesQuery The current query, for fluid interface
     */
    public function filterByCheckedOutTime($checkedOutTime = null, $comparison = null)
    {
        if (is_array($checkedOutTime)) {
            $useMinMax = false;
            if (isset($checkedOutTime['min'])) {
                $this->addUsingAlias(Jm3CategoriesPeer::CHECKED_OUT_TIME, $checkedOutTime['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($checkedOutTime['max'])) {
                $this->addUsingAlias(Jm3CategoriesPeer::CHECKED_OUT_TIME, $checkedOutTime['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Jm3CategoriesPeer::CHECKED_OUT_TIME, $checkedOutTime, $comparison);
    }

    /**
     * Filter the query on the access column
     *
     * Example usage:
     * <code>
     * $query->filterByAccess(1234); // WHERE access = 1234
     * $query->filterByAccess(array(12, 34)); // WHERE access IN (12, 34)
     * $query->filterByAccess(array('min' => 12)); // WHERE access >= 12
     * $query->filterByAccess(array('max' => 12)); // WHERE access <= 12
     * </code>
     *
     * @param     mixed $access The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3CategoriesQuery The current query, for fluid interface
     */
    public function filterByAccess($access = null, $comparison = null)
    {
        if (is_array($access)) {
            $useMinMax = false;
            if (isset($access['min'])) {
                $this->addUsingAlias(Jm3CategoriesPeer::ACCESS, $access['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($access['max'])) {
                $this->addUsingAlias(Jm3CategoriesPeer::ACCESS, $access['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Jm3CategoriesPeer::ACCESS, $access, $comparison);
    }

    /**
     * Filter the query on the params column
     *
     * Example usage:
     * <code>
     * $query->filterByParams('fooValue');   // WHERE params = 'fooValue'
     * $query->filterByParams('%fooValue%'); // WHERE params LIKE '%fooValue%'
     * </code>
     *
     * @param     string $params The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3CategoriesQuery The current query, for fluid interface
     */
    public function filterByParams($params = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($params)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $params)) {
                $params = str_replace('*', '%', $params);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(Jm3CategoriesPeer::PARAMS, $params, $comparison);
    }

    /**
     * Filter the query on the metadesc column
     *
     * Example usage:
     * <code>
     * $query->filterByMetadesc('fooValue');   // WHERE metadesc = 'fooValue'
     * $query->filterByMetadesc('%fooValue%'); // WHERE metadesc LIKE '%fooValue%'
     * </code>
     *
     * @param     string $metadesc The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3CategoriesQuery The current query, for fluid interface
     */
    public function filterByMetadesc($metadesc = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($metadesc)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $metadesc)) {
                $metadesc = str_replace('*', '%', $metadesc);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(Jm3CategoriesPeer::METADESC, $metadesc, $comparison);
    }

    /**
     * Filter the query on the metakey column
     *
     * Example usage:
     * <code>
     * $query->filterByMetakey('fooValue');   // WHERE metakey = 'fooValue'
     * $query->filterByMetakey('%fooValue%'); // WHERE metakey LIKE '%fooValue%'
     * </code>
     *
     * @param     string $metakey The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3CategoriesQuery The current query, for fluid interface
     */
    public function filterByMetakey($metakey = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($metakey)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $metakey)) {
                $metakey = str_replace('*', '%', $metakey);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(Jm3CategoriesPeer::METAKEY, $metakey, $comparison);
    }

    /**
     * Filter the query on the metadata column
     *
     * Example usage:
     * <code>
     * $query->filterByMetadata('fooValue');   // WHERE metadata = 'fooValue'
     * $query->filterByMetadata('%fooValue%'); // WHERE metadata LIKE '%fooValue%'
     * </code>
     *
     * @param     string $metadata The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3CategoriesQuery The current query, for fluid interface
     */
    public function filterByMetadata($metadata = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($metadata)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $metadata)) {
                $metadata = str_replace('*', '%', $metadata);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(Jm3CategoriesPeer::METADATA, $metadata, $comparison);
    }

    /**
     * Filter the query on the created_user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedUserId(1234); // WHERE created_user_id = 1234
     * $query->filterByCreatedUserId(array(12, 34)); // WHERE created_user_id IN (12, 34)
     * $query->filterByCreatedUserId(array('min' => 12)); // WHERE created_user_id >= 12
     * $query->filterByCreatedUserId(array('max' => 12)); // WHERE created_user_id <= 12
     * </code>
     *
     * @param     mixed $createdUserId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3CategoriesQuery The current query, for fluid interface
     */
    public function filterByCreatedUserId($createdUserId = null, $comparison = null)
    {
        if (is_array($createdUserId)) {
            $useMinMax = false;
            if (isset($createdUserId['min'])) {
                $this->addUsingAlias(Jm3CategoriesPeer::CREATED_USER_ID, $createdUserId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdUserId['max'])) {
                $this->addUsingAlias(Jm3CategoriesPeer::CREATED_USER_ID, $createdUserId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Jm3CategoriesPeer::CREATED_USER_ID, $createdUserId, $comparison);
    }

    /**
     * Filter the query on the created_time column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedTime('2011-03-14'); // WHERE created_time = '2011-03-14'
     * $query->filterByCreatedTime('now'); // WHERE created_time = '2011-03-14'
     * $query->filterByCreatedTime(array('max' => 'yesterday')); // WHERE created_time < '2011-03-13'
     * </code>
     *
     * @param     mixed $createdTime The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3CategoriesQuery The current query, for fluid interface
     */
    public function filterByCreatedTime($createdTime = null, $comparison = null)
    {
        if (is_array($createdTime)) {
            $useMinMax = false;
            if (isset($createdTime['min'])) {
                $this->addUsingAlias(Jm3CategoriesPeer::CREATED_TIME, $createdTime['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdTime['max'])) {
                $this->addUsingAlias(Jm3CategoriesPeer::CREATED_TIME, $createdTime['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Jm3CategoriesPeer::CREATED_TIME, $createdTime, $comparison);
    }

    /**
     * Filter the query on the modified_user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByModifiedUserId(1234); // WHERE modified_user_id = 1234
     * $query->filterByModifiedUserId(array(12, 34)); // WHERE modified_user_id IN (12, 34)
     * $query->filterByModifiedUserId(array('min' => 12)); // WHERE modified_user_id >= 12
     * $query->filterByModifiedUserId(array('max' => 12)); // WHERE modified_user_id <= 12
     * </code>
     *
     * @param     mixed $modifiedUserId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3CategoriesQuery The current query, for fluid interface
     */
    public function filterByModifiedUserId($modifiedUserId = null, $comparison = null)
    {
        if (is_array($modifiedUserId)) {
            $useMinMax = false;
            if (isset($modifiedUserId['min'])) {
                $this->addUsingAlias(Jm3CategoriesPeer::MODIFIED_USER_ID, $modifiedUserId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($modifiedUserId['max'])) {
                $this->addUsingAlias(Jm3CategoriesPeer::MODIFIED_USER_ID, $modifiedUserId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Jm3CategoriesPeer::MODIFIED_USER_ID, $modifiedUserId, $comparison);
    }

    /**
     * Filter the query on the modified_time column
     *
     * Example usage:
     * <code>
     * $query->filterByModifiedTime('2011-03-14'); // WHERE modified_time = '2011-03-14'
     * $query->filterByModifiedTime('now'); // WHERE modified_time = '2011-03-14'
     * $query->filterByModifiedTime(array('max' => 'yesterday')); // WHERE modified_time < '2011-03-13'
     * </code>
     *
     * @param     mixed $modifiedTime The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3CategoriesQuery The current query, for fluid interface
     */
    public function filterByModifiedTime($modifiedTime = null, $comparison = null)
    {
        if (is_array($modifiedTime)) {
            $useMinMax = false;
            if (isset($modifiedTime['min'])) {
                $this->addUsingAlias(Jm3CategoriesPeer::MODIFIED_TIME, $modifiedTime['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($modifiedTime['max'])) {
                $this->addUsingAlias(Jm3CategoriesPeer::MODIFIED_TIME, $modifiedTime['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Jm3CategoriesPeer::MODIFIED_TIME, $modifiedTime, $comparison);
    }

    /**
     * Filter the query on the hits column
     *
     * Example usage:
     * <code>
     * $query->filterByHits(1234); // WHERE hits = 1234
     * $query->filterByHits(array(12, 34)); // WHERE hits IN (12, 34)
     * $query->filterByHits(array('min' => 12)); // WHERE hits >= 12
     * $query->filterByHits(array('max' => 12)); // WHERE hits <= 12
     * </code>
     *
     * @param     mixed $hits The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3CategoriesQuery The current query, for fluid interface
     */
    public function filterByHits($hits = null, $comparison = null)
    {
        if (is_array($hits)) {
            $useMinMax = false;
            if (isset($hits['min'])) {
                $this->addUsingAlias(Jm3CategoriesPeer::HITS, $hits['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($hits['max'])) {
                $this->addUsingAlias(Jm3CategoriesPeer::HITS, $hits['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Jm3CategoriesPeer::HITS, $hits, $comparison);
    }

    /**
     * Filter the query on the language column
     *
     * Example usage:
     * <code>
     * $query->filterByLanguage('fooValue');   // WHERE language = 'fooValue'
     * $query->filterByLanguage('%fooValue%'); // WHERE language LIKE '%fooValue%'
     * </code>
     *
     * @param     string $language The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3CategoriesQuery The current query, for fluid interface
     */
    public function filterByLanguage($language = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($language)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $language)) {
                $language = str_replace('*', '%', $language);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(Jm3CategoriesPeer::LANGUAGE, $language, $comparison);
    }

    /**
     * Filter the query on the version column
     *
     * Example usage:
     * <code>
     * $query->filterByVersion(1234); // WHERE version = 1234
     * $query->filterByVersion(array(12, 34)); // WHERE version IN (12, 34)
     * $query->filterByVersion(array('min' => 12)); // WHERE version >= 12
     * $query->filterByVersion(array('max' => 12)); // WHERE version <= 12
     * </code>
     *
     * @param     mixed $version The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3CategoriesQuery The current query, for fluid interface
     */
    public function filterByVersion($version = null, $comparison = null)
    {
        if (is_array($version)) {
            $useMinMax = false;
            if (isset($version['min'])) {
                $this->addUsingAlias(Jm3CategoriesPeer::VERSION, $version['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($version['max'])) {
                $this->addUsingAlias(Jm3CategoriesPeer::VERSION, $version['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Jm3CategoriesPeer::VERSION, $version, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   Jm3Categories $jm3Categories Object to remove from the list of results
     *
     * @return Jm3CategoriesQuery The current query, for fluid interface
     */
    public function prune($jm3Categories = null)
    {
        if ($jm3Categories) {
            $this->addUsingAlias(Jm3CategoriesPeer::ID, $jm3Categories->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
