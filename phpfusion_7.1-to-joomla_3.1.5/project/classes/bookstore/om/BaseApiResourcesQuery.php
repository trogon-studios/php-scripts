<?php


/**
 * Base class that represents a query for the 'api_resources' table.
 *
 *
 *
 * @method ApiResourcesQuery orderById($order = Criteria::ASC) Order by the id column
 * @method ApiResourcesQuery orderByResourcequantity($order = Criteria::ASC) Order by the resourceQuantity column
 * @method ApiResourcesQuery orderByResourceproduction($order = Criteria::ASC) Order by the resourceProduction column
 * @method ApiResourcesQuery orderByResourceResourcetype($order = Criteria::ASC) Order by the resource_resourceType column
 * @method ApiResourcesQuery orderByResourcePlot($order = Criteria::ASC) Order by the resource_plot column
 *
 * @method ApiResourcesQuery groupById() Group by the id column
 * @method ApiResourcesQuery groupByResourcequantity() Group by the resourceQuantity column
 * @method ApiResourcesQuery groupByResourceproduction() Group by the resourceProduction column
 * @method ApiResourcesQuery groupByResourceResourcetype() Group by the resource_resourceType column
 * @method ApiResourcesQuery groupByResourcePlot() Group by the resource_plot column
 *
 * @method ApiResourcesQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method ApiResourcesQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method ApiResourcesQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method ApiResources findOne(PropelPDO $con = null) Return the first ApiResources matching the query
 * @method ApiResources findOneOrCreate(PropelPDO $con = null) Return the first ApiResources matching the query, or a new ApiResources object populated from the query conditions when no match is found
 *
 * @method ApiResources findOneByResourcequantity(int $resourceQuantity) Return the first ApiResources filtered by the resourceQuantity column
 * @method ApiResources findOneByResourceproduction(string $resourceProduction) Return the first ApiResources filtered by the resourceProduction column
 * @method ApiResources findOneByResourceResourcetype(int $resource_resourceType) Return the first ApiResources filtered by the resource_resourceType column
 * @method ApiResources findOneByResourcePlot(string $resource_plot) Return the first ApiResources filtered by the resource_plot column
 *
 * @method array findById(string $id) Return ApiResources objects filtered by the id column
 * @method array findByResourcequantity(int $resourceQuantity) Return ApiResources objects filtered by the resourceQuantity column
 * @method array findByResourceproduction(string $resourceProduction) Return ApiResources objects filtered by the resourceProduction column
 * @method array findByResourceResourcetype(int $resource_resourceType) Return ApiResources objects filtered by the resource_resourceType column
 * @method array findByResourcePlot(string $resource_plot) Return ApiResources objects filtered by the resource_plot column
 *
 * @package    propel.generator.bookstore.om
 */
abstract class BaseApiResourcesQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseApiResourcesQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'bookstore';
        }
        if (null === $modelName) {
            $modelName = 'ApiResources';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ApiResourcesQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   ApiResourcesQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return ApiResourcesQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof ApiResourcesQuery) {
            return $criteria;
        }
        $query = new ApiResourcesQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   ApiResources|ApiResources[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = ApiResourcesPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(ApiResourcesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 ApiResources A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 ApiResources A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `resourceQuantity`, `resourceProduction`, `resource_resourceType`, `resource_plot` FROM `api_resources` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new ApiResources();
            $obj->hydrate($row);
            ApiResourcesPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return ApiResources|ApiResources[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|ApiResources[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return ApiResourcesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ApiResourcesPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return ApiResourcesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ApiResourcesPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiResourcesQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ApiResourcesPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ApiResourcesPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiResourcesPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the resourceQuantity column
     *
     * Example usage:
     * <code>
     * $query->filterByResourcequantity(1234); // WHERE resourceQuantity = 1234
     * $query->filterByResourcequantity(array(12, 34)); // WHERE resourceQuantity IN (12, 34)
     * $query->filterByResourcequantity(array('min' => 12)); // WHERE resourceQuantity >= 12
     * $query->filterByResourcequantity(array('max' => 12)); // WHERE resourceQuantity <= 12
     * </code>
     *
     * @param     mixed $resourcequantity The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiResourcesQuery The current query, for fluid interface
     */
    public function filterByResourcequantity($resourcequantity = null, $comparison = null)
    {
        if (is_array($resourcequantity)) {
            $useMinMax = false;
            if (isset($resourcequantity['min'])) {
                $this->addUsingAlias(ApiResourcesPeer::RESOURCEQUANTITY, $resourcequantity['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($resourcequantity['max'])) {
                $this->addUsingAlias(ApiResourcesPeer::RESOURCEQUANTITY, $resourcequantity['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiResourcesPeer::RESOURCEQUANTITY, $resourcequantity, $comparison);
    }

    /**
     * Filter the query on the resourceProduction column
     *
     * Example usage:
     * <code>
     * $query->filterByResourceproduction(1234); // WHERE resourceProduction = 1234
     * $query->filterByResourceproduction(array(12, 34)); // WHERE resourceProduction IN (12, 34)
     * $query->filterByResourceproduction(array('min' => 12)); // WHERE resourceProduction >= 12
     * $query->filterByResourceproduction(array('max' => 12)); // WHERE resourceProduction <= 12
     * </code>
     *
     * @param     mixed $resourceproduction The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiResourcesQuery The current query, for fluid interface
     */
    public function filterByResourceproduction($resourceproduction = null, $comparison = null)
    {
        if (is_array($resourceproduction)) {
            $useMinMax = false;
            if (isset($resourceproduction['min'])) {
                $this->addUsingAlias(ApiResourcesPeer::RESOURCEPRODUCTION, $resourceproduction['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($resourceproduction['max'])) {
                $this->addUsingAlias(ApiResourcesPeer::RESOURCEPRODUCTION, $resourceproduction['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiResourcesPeer::RESOURCEPRODUCTION, $resourceproduction, $comparison);
    }

    /**
     * Filter the query on the resource_resourceType column
     *
     * Example usage:
     * <code>
     * $query->filterByResourceResourcetype(1234); // WHERE resource_resourceType = 1234
     * $query->filterByResourceResourcetype(array(12, 34)); // WHERE resource_resourceType IN (12, 34)
     * $query->filterByResourceResourcetype(array('min' => 12)); // WHERE resource_resourceType >= 12
     * $query->filterByResourceResourcetype(array('max' => 12)); // WHERE resource_resourceType <= 12
     * </code>
     *
     * @param     mixed $resourceResourcetype The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiResourcesQuery The current query, for fluid interface
     */
    public function filterByResourceResourcetype($resourceResourcetype = null, $comparison = null)
    {
        if (is_array($resourceResourcetype)) {
            $useMinMax = false;
            if (isset($resourceResourcetype['min'])) {
                $this->addUsingAlias(ApiResourcesPeer::RESOURCE_RESOURCETYPE, $resourceResourcetype['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($resourceResourcetype['max'])) {
                $this->addUsingAlias(ApiResourcesPeer::RESOURCE_RESOURCETYPE, $resourceResourcetype['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiResourcesPeer::RESOURCE_RESOURCETYPE, $resourceResourcetype, $comparison);
    }

    /**
     * Filter the query on the resource_plot column
     *
     * Example usage:
     * <code>
     * $query->filterByResourcePlot(1234); // WHERE resource_plot = 1234
     * $query->filterByResourcePlot(array(12, 34)); // WHERE resource_plot IN (12, 34)
     * $query->filterByResourcePlot(array('min' => 12)); // WHERE resource_plot >= 12
     * $query->filterByResourcePlot(array('max' => 12)); // WHERE resource_plot <= 12
     * </code>
     *
     * @param     mixed $resourcePlot The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiResourcesQuery The current query, for fluid interface
     */
    public function filterByResourcePlot($resourcePlot = null, $comparison = null)
    {
        if (is_array($resourcePlot)) {
            $useMinMax = false;
            if (isset($resourcePlot['min'])) {
                $this->addUsingAlias(ApiResourcesPeer::RESOURCE_PLOT, $resourcePlot['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($resourcePlot['max'])) {
                $this->addUsingAlias(ApiResourcesPeer::RESOURCE_PLOT, $resourcePlot['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiResourcesPeer::RESOURCE_PLOT, $resourcePlot, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ApiResources $apiResources Object to remove from the list of results
     *
     * @return ApiResourcesQuery The current query, for fluid interface
     */
    public function prune($apiResources = null)
    {
        if ($apiResources) {
            $this->addUsingAlias(ApiResourcesPeer::ID, $apiResources->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
