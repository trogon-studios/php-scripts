<?php


/**
 * Base class that represents a query for the 'bsfusn_articles' table.
 *
 *
 *
 * @method BsfusnArticlesQuery orderByArticleId($order = Criteria::ASC) Order by the article_id column
 * @method BsfusnArticlesQuery orderByArticleCat($order = Criteria::ASC) Order by the article_cat column
 * @method BsfusnArticlesQuery orderByArticleSubject($order = Criteria::ASC) Order by the article_subject column
 * @method BsfusnArticlesQuery orderByArticleSnippet($order = Criteria::ASC) Order by the article_snippet column
 * @method BsfusnArticlesQuery orderByArticleArticle($order = Criteria::ASC) Order by the article_article column
 * @method BsfusnArticlesQuery orderByArticleDraft($order = Criteria::ASC) Order by the article_draft column
 * @method BsfusnArticlesQuery orderByArticleBreaks($order = Criteria::ASC) Order by the article_breaks column
 * @method BsfusnArticlesQuery orderByArticleName($order = Criteria::ASC) Order by the article_name column
 * @method BsfusnArticlesQuery orderByArticleDatestamp($order = Criteria::ASC) Order by the article_datestamp column
 * @method BsfusnArticlesQuery orderByArticleReads($order = Criteria::ASC) Order by the article_reads column
 * @method BsfusnArticlesQuery orderByArticleAllowComments($order = Criteria::ASC) Order by the article_allow_comments column
 * @method BsfusnArticlesQuery orderByArticleAllowRatings($order = Criteria::ASC) Order by the article_allow_ratings column
 *
 * @method BsfusnArticlesQuery groupByArticleId() Group by the article_id column
 * @method BsfusnArticlesQuery groupByArticleCat() Group by the article_cat column
 * @method BsfusnArticlesQuery groupByArticleSubject() Group by the article_subject column
 * @method BsfusnArticlesQuery groupByArticleSnippet() Group by the article_snippet column
 * @method BsfusnArticlesQuery groupByArticleArticle() Group by the article_article column
 * @method BsfusnArticlesQuery groupByArticleDraft() Group by the article_draft column
 * @method BsfusnArticlesQuery groupByArticleBreaks() Group by the article_breaks column
 * @method BsfusnArticlesQuery groupByArticleName() Group by the article_name column
 * @method BsfusnArticlesQuery groupByArticleDatestamp() Group by the article_datestamp column
 * @method BsfusnArticlesQuery groupByArticleReads() Group by the article_reads column
 * @method BsfusnArticlesQuery groupByArticleAllowComments() Group by the article_allow_comments column
 * @method BsfusnArticlesQuery groupByArticleAllowRatings() Group by the article_allow_ratings column
 *
 * @method BsfusnArticlesQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method BsfusnArticlesQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method BsfusnArticlesQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method BsfusnArticles findOne(PropelPDO $con = null) Return the first BsfusnArticles matching the query
 * @method BsfusnArticles findOneOrCreate(PropelPDO $con = null) Return the first BsfusnArticles matching the query, or a new BsfusnArticles object populated from the query conditions when no match is found
 *
 * @method BsfusnArticles findOneByArticleCat(int $article_cat) Return the first BsfusnArticles filtered by the article_cat column
 * @method BsfusnArticles findOneByArticleSubject(string $article_subject) Return the first BsfusnArticles filtered by the article_subject column
 * @method BsfusnArticles findOneByArticleSnippet(string $article_snippet) Return the first BsfusnArticles filtered by the article_snippet column
 * @method BsfusnArticles findOneByArticleArticle(string $article_article) Return the first BsfusnArticles filtered by the article_article column
 * @method BsfusnArticles findOneByArticleDraft(boolean $article_draft) Return the first BsfusnArticles filtered by the article_draft column
 * @method BsfusnArticles findOneByArticleBreaks(string $article_breaks) Return the first BsfusnArticles filtered by the article_breaks column
 * @method BsfusnArticles findOneByArticleName(int $article_name) Return the first BsfusnArticles filtered by the article_name column
 * @method BsfusnArticles findOneByArticleDatestamp(int $article_datestamp) Return the first BsfusnArticles filtered by the article_datestamp column
 * @method BsfusnArticles findOneByArticleReads(int $article_reads) Return the first BsfusnArticles filtered by the article_reads column
 * @method BsfusnArticles findOneByArticleAllowComments(boolean $article_allow_comments) Return the first BsfusnArticles filtered by the article_allow_comments column
 * @method BsfusnArticles findOneByArticleAllowRatings(boolean $article_allow_ratings) Return the first BsfusnArticles filtered by the article_allow_ratings column
 *
 * @method array findByArticleId(int $article_id) Return BsfusnArticles objects filtered by the article_id column
 * @method array findByArticleCat(int $article_cat) Return BsfusnArticles objects filtered by the article_cat column
 * @method array findByArticleSubject(string $article_subject) Return BsfusnArticles objects filtered by the article_subject column
 * @method array findByArticleSnippet(string $article_snippet) Return BsfusnArticles objects filtered by the article_snippet column
 * @method array findByArticleArticle(string $article_article) Return BsfusnArticles objects filtered by the article_article column
 * @method array findByArticleDraft(boolean $article_draft) Return BsfusnArticles objects filtered by the article_draft column
 * @method array findByArticleBreaks(string $article_breaks) Return BsfusnArticles objects filtered by the article_breaks column
 * @method array findByArticleName(int $article_name) Return BsfusnArticles objects filtered by the article_name column
 * @method array findByArticleDatestamp(int $article_datestamp) Return BsfusnArticles objects filtered by the article_datestamp column
 * @method array findByArticleReads(int $article_reads) Return BsfusnArticles objects filtered by the article_reads column
 * @method array findByArticleAllowComments(boolean $article_allow_comments) Return BsfusnArticles objects filtered by the article_allow_comments column
 * @method array findByArticleAllowRatings(boolean $article_allow_ratings) Return BsfusnArticles objects filtered by the article_allow_ratings column
 *
 * @package    propel.generator.bookstore.om
 */
abstract class BaseBsfusnArticlesQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseBsfusnArticlesQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'bookstore';
        }
        if (null === $modelName) {
            $modelName = 'BsfusnArticles';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new BsfusnArticlesQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   BsfusnArticlesQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return BsfusnArticlesQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof BsfusnArticlesQuery) {
            return $criteria;
        }
        $query = new BsfusnArticlesQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   BsfusnArticles|BsfusnArticles[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = BsfusnArticlesPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(BsfusnArticlesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 BsfusnArticles A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByArticleId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 BsfusnArticles A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `article_id`, `article_cat`, `article_subject`, `article_snippet`, `article_article`, `article_draft`, `article_breaks`, `article_name`, `article_datestamp`, `article_reads`, `article_allow_comments`, `article_allow_ratings` FROM `bsfusn_articles` WHERE `article_id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new BsfusnArticles();
            $obj->hydrate($row);
            BsfusnArticlesPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return BsfusnArticles|BsfusnArticles[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|BsfusnArticles[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return BsfusnArticlesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(BsfusnArticlesPeer::ARTICLE_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return BsfusnArticlesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(BsfusnArticlesPeer::ARTICLE_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the article_id column
     *
     * Example usage:
     * <code>
     * $query->filterByArticleId(1234); // WHERE article_id = 1234
     * $query->filterByArticleId(array(12, 34)); // WHERE article_id IN (12, 34)
     * $query->filterByArticleId(array('min' => 12)); // WHERE article_id >= 12
     * $query->filterByArticleId(array('max' => 12)); // WHERE article_id <= 12
     * </code>
     *
     * @param     mixed $articleId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BsfusnArticlesQuery The current query, for fluid interface
     */
    public function filterByArticleId($articleId = null, $comparison = null)
    {
        if (is_array($articleId)) {
            $useMinMax = false;
            if (isset($articleId['min'])) {
                $this->addUsingAlias(BsfusnArticlesPeer::ARTICLE_ID, $articleId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($articleId['max'])) {
                $this->addUsingAlias(BsfusnArticlesPeer::ARTICLE_ID, $articleId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BsfusnArticlesPeer::ARTICLE_ID, $articleId, $comparison);
    }

    /**
     * Filter the query on the article_cat column
     *
     * Example usage:
     * <code>
     * $query->filterByArticleCat(1234); // WHERE article_cat = 1234
     * $query->filterByArticleCat(array(12, 34)); // WHERE article_cat IN (12, 34)
     * $query->filterByArticleCat(array('min' => 12)); // WHERE article_cat >= 12
     * $query->filterByArticleCat(array('max' => 12)); // WHERE article_cat <= 12
     * </code>
     *
     * @param     mixed $articleCat The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BsfusnArticlesQuery The current query, for fluid interface
     */
    public function filterByArticleCat($articleCat = null, $comparison = null)
    {
        if (is_array($articleCat)) {
            $useMinMax = false;
            if (isset($articleCat['min'])) {
                $this->addUsingAlias(BsfusnArticlesPeer::ARTICLE_CAT, $articleCat['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($articleCat['max'])) {
                $this->addUsingAlias(BsfusnArticlesPeer::ARTICLE_CAT, $articleCat['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BsfusnArticlesPeer::ARTICLE_CAT, $articleCat, $comparison);
    }

    /**
     * Filter the query on the article_subject column
     *
     * Example usage:
     * <code>
     * $query->filterByArticleSubject('fooValue');   // WHERE article_subject = 'fooValue'
     * $query->filterByArticleSubject('%fooValue%'); // WHERE article_subject LIKE '%fooValue%'
     * </code>
     *
     * @param     string $articleSubject The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BsfusnArticlesQuery The current query, for fluid interface
     */
    public function filterByArticleSubject($articleSubject = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($articleSubject)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $articleSubject)) {
                $articleSubject = str_replace('*', '%', $articleSubject);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(BsfusnArticlesPeer::ARTICLE_SUBJECT, $articleSubject, $comparison);
    }

    /**
     * Filter the query on the article_snippet column
     *
     * Example usage:
     * <code>
     * $query->filterByArticleSnippet('fooValue');   // WHERE article_snippet = 'fooValue'
     * $query->filterByArticleSnippet('%fooValue%'); // WHERE article_snippet LIKE '%fooValue%'
     * </code>
     *
     * @param     string $articleSnippet The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BsfusnArticlesQuery The current query, for fluid interface
     */
    public function filterByArticleSnippet($articleSnippet = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($articleSnippet)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $articleSnippet)) {
                $articleSnippet = str_replace('*', '%', $articleSnippet);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(BsfusnArticlesPeer::ARTICLE_SNIPPET, $articleSnippet, $comparison);
    }

    /**
     * Filter the query on the article_article column
     *
     * Example usage:
     * <code>
     * $query->filterByArticleArticle('fooValue');   // WHERE article_article = 'fooValue'
     * $query->filterByArticleArticle('%fooValue%'); // WHERE article_article LIKE '%fooValue%'
     * </code>
     *
     * @param     string $articleArticle The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BsfusnArticlesQuery The current query, for fluid interface
     */
    public function filterByArticleArticle($articleArticle = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($articleArticle)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $articleArticle)) {
                $articleArticle = str_replace('*', '%', $articleArticle);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(BsfusnArticlesPeer::ARTICLE_ARTICLE, $articleArticle, $comparison);
    }

    /**
     * Filter the query on the article_draft column
     *
     * Example usage:
     * <code>
     * $query->filterByArticleDraft(true); // WHERE article_draft = true
     * $query->filterByArticleDraft('yes'); // WHERE article_draft = true
     * </code>
     *
     * @param     boolean|string $articleDraft The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BsfusnArticlesQuery The current query, for fluid interface
     */
    public function filterByArticleDraft($articleDraft = null, $comparison = null)
    {
        if (is_string($articleDraft)) {
            $articleDraft = in_array(strtolower($articleDraft), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(BsfusnArticlesPeer::ARTICLE_DRAFT, $articleDraft, $comparison);
    }

    /**
     * Filter the query on the article_breaks column
     *
     * Example usage:
     * <code>
     * $query->filterByArticleBreaks('fooValue');   // WHERE article_breaks = 'fooValue'
     * $query->filterByArticleBreaks('%fooValue%'); // WHERE article_breaks LIKE '%fooValue%'
     * </code>
     *
     * @param     string $articleBreaks The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BsfusnArticlesQuery The current query, for fluid interface
     */
    public function filterByArticleBreaks($articleBreaks = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($articleBreaks)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $articleBreaks)) {
                $articleBreaks = str_replace('*', '%', $articleBreaks);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(BsfusnArticlesPeer::ARTICLE_BREAKS, $articleBreaks, $comparison);
    }

    /**
     * Filter the query on the article_name column
     *
     * Example usage:
     * <code>
     * $query->filterByArticleName(1234); // WHERE article_name = 1234
     * $query->filterByArticleName(array(12, 34)); // WHERE article_name IN (12, 34)
     * $query->filterByArticleName(array('min' => 12)); // WHERE article_name >= 12
     * $query->filterByArticleName(array('max' => 12)); // WHERE article_name <= 12
     * </code>
     *
     * @param     mixed $articleName The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BsfusnArticlesQuery The current query, for fluid interface
     */
    public function filterByArticleName($articleName = null, $comparison = null)
    {
        if (is_array($articleName)) {
            $useMinMax = false;
            if (isset($articleName['min'])) {
                $this->addUsingAlias(BsfusnArticlesPeer::ARTICLE_NAME, $articleName['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($articleName['max'])) {
                $this->addUsingAlias(BsfusnArticlesPeer::ARTICLE_NAME, $articleName['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BsfusnArticlesPeer::ARTICLE_NAME, $articleName, $comparison);
    }

    /**
     * Filter the query on the article_datestamp column
     *
     * Example usage:
     * <code>
     * $query->filterByArticleDatestamp(1234); // WHERE article_datestamp = 1234
     * $query->filterByArticleDatestamp(array(12, 34)); // WHERE article_datestamp IN (12, 34)
     * $query->filterByArticleDatestamp(array('min' => 12)); // WHERE article_datestamp >= 12
     * $query->filterByArticleDatestamp(array('max' => 12)); // WHERE article_datestamp <= 12
     * </code>
     *
     * @param     mixed $articleDatestamp The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BsfusnArticlesQuery The current query, for fluid interface
     */
    public function filterByArticleDatestamp($articleDatestamp = null, $comparison = null)
    {
        if (is_array($articleDatestamp)) {
            $useMinMax = false;
            if (isset($articleDatestamp['min'])) {
                $this->addUsingAlias(BsfusnArticlesPeer::ARTICLE_DATESTAMP, $articleDatestamp['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($articleDatestamp['max'])) {
                $this->addUsingAlias(BsfusnArticlesPeer::ARTICLE_DATESTAMP, $articleDatestamp['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BsfusnArticlesPeer::ARTICLE_DATESTAMP, $articleDatestamp, $comparison);
    }

    /**
     * Filter the query on the article_reads column
     *
     * Example usage:
     * <code>
     * $query->filterByArticleReads(1234); // WHERE article_reads = 1234
     * $query->filterByArticleReads(array(12, 34)); // WHERE article_reads IN (12, 34)
     * $query->filterByArticleReads(array('min' => 12)); // WHERE article_reads >= 12
     * $query->filterByArticleReads(array('max' => 12)); // WHERE article_reads <= 12
     * </code>
     *
     * @param     mixed $articleReads The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BsfusnArticlesQuery The current query, for fluid interface
     */
    public function filterByArticleReads($articleReads = null, $comparison = null)
    {
        if (is_array($articleReads)) {
            $useMinMax = false;
            if (isset($articleReads['min'])) {
                $this->addUsingAlias(BsfusnArticlesPeer::ARTICLE_READS, $articleReads['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($articleReads['max'])) {
                $this->addUsingAlias(BsfusnArticlesPeer::ARTICLE_READS, $articleReads['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BsfusnArticlesPeer::ARTICLE_READS, $articleReads, $comparison);
    }

    /**
     * Filter the query on the article_allow_comments column
     *
     * Example usage:
     * <code>
     * $query->filterByArticleAllowComments(true); // WHERE article_allow_comments = true
     * $query->filterByArticleAllowComments('yes'); // WHERE article_allow_comments = true
     * </code>
     *
     * @param     boolean|string $articleAllowComments The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BsfusnArticlesQuery The current query, for fluid interface
     */
    public function filterByArticleAllowComments($articleAllowComments = null, $comparison = null)
    {
        if (is_string($articleAllowComments)) {
            $articleAllowComments = in_array(strtolower($articleAllowComments), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(BsfusnArticlesPeer::ARTICLE_ALLOW_COMMENTS, $articleAllowComments, $comparison);
    }

    /**
     * Filter the query on the article_allow_ratings column
     *
     * Example usage:
     * <code>
     * $query->filterByArticleAllowRatings(true); // WHERE article_allow_ratings = true
     * $query->filterByArticleAllowRatings('yes'); // WHERE article_allow_ratings = true
     * </code>
     *
     * @param     boolean|string $articleAllowRatings The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BsfusnArticlesQuery The current query, for fluid interface
     */
    public function filterByArticleAllowRatings($articleAllowRatings = null, $comparison = null)
    {
        if (is_string($articleAllowRatings)) {
            $articleAllowRatings = in_array(strtolower($articleAllowRatings), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(BsfusnArticlesPeer::ARTICLE_ALLOW_RATINGS, $articleAllowRatings, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   BsfusnArticles $bsfusnArticles Object to remove from the list of results
     *
     * @return BsfusnArticlesQuery The current query, for fluid interface
     */
    public function prune($bsfusnArticles = null)
    {
        if ($bsfusnArticles) {
            $this->addUsingAlias(BsfusnArticlesPeer::ARTICLE_ID, $bsfusnArticles->getArticleId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
