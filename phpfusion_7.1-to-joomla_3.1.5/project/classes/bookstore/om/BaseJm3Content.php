<?php


/**
 * Base class that represents a row from the 'jm3_content' table.
 *
 *
 *
 * @package    propel.generator.bookstore.om
 */
abstract class BaseJm3Content extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Jm3ContentPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        Jm3ContentPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the asset_id field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $asset_id;

    /**
     * The value for the title field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $title;

    /**
     * The value for the alias field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $alias;

    /**
     * The value for the introtext field.
     * @var        string
     */
    protected $introtext;

    /**
     * The value for the fulltext field.
     * @var        string
     */
    protected $fulltext;

    /**
     * The value for the state field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $state;

    /**
     * The value for the catid field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $catid;

    /**
     * The value for the created field.
     * Note: this column has a database default value of: NULL
     * @var        string
     */
    protected $created;

    /**
     * The value for the created_by field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $created_by;

    /**
     * The value for the created_by_alias field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $created_by_alias;

    /**
     * The value for the modified field.
     * Note: this column has a database default value of: NULL
     * @var        string
     */
    protected $modified;

    /**
     * The value for the modified_by field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $modified_by;

    /**
     * The value for the checked_out field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $checked_out;

    /**
     * The value for the checked_out_time field.
     * Note: this column has a database default value of: NULL
     * @var        string
     */
    protected $checked_out_time;

    /**
     * The value for the publish_up field.
     * Note: this column has a database default value of: NULL
     * @var        string
     */
    protected $publish_up;

    /**
     * The value for the publish_down field.
     * Note: this column has a database default value of: NULL
     * @var        string
     */
    protected $publish_down;

    /**
     * The value for the images field.
     * @var        string
     */
    protected $images;

    /**
     * The value for the urls field.
     * @var        string
     */
    protected $urls;

    /**
     * The value for the attribs field.
     * @var        string
     */
    protected $attribs;

    /**
     * The value for the version field.
     * Note: this column has a database default value of: 1
     * @var        int
     */
    protected $version;

    /**
     * The value for the ordering field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $ordering;

    /**
     * The value for the metakey field.
     * @var        string
     */
    protected $metakey;

    /**
     * The value for the metadesc field.
     * @var        string
     */
    protected $metadesc;

    /**
     * The value for the access field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $access;

    /**
     * The value for the hits field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $hits;

    /**
     * The value for the metadata field.
     * @var        string
     */
    protected $metadata;

    /**
     * The value for the featured field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $featured;

    /**
     * The value for the language field.
     * @var        string
     */
    protected $language;

    /**
     * The value for the xreference field.
     * @var        string
     */
    protected $xreference;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->asset_id = 0;
        $this->title = '';
        $this->alias = '';
        $this->state = 0;
        $this->catid = 0;
        $this->created = NULL;
        $this->created_by = 0;
        $this->created_by_alias = '';
        $this->modified = NULL;
        $this->modified_by = 0;
        $this->checked_out = 0;
        $this->checked_out_time = NULL;
        $this->publish_up = NULL;
        $this->publish_down = NULL;
        $this->version = 1;
        $this->ordering = 0;
        $this->access = 0;
        $this->hits = 0;
        $this->featured = 0;
    }

    /**
     * Initializes internal state of BaseJm3Content object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [asset_id] column value.
     *
     * @return int
     */
    public function getAssetId()
    {

        return $this->asset_id;
    }

    /**
     * Get the [title] column value.
     *
     * @return string
     */
    public function getTitle()
    {

        return $this->title;
    }

    /**
     * Get the [alias] column value.
     *
     * @return string
     */
    public function getAlias()
    {

        return $this->alias;
    }

    /**
     * Get the [introtext] column value.
     *
     * @return string
     */
    public function getIntrotext()
    {

        return $this->introtext;
    }

    /**
     * Get the [fulltext] column value.
     *
     * @return string
     */
    public function getFulltext()
    {

        return $this->fulltext;
    }

    /**
     * Get the [state] column value.
     *
     * @return int
     */
    public function getState()
    {

        return $this->state;
    }

    /**
     * Get the [catid] column value.
     *
     * @return int
     */
    public function getCatid()
    {

        return $this->catid;
    }

    /**
     * Get the [optionally formatted] temporal [created] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreated($format = 'Y-m-d H:i:s')
    {
        if ($this->created === null) {
            return null;
        }

        if ($this->created === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->created);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->created, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [created_by] column value.
     *
     * @return int
     */
    public function getCreatedBy()
    {

        return $this->created_by;
    }

    /**
     * Get the [created_by_alias] column value.
     *
     * @return string
     */
    public function getCreatedByAlias()
    {

        return $this->created_by_alias;
    }

    /**
     * Get the [optionally formatted] temporal [modified] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getModified($format = 'Y-m-d H:i:s')
    {
        if ($this->modified === null) {
            return null;
        }

        if ($this->modified === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->modified);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->modified, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [modified_by] column value.
     *
     * @return int
     */
    public function getModifiedBy()
    {

        return $this->modified_by;
    }

    /**
     * Get the [checked_out] column value.
     *
     * @return int
     */
    public function getCheckedOut()
    {

        return $this->checked_out;
    }

    /**
     * Get the [optionally formatted] temporal [checked_out_time] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCheckedOutTime($format = 'Y-m-d H:i:s')
    {
        if ($this->checked_out_time === null) {
            return null;
        }

        if ($this->checked_out_time === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->checked_out_time);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->checked_out_time, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [optionally formatted] temporal [publish_up] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getPublishUp($format = 'Y-m-d H:i:s')
    {
        if ($this->publish_up === null) {
            return null;
        }

        if ($this->publish_up === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->publish_up);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->publish_up, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [optionally formatted] temporal [publish_down] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getPublishDown($format = 'Y-m-d H:i:s')
    {
        if ($this->publish_down === null) {
            return null;
        }

        if ($this->publish_down === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->publish_down);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->publish_down, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [images] column value.
     *
     * @return string
     */
    public function getImages()
    {

        return $this->images;
    }

    /**
     * Get the [urls] column value.
     *
     * @return string
     */
    public function getUrls()
    {

        return $this->urls;
    }

    /**
     * Get the [attribs] column value.
     *
     * @return string
     */
    public function getAttribs()
    {

        return $this->attribs;
    }

    /**
     * Get the [version] column value.
     *
     * @return int
     */
    public function getVersion()
    {

        return $this->version;
    }

    /**
     * Get the [ordering] column value.
     *
     * @return int
     */
    public function getOrdering()
    {

        return $this->ordering;
    }

    /**
     * Get the [metakey] column value.
     *
     * @return string
     */
    public function getMetakey()
    {

        return $this->metakey;
    }

    /**
     * Get the [metadesc] column value.
     *
     * @return string
     */
    public function getMetadesc()
    {

        return $this->metadesc;
    }

    /**
     * Get the [access] column value.
     *
     * @return int
     */
    public function getAccess()
    {

        return $this->access;
    }

    /**
     * Get the [hits] column value.
     *
     * @return int
     */
    public function getHits()
    {

        return $this->hits;
    }

    /**
     * Get the [metadata] column value.
     *
     * @return string
     */
    public function getMetadata()
    {

        return $this->metadata;
    }

    /**
     * Get the [featured] column value.
     *
     * @return int
     */
    public function getFeatured()
    {

        return $this->featured;
    }

    /**
     * Get the [language] column value.
     *
     * @return string
     */
    public function getLanguage()
    {

        return $this->language;
    }

    /**
     * Get the [xreference] column value.
     *
     * @return string
     */
    public function getXreference()
    {

        return $this->xreference;
    }

    /**
     * Set the value of [id] column.
     *
     * @param  int $v new value
     * @return Jm3Content The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = Jm3ContentPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [asset_id] column.
     *
     * @param  int $v new value
     * @return Jm3Content The current object (for fluent API support)
     */
    public function setAssetId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->asset_id !== $v) {
            $this->asset_id = $v;
            $this->modifiedColumns[] = Jm3ContentPeer::ASSET_ID;
        }


        return $this;
    } // setAssetId()

    /**
     * Set the value of [title] column.
     *
     * @param  string $v new value
     * @return Jm3Content The current object (for fluent API support)
     */
    public function setTitle($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->title !== $v) {
            $this->title = $v;
            $this->modifiedColumns[] = Jm3ContentPeer::TITLE;
        }


        return $this;
    } // setTitle()

    /**
     * Set the value of [alias] column.
     *
     * @param  string $v new value
     * @return Jm3Content The current object (for fluent API support)
     */
    public function setAlias($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->alias !== $v) {
            $this->alias = $v;
            $this->modifiedColumns[] = Jm3ContentPeer::ALIAS;
        }


        return $this;
    } // setAlias()

    /**
     * Set the value of [introtext] column.
     *
     * @param  string $v new value
     * @return Jm3Content The current object (for fluent API support)
     */
    public function setIntrotext($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->introtext !== $v) {
            $this->introtext = $v;
            $this->modifiedColumns[] = Jm3ContentPeer::INTROTEXT;
        }


        return $this;
    } // setIntrotext()

    /**
     * Set the value of [fulltext] column.
     *
     * @param  string $v new value
     * @return Jm3Content The current object (for fluent API support)
     */
    public function setFulltext($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->fulltext !== $v) {
            $this->fulltext = $v;
            $this->modifiedColumns[] = Jm3ContentPeer::FULLTEXT;
        }


        return $this;
    } // setFulltext()

    /**
     * Set the value of [state] column.
     *
     * @param  int $v new value
     * @return Jm3Content The current object (for fluent API support)
     */
    public function setState($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->state !== $v) {
            $this->state = $v;
            $this->modifiedColumns[] = Jm3ContentPeer::STATE;
        }


        return $this;
    } // setState()

    /**
     * Set the value of [catid] column.
     *
     * @param  int $v new value
     * @return Jm3Content The current object (for fluent API support)
     */
    public function setCatid($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->catid !== $v) {
            $this->catid = $v;
            $this->modifiedColumns[] = Jm3ContentPeer::CATID;
        }


        return $this;
    } // setCatid()

    /**
     * Sets the value of [created] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Jm3Content The current object (for fluent API support)
     */
    public function setCreated($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created !== null || $dt !== null) {
            $currentDateAsString = ($this->created !== null && $tmpDt = new DateTime($this->created)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ( ($currentDateAsString !== $newDateAsString) // normalized values don't match
                || ($dt->format('Y-m-d H:i:s') === NULL) // or the entered value matches the default
                 ) {
                $this->created = $newDateAsString;
                $this->modifiedColumns[] = Jm3ContentPeer::CREATED;
            }
        } // if either are not null


        return $this;
    } // setCreated()

    /**
     * Set the value of [created_by] column.
     *
     * @param  int $v new value
     * @return Jm3Content The current object (for fluent API support)
     */
    public function setCreatedBy($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->created_by !== $v) {
            $this->created_by = $v;
            $this->modifiedColumns[] = Jm3ContentPeer::CREATED_BY;
        }


        return $this;
    } // setCreatedBy()

    /**
     * Set the value of [created_by_alias] column.
     *
     * @param  string $v new value
     * @return Jm3Content The current object (for fluent API support)
     */
    public function setCreatedByAlias($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->created_by_alias !== $v) {
            $this->created_by_alias = $v;
            $this->modifiedColumns[] = Jm3ContentPeer::CREATED_BY_ALIAS;
        }


        return $this;
    } // setCreatedByAlias()

    /**
     * Sets the value of [modified] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Jm3Content The current object (for fluent API support)
     */
    public function setModified($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->modified !== null || $dt !== null) {
            $currentDateAsString = ($this->modified !== null && $tmpDt = new DateTime($this->modified)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ( ($currentDateAsString !== $newDateAsString) // normalized values don't match
                || ($dt->format('Y-m-d H:i:s') === NULL) // or the entered value matches the default
                 ) {
                $this->modified = $newDateAsString;
                $this->modifiedColumns[] = Jm3ContentPeer::MODIFIED;
            }
        } // if either are not null


        return $this;
    } // setModified()

    /**
     * Set the value of [modified_by] column.
     *
     * @param  int $v new value
     * @return Jm3Content The current object (for fluent API support)
     */
    public function setModifiedBy($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->modified_by !== $v) {
            $this->modified_by = $v;
            $this->modifiedColumns[] = Jm3ContentPeer::MODIFIED_BY;
        }


        return $this;
    } // setModifiedBy()

    /**
     * Set the value of [checked_out] column.
     *
     * @param  int $v new value
     * @return Jm3Content The current object (for fluent API support)
     */
    public function setCheckedOut($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->checked_out !== $v) {
            $this->checked_out = $v;
            $this->modifiedColumns[] = Jm3ContentPeer::CHECKED_OUT;
        }


        return $this;
    } // setCheckedOut()

    /**
     * Sets the value of [checked_out_time] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Jm3Content The current object (for fluent API support)
     */
    public function setCheckedOutTime($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->checked_out_time !== null || $dt !== null) {
            $currentDateAsString = ($this->checked_out_time !== null && $tmpDt = new DateTime($this->checked_out_time)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ( ($currentDateAsString !== $newDateAsString) // normalized values don't match
                || ($dt->format('Y-m-d H:i:s') === NULL) // or the entered value matches the default
                 ) {
                $this->checked_out_time = $newDateAsString;
                $this->modifiedColumns[] = Jm3ContentPeer::CHECKED_OUT_TIME;
            }
        } // if either are not null


        return $this;
    } // setCheckedOutTime()

    /**
     * Sets the value of [publish_up] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Jm3Content The current object (for fluent API support)
     */
    public function setPublishUp($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->publish_up !== null || $dt !== null) {
            $currentDateAsString = ($this->publish_up !== null && $tmpDt = new DateTime($this->publish_up)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ( ($currentDateAsString !== $newDateAsString) // normalized values don't match
                || ($dt->format('Y-m-d H:i:s') === NULL) // or the entered value matches the default
                 ) {
                $this->publish_up = $newDateAsString;
                $this->modifiedColumns[] = Jm3ContentPeer::PUBLISH_UP;
            }
        } // if either are not null


        return $this;
    } // setPublishUp()

    /**
     * Sets the value of [publish_down] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Jm3Content The current object (for fluent API support)
     */
    public function setPublishDown($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->publish_down !== null || $dt !== null) {
            $currentDateAsString = ($this->publish_down !== null && $tmpDt = new DateTime($this->publish_down)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ( ($currentDateAsString !== $newDateAsString) // normalized values don't match
                || ($dt->format('Y-m-d H:i:s') === NULL) // or the entered value matches the default
                 ) {
                $this->publish_down = $newDateAsString;
                $this->modifiedColumns[] = Jm3ContentPeer::PUBLISH_DOWN;
            }
        } // if either are not null


        return $this;
    } // setPublishDown()

    /**
     * Set the value of [images] column.
     *
     * @param  string $v new value
     * @return Jm3Content The current object (for fluent API support)
     */
    public function setImages($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->images !== $v) {
            $this->images = $v;
            $this->modifiedColumns[] = Jm3ContentPeer::IMAGES;
        }


        return $this;
    } // setImages()

    /**
     * Set the value of [urls] column.
     *
     * @param  string $v new value
     * @return Jm3Content The current object (for fluent API support)
     */
    public function setUrls($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->urls !== $v) {
            $this->urls = $v;
            $this->modifiedColumns[] = Jm3ContentPeer::URLS;
        }


        return $this;
    } // setUrls()

    /**
     * Set the value of [attribs] column.
     *
     * @param  string $v new value
     * @return Jm3Content The current object (for fluent API support)
     */
    public function setAttribs($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->attribs !== $v) {
            $this->attribs = $v;
            $this->modifiedColumns[] = Jm3ContentPeer::ATTRIBS;
        }


        return $this;
    } // setAttribs()

    /**
     * Set the value of [version] column.
     *
     * @param  int $v new value
     * @return Jm3Content The current object (for fluent API support)
     */
    public function setVersion($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->version !== $v) {
            $this->version = $v;
            $this->modifiedColumns[] = Jm3ContentPeer::VERSION;
        }


        return $this;
    } // setVersion()

    /**
     * Set the value of [ordering] column.
     *
     * @param  int $v new value
     * @return Jm3Content The current object (for fluent API support)
     */
    public function setOrdering($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->ordering !== $v) {
            $this->ordering = $v;
            $this->modifiedColumns[] = Jm3ContentPeer::ORDERING;
        }


        return $this;
    } // setOrdering()

    /**
     * Set the value of [metakey] column.
     *
     * @param  string $v new value
     * @return Jm3Content The current object (for fluent API support)
     */
    public function setMetakey($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->metakey !== $v) {
            $this->metakey = $v;
            $this->modifiedColumns[] = Jm3ContentPeer::METAKEY;
        }


        return $this;
    } // setMetakey()

    /**
     * Set the value of [metadesc] column.
     *
     * @param  string $v new value
     * @return Jm3Content The current object (for fluent API support)
     */
    public function setMetadesc($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->metadesc !== $v) {
            $this->metadesc = $v;
            $this->modifiedColumns[] = Jm3ContentPeer::METADESC;
        }


        return $this;
    } // setMetadesc()

    /**
     * Set the value of [access] column.
     *
     * @param  int $v new value
     * @return Jm3Content The current object (for fluent API support)
     */
    public function setAccess($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->access !== $v) {
            $this->access = $v;
            $this->modifiedColumns[] = Jm3ContentPeer::ACCESS;
        }


        return $this;
    } // setAccess()

    /**
     * Set the value of [hits] column.
     *
     * @param  int $v new value
     * @return Jm3Content The current object (for fluent API support)
     */
    public function setHits($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->hits !== $v) {
            $this->hits = $v;
            $this->modifiedColumns[] = Jm3ContentPeer::HITS;
        }


        return $this;
    } // setHits()

    /**
     * Set the value of [metadata] column.
     *
     * @param  string $v new value
     * @return Jm3Content The current object (for fluent API support)
     */
    public function setMetadata($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->metadata !== $v) {
            $this->metadata = $v;
            $this->modifiedColumns[] = Jm3ContentPeer::METADATA;
        }


        return $this;
    } // setMetadata()

    /**
     * Set the value of [featured] column.
     *
     * @param  int $v new value
     * @return Jm3Content The current object (for fluent API support)
     */
    public function setFeatured($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->featured !== $v) {
            $this->featured = $v;
            $this->modifiedColumns[] = Jm3ContentPeer::FEATURED;
        }


        return $this;
    } // setFeatured()

    /**
     * Set the value of [language] column.
     *
     * @param  string $v new value
     * @return Jm3Content The current object (for fluent API support)
     */
    public function setLanguage($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->language !== $v) {
            $this->language = $v;
            $this->modifiedColumns[] = Jm3ContentPeer::LANGUAGE;
        }


        return $this;
    } // setLanguage()

    /**
     * Set the value of [xreference] column.
     *
     * @param  string $v new value
     * @return Jm3Content The current object (for fluent API support)
     */
    public function setXreference($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->xreference !== $v) {
            $this->xreference = $v;
            $this->modifiedColumns[] = Jm3ContentPeer::XREFERENCE;
        }


        return $this;
    } // setXreference()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->asset_id !== 0) {
                return false;
            }

            if ($this->title !== '') {
                return false;
            }

            if ($this->alias !== '') {
                return false;
            }

            if ($this->state !== 0) {
                return false;
            }

            if ($this->catid !== 0) {
                return false;
            }

            if ($this->created !== NULL) {
                return false;
            }

            if ($this->created_by !== 0) {
                return false;
            }

            if ($this->created_by_alias !== '') {
                return false;
            }

            if ($this->modified !== NULL) {
                return false;
            }

            if ($this->modified_by !== 0) {
                return false;
            }

            if ($this->checked_out !== 0) {
                return false;
            }

            if ($this->checked_out_time !== NULL) {
                return false;
            }

            if ($this->publish_up !== NULL) {
                return false;
            }

            if ($this->publish_down !== NULL) {
                return false;
            }

            if ($this->version !== 1) {
                return false;
            }

            if ($this->ordering !== 0) {
                return false;
            }

            if ($this->access !== 0) {
                return false;
            }

            if ($this->hits !== 0) {
                return false;
            }

            if ($this->featured !== 0) {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->asset_id = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->title = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->alias = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->introtext = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->fulltext = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->state = ($row[$startcol + 6] !== null) ? (int) $row[$startcol + 6] : null;
            $this->catid = ($row[$startcol + 7] !== null) ? (int) $row[$startcol + 7] : null;
            $this->created = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->created_by = ($row[$startcol + 9] !== null) ? (int) $row[$startcol + 9] : null;
            $this->created_by_alias = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->modified = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->modified_by = ($row[$startcol + 12] !== null) ? (int) $row[$startcol + 12] : null;
            $this->checked_out = ($row[$startcol + 13] !== null) ? (int) $row[$startcol + 13] : null;
            $this->checked_out_time = ($row[$startcol + 14] !== null) ? (string) $row[$startcol + 14] : null;
            $this->publish_up = ($row[$startcol + 15] !== null) ? (string) $row[$startcol + 15] : null;
            $this->publish_down = ($row[$startcol + 16] !== null) ? (string) $row[$startcol + 16] : null;
            $this->images = ($row[$startcol + 17] !== null) ? (string) $row[$startcol + 17] : null;
            $this->urls = ($row[$startcol + 18] !== null) ? (string) $row[$startcol + 18] : null;
            $this->attribs = ($row[$startcol + 19] !== null) ? (string) $row[$startcol + 19] : null;
            $this->version = ($row[$startcol + 20] !== null) ? (int) $row[$startcol + 20] : null;
            $this->ordering = ($row[$startcol + 21] !== null) ? (int) $row[$startcol + 21] : null;
            $this->metakey = ($row[$startcol + 22] !== null) ? (string) $row[$startcol + 22] : null;
            $this->metadesc = ($row[$startcol + 23] !== null) ? (string) $row[$startcol + 23] : null;
            $this->access = ($row[$startcol + 24] !== null) ? (int) $row[$startcol + 24] : null;
            $this->hits = ($row[$startcol + 25] !== null) ? (int) $row[$startcol + 25] : null;
            $this->metadata = ($row[$startcol + 26] !== null) ? (string) $row[$startcol + 26] : null;
            $this->featured = ($row[$startcol + 27] !== null) ? (int) $row[$startcol + 27] : null;
            $this->language = ($row[$startcol + 28] !== null) ? (string) $row[$startcol + 28] : null;
            $this->xreference = ($row[$startcol + 29] !== null) ? (string) $row[$startcol + 29] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 30; // 30 = Jm3ContentPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Jm3Content object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(Jm3ContentPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = Jm3ContentPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(Jm3ContentPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = Jm3ContentQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(Jm3ContentPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                Jm3ContentPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = Jm3ContentPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . Jm3ContentPeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(Jm3ContentPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(Jm3ContentPeer::ASSET_ID)) {
            $modifiedColumns[':p' . $index++]  = '`asset_id`';
        }
        if ($this->isColumnModified(Jm3ContentPeer::TITLE)) {
            $modifiedColumns[':p' . $index++]  = '`title`';
        }
        if ($this->isColumnModified(Jm3ContentPeer::ALIAS)) {
            $modifiedColumns[':p' . $index++]  = '`alias`';
        }
        if ($this->isColumnModified(Jm3ContentPeer::INTROTEXT)) {
            $modifiedColumns[':p' . $index++]  = '`introtext`';
        }
        if ($this->isColumnModified(Jm3ContentPeer::FULLTEXT)) {
            $modifiedColumns[':p' . $index++]  = '`fulltext`';
        }
        if ($this->isColumnModified(Jm3ContentPeer::STATE)) {
            $modifiedColumns[':p' . $index++]  = '`state`';
        }
        if ($this->isColumnModified(Jm3ContentPeer::CATID)) {
            $modifiedColumns[':p' . $index++]  = '`catid`';
        }
        if ($this->isColumnModified(Jm3ContentPeer::CREATED)) {
            $modifiedColumns[':p' . $index++]  = '`created`';
        }
        if ($this->isColumnModified(Jm3ContentPeer::CREATED_BY)) {
            $modifiedColumns[':p' . $index++]  = '`created_by`';
        }
        if ($this->isColumnModified(Jm3ContentPeer::CREATED_BY_ALIAS)) {
            $modifiedColumns[':p' . $index++]  = '`created_by_alias`';
        }
        if ($this->isColumnModified(Jm3ContentPeer::MODIFIED)) {
            $modifiedColumns[':p' . $index++]  = '`modified`';
        }
        if ($this->isColumnModified(Jm3ContentPeer::MODIFIED_BY)) {
            $modifiedColumns[':p' . $index++]  = '`modified_by`';
        }
        if ($this->isColumnModified(Jm3ContentPeer::CHECKED_OUT)) {
            $modifiedColumns[':p' . $index++]  = '`checked_out`';
        }
        if ($this->isColumnModified(Jm3ContentPeer::CHECKED_OUT_TIME)) {
            $modifiedColumns[':p' . $index++]  = '`checked_out_time`';
        }
        if ($this->isColumnModified(Jm3ContentPeer::PUBLISH_UP)) {
            $modifiedColumns[':p' . $index++]  = '`publish_up`';
        }
        if ($this->isColumnModified(Jm3ContentPeer::PUBLISH_DOWN)) {
            $modifiedColumns[':p' . $index++]  = '`publish_down`';
        }
        if ($this->isColumnModified(Jm3ContentPeer::IMAGES)) {
            $modifiedColumns[':p' . $index++]  = '`images`';
        }
        if ($this->isColumnModified(Jm3ContentPeer::URLS)) {
            $modifiedColumns[':p' . $index++]  = '`urls`';
        }
        if ($this->isColumnModified(Jm3ContentPeer::ATTRIBS)) {
            $modifiedColumns[':p' . $index++]  = '`attribs`';
        }
        if ($this->isColumnModified(Jm3ContentPeer::VERSION)) {
            $modifiedColumns[':p' . $index++]  = '`version`';
        }
        if ($this->isColumnModified(Jm3ContentPeer::ORDERING)) {
            $modifiedColumns[':p' . $index++]  = '`ordering`';
        }
        if ($this->isColumnModified(Jm3ContentPeer::METAKEY)) {
            $modifiedColumns[':p' . $index++]  = '`metakey`';
        }
        if ($this->isColumnModified(Jm3ContentPeer::METADESC)) {
            $modifiedColumns[':p' . $index++]  = '`metadesc`';
        }
        if ($this->isColumnModified(Jm3ContentPeer::ACCESS)) {
            $modifiedColumns[':p' . $index++]  = '`access`';
        }
        if ($this->isColumnModified(Jm3ContentPeer::HITS)) {
            $modifiedColumns[':p' . $index++]  = '`hits`';
        }
        if ($this->isColumnModified(Jm3ContentPeer::METADATA)) {
            $modifiedColumns[':p' . $index++]  = '`metadata`';
        }
        if ($this->isColumnModified(Jm3ContentPeer::FEATURED)) {
            $modifiedColumns[':p' . $index++]  = '`featured`';
        }
        if ($this->isColumnModified(Jm3ContentPeer::LANGUAGE)) {
            $modifiedColumns[':p' . $index++]  = '`language`';
        }
        if ($this->isColumnModified(Jm3ContentPeer::XREFERENCE)) {
            $modifiedColumns[':p' . $index++]  = '`xreference`';
        }

        $sql = sprintf(
            'INSERT INTO `jm3_content` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`asset_id`':
                        $stmt->bindValue($identifier, $this->asset_id, PDO::PARAM_INT);
                        break;
                    case '`title`':
                        $stmt->bindValue($identifier, $this->title, PDO::PARAM_STR);
                        break;
                    case '`alias`':
                        $stmt->bindValue($identifier, $this->alias, PDO::PARAM_STR);
                        break;
                    case '`introtext`':
                        $stmt->bindValue($identifier, $this->introtext, PDO::PARAM_STR);
                        break;
                    case '`fulltext`':
                        $stmt->bindValue($identifier, $this->fulltext, PDO::PARAM_STR);
                        break;
                    case '`state`':
                        $stmt->bindValue($identifier, $this->state, PDO::PARAM_INT);
                        break;
                    case '`catid`':
                        $stmt->bindValue($identifier, $this->catid, PDO::PARAM_INT);
                        break;
                    case '`created`':
                        $stmt->bindValue($identifier, $this->created, PDO::PARAM_STR);
                        break;
                    case '`created_by`':
                        $stmt->bindValue($identifier, $this->created_by, PDO::PARAM_INT);
                        break;
                    case '`created_by_alias`':
                        $stmt->bindValue($identifier, $this->created_by_alias, PDO::PARAM_STR);
                        break;
                    case '`modified`':
                        $stmt->bindValue($identifier, $this->modified, PDO::PARAM_STR);
                        break;
                    case '`modified_by`':
                        $stmt->bindValue($identifier, $this->modified_by, PDO::PARAM_INT);
                        break;
                    case '`checked_out`':
                        $stmt->bindValue($identifier, $this->checked_out, PDO::PARAM_INT);
                        break;
                    case '`checked_out_time`':
                        $stmt->bindValue($identifier, $this->checked_out_time, PDO::PARAM_STR);
                        break;
                    case '`publish_up`':
                        $stmt->bindValue($identifier, $this->publish_up, PDO::PARAM_STR);
                        break;
                    case '`publish_down`':
                        $stmt->bindValue($identifier, $this->publish_down, PDO::PARAM_STR);
                        break;
                    case '`images`':
                        $stmt->bindValue($identifier, $this->images, PDO::PARAM_STR);
                        break;
                    case '`urls`':
                        $stmt->bindValue($identifier, $this->urls, PDO::PARAM_STR);
                        break;
                    case '`attribs`':
                        $stmt->bindValue($identifier, $this->attribs, PDO::PARAM_STR);
                        break;
                    case '`version`':
                        $stmt->bindValue($identifier, $this->version, PDO::PARAM_INT);
                        break;
                    case '`ordering`':
                        $stmt->bindValue($identifier, $this->ordering, PDO::PARAM_INT);
                        break;
                    case '`metakey`':
                        $stmt->bindValue($identifier, $this->metakey, PDO::PARAM_STR);
                        break;
                    case '`metadesc`':
                        $stmt->bindValue($identifier, $this->metadesc, PDO::PARAM_STR);
                        break;
                    case '`access`':
                        $stmt->bindValue($identifier, $this->access, PDO::PARAM_INT);
                        break;
                    case '`hits`':
                        $stmt->bindValue($identifier, $this->hits, PDO::PARAM_INT);
                        break;
                    case '`metadata`':
                        $stmt->bindValue($identifier, $this->metadata, PDO::PARAM_STR);
                        break;
                    case '`featured`':
                        $stmt->bindValue($identifier, $this->featured, PDO::PARAM_INT);
                        break;
                    case '`language`':
                        $stmt->bindValue($identifier, $this->language, PDO::PARAM_STR);
                        break;
                    case '`xreference`':
                        $stmt->bindValue($identifier, $this->xreference, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            if (($retval = Jm3ContentPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }



            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = Jm3ContentPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getAssetId();
                break;
            case 2:
                return $this->getTitle();
                break;
            case 3:
                return $this->getAlias();
                break;
            case 4:
                return $this->getIntrotext();
                break;
            case 5:
                return $this->getFulltext();
                break;
            case 6:
                return $this->getState();
                break;
            case 7:
                return $this->getCatid();
                break;
            case 8:
                return $this->getCreated();
                break;
            case 9:
                return $this->getCreatedBy();
                break;
            case 10:
                return $this->getCreatedByAlias();
                break;
            case 11:
                return $this->getModified();
                break;
            case 12:
                return $this->getModifiedBy();
                break;
            case 13:
                return $this->getCheckedOut();
                break;
            case 14:
                return $this->getCheckedOutTime();
                break;
            case 15:
                return $this->getPublishUp();
                break;
            case 16:
                return $this->getPublishDown();
                break;
            case 17:
                return $this->getImages();
                break;
            case 18:
                return $this->getUrls();
                break;
            case 19:
                return $this->getAttribs();
                break;
            case 20:
                return $this->getVersion();
                break;
            case 21:
                return $this->getOrdering();
                break;
            case 22:
                return $this->getMetakey();
                break;
            case 23:
                return $this->getMetadesc();
                break;
            case 24:
                return $this->getAccess();
                break;
            case 25:
                return $this->getHits();
                break;
            case 26:
                return $this->getMetadata();
                break;
            case 27:
                return $this->getFeatured();
                break;
            case 28:
                return $this->getLanguage();
                break;
            case 29:
                return $this->getXreference();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array())
    {
        if (isset($alreadyDumpedObjects['Jm3Content'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Jm3Content'][$this->getPrimaryKey()] = true;
        $keys = Jm3ContentPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getAssetId(),
            $keys[2] => $this->getTitle(),
            $keys[3] => $this->getAlias(),
            $keys[4] => $this->getIntrotext(),
            $keys[5] => $this->getFulltext(),
            $keys[6] => $this->getState(),
            $keys[7] => $this->getCatid(),
            $keys[8] => $this->getCreated(),
            $keys[9] => $this->getCreatedBy(),
            $keys[10] => $this->getCreatedByAlias(),
            $keys[11] => $this->getModified(),
            $keys[12] => $this->getModifiedBy(),
            $keys[13] => $this->getCheckedOut(),
            $keys[14] => $this->getCheckedOutTime(),
            $keys[15] => $this->getPublishUp(),
            $keys[16] => $this->getPublishDown(),
            $keys[17] => $this->getImages(),
            $keys[18] => $this->getUrls(),
            $keys[19] => $this->getAttribs(),
            $keys[20] => $this->getVersion(),
            $keys[21] => $this->getOrdering(),
            $keys[22] => $this->getMetakey(),
            $keys[23] => $this->getMetadesc(),
            $keys[24] => $this->getAccess(),
            $keys[25] => $this->getHits(),
            $keys[26] => $this->getMetadata(),
            $keys[27] => $this->getFeatured(),
            $keys[28] => $this->getLanguage(),
            $keys[29] => $this->getXreference(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach($virtualColumns as $key => $virtualColumn)
        {
            $result[$key] = $virtualColumn;
        }


        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = Jm3ContentPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setAssetId($value);
                break;
            case 2:
                $this->setTitle($value);
                break;
            case 3:
                $this->setAlias($value);
                break;
            case 4:
                $this->setIntrotext($value);
                break;
            case 5:
                $this->setFulltext($value);
                break;
            case 6:
                $this->setState($value);
                break;
            case 7:
                $this->setCatid($value);
                break;
            case 8:
                $this->setCreated($value);
                break;
            case 9:
                $this->setCreatedBy($value);
                break;
            case 10:
                $this->setCreatedByAlias($value);
                break;
            case 11:
                $this->setModified($value);
                break;
            case 12:
                $this->setModifiedBy($value);
                break;
            case 13:
                $this->setCheckedOut($value);
                break;
            case 14:
                $this->setCheckedOutTime($value);
                break;
            case 15:
                $this->setPublishUp($value);
                break;
            case 16:
                $this->setPublishDown($value);
                break;
            case 17:
                $this->setImages($value);
                break;
            case 18:
                $this->setUrls($value);
                break;
            case 19:
                $this->setAttribs($value);
                break;
            case 20:
                $this->setVersion($value);
                break;
            case 21:
                $this->setOrdering($value);
                break;
            case 22:
                $this->setMetakey($value);
                break;
            case 23:
                $this->setMetadesc($value);
                break;
            case 24:
                $this->setAccess($value);
                break;
            case 25:
                $this->setHits($value);
                break;
            case 26:
                $this->setMetadata($value);
                break;
            case 27:
                $this->setFeatured($value);
                break;
            case 28:
                $this->setLanguage($value);
                break;
            case 29:
                $this->setXreference($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = Jm3ContentPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setAssetId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setTitle($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setAlias($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setIntrotext($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setFulltext($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setState($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setCatid($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setCreated($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setCreatedBy($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setCreatedByAlias($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setModified($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setModifiedBy($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setCheckedOut($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr)) $this->setCheckedOutTime($arr[$keys[14]]);
        if (array_key_exists($keys[15], $arr)) $this->setPublishUp($arr[$keys[15]]);
        if (array_key_exists($keys[16], $arr)) $this->setPublishDown($arr[$keys[16]]);
        if (array_key_exists($keys[17], $arr)) $this->setImages($arr[$keys[17]]);
        if (array_key_exists($keys[18], $arr)) $this->setUrls($arr[$keys[18]]);
        if (array_key_exists($keys[19], $arr)) $this->setAttribs($arr[$keys[19]]);
        if (array_key_exists($keys[20], $arr)) $this->setVersion($arr[$keys[20]]);
        if (array_key_exists($keys[21], $arr)) $this->setOrdering($arr[$keys[21]]);
        if (array_key_exists($keys[22], $arr)) $this->setMetakey($arr[$keys[22]]);
        if (array_key_exists($keys[23], $arr)) $this->setMetadesc($arr[$keys[23]]);
        if (array_key_exists($keys[24], $arr)) $this->setAccess($arr[$keys[24]]);
        if (array_key_exists($keys[25], $arr)) $this->setHits($arr[$keys[25]]);
        if (array_key_exists($keys[26], $arr)) $this->setMetadata($arr[$keys[26]]);
        if (array_key_exists($keys[27], $arr)) $this->setFeatured($arr[$keys[27]]);
        if (array_key_exists($keys[28], $arr)) $this->setLanguage($arr[$keys[28]]);
        if (array_key_exists($keys[29], $arr)) $this->setXreference($arr[$keys[29]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(Jm3ContentPeer::DATABASE_NAME);

        if ($this->isColumnModified(Jm3ContentPeer::ID)) $criteria->add(Jm3ContentPeer::ID, $this->id);
        if ($this->isColumnModified(Jm3ContentPeer::ASSET_ID)) $criteria->add(Jm3ContentPeer::ASSET_ID, $this->asset_id);
        if ($this->isColumnModified(Jm3ContentPeer::TITLE)) $criteria->add(Jm3ContentPeer::TITLE, $this->title);
        if ($this->isColumnModified(Jm3ContentPeer::ALIAS)) $criteria->add(Jm3ContentPeer::ALIAS, $this->alias);
        if ($this->isColumnModified(Jm3ContentPeer::INTROTEXT)) $criteria->add(Jm3ContentPeer::INTROTEXT, $this->introtext);
        if ($this->isColumnModified(Jm3ContentPeer::FULLTEXT)) $criteria->add(Jm3ContentPeer::FULLTEXT, $this->fulltext);
        if ($this->isColumnModified(Jm3ContentPeer::STATE)) $criteria->add(Jm3ContentPeer::STATE, $this->state);
        if ($this->isColumnModified(Jm3ContentPeer::CATID)) $criteria->add(Jm3ContentPeer::CATID, $this->catid);
        if ($this->isColumnModified(Jm3ContentPeer::CREATED)) $criteria->add(Jm3ContentPeer::CREATED, $this->created);
        if ($this->isColumnModified(Jm3ContentPeer::CREATED_BY)) $criteria->add(Jm3ContentPeer::CREATED_BY, $this->created_by);
        if ($this->isColumnModified(Jm3ContentPeer::CREATED_BY_ALIAS)) $criteria->add(Jm3ContentPeer::CREATED_BY_ALIAS, $this->created_by_alias);
        if ($this->isColumnModified(Jm3ContentPeer::MODIFIED)) $criteria->add(Jm3ContentPeer::MODIFIED, $this->modified);
        if ($this->isColumnModified(Jm3ContentPeer::MODIFIED_BY)) $criteria->add(Jm3ContentPeer::MODIFIED_BY, $this->modified_by);
        if ($this->isColumnModified(Jm3ContentPeer::CHECKED_OUT)) $criteria->add(Jm3ContentPeer::CHECKED_OUT, $this->checked_out);
        if ($this->isColumnModified(Jm3ContentPeer::CHECKED_OUT_TIME)) $criteria->add(Jm3ContentPeer::CHECKED_OUT_TIME, $this->checked_out_time);
        if ($this->isColumnModified(Jm3ContentPeer::PUBLISH_UP)) $criteria->add(Jm3ContentPeer::PUBLISH_UP, $this->publish_up);
        if ($this->isColumnModified(Jm3ContentPeer::PUBLISH_DOWN)) $criteria->add(Jm3ContentPeer::PUBLISH_DOWN, $this->publish_down);
        if ($this->isColumnModified(Jm3ContentPeer::IMAGES)) $criteria->add(Jm3ContentPeer::IMAGES, $this->images);
        if ($this->isColumnModified(Jm3ContentPeer::URLS)) $criteria->add(Jm3ContentPeer::URLS, $this->urls);
        if ($this->isColumnModified(Jm3ContentPeer::ATTRIBS)) $criteria->add(Jm3ContentPeer::ATTRIBS, $this->attribs);
        if ($this->isColumnModified(Jm3ContentPeer::VERSION)) $criteria->add(Jm3ContentPeer::VERSION, $this->version);
        if ($this->isColumnModified(Jm3ContentPeer::ORDERING)) $criteria->add(Jm3ContentPeer::ORDERING, $this->ordering);
        if ($this->isColumnModified(Jm3ContentPeer::METAKEY)) $criteria->add(Jm3ContentPeer::METAKEY, $this->metakey);
        if ($this->isColumnModified(Jm3ContentPeer::METADESC)) $criteria->add(Jm3ContentPeer::METADESC, $this->metadesc);
        if ($this->isColumnModified(Jm3ContentPeer::ACCESS)) $criteria->add(Jm3ContentPeer::ACCESS, $this->access);
        if ($this->isColumnModified(Jm3ContentPeer::HITS)) $criteria->add(Jm3ContentPeer::HITS, $this->hits);
        if ($this->isColumnModified(Jm3ContentPeer::METADATA)) $criteria->add(Jm3ContentPeer::METADATA, $this->metadata);
        if ($this->isColumnModified(Jm3ContentPeer::FEATURED)) $criteria->add(Jm3ContentPeer::FEATURED, $this->featured);
        if ($this->isColumnModified(Jm3ContentPeer::LANGUAGE)) $criteria->add(Jm3ContentPeer::LANGUAGE, $this->language);
        if ($this->isColumnModified(Jm3ContentPeer::XREFERENCE)) $criteria->add(Jm3ContentPeer::XREFERENCE, $this->xreference);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(Jm3ContentPeer::DATABASE_NAME);
        $criteria->add(Jm3ContentPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of Jm3Content (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setAssetId($this->getAssetId());
        $copyObj->setTitle($this->getTitle());
        $copyObj->setAlias($this->getAlias());
        $copyObj->setIntrotext($this->getIntrotext());
        $copyObj->setFulltext($this->getFulltext());
        $copyObj->setState($this->getState());
        $copyObj->setCatid($this->getCatid());
        $copyObj->setCreated($this->getCreated());
        $copyObj->setCreatedBy($this->getCreatedBy());
        $copyObj->setCreatedByAlias($this->getCreatedByAlias());
        $copyObj->setModified($this->getModified());
        $copyObj->setModifiedBy($this->getModifiedBy());
        $copyObj->setCheckedOut($this->getCheckedOut());
        $copyObj->setCheckedOutTime($this->getCheckedOutTime());
        $copyObj->setPublishUp($this->getPublishUp());
        $copyObj->setPublishDown($this->getPublishDown());
        $copyObj->setImages($this->getImages());
        $copyObj->setUrls($this->getUrls());
        $copyObj->setAttribs($this->getAttribs());
        $copyObj->setVersion($this->getVersion());
        $copyObj->setOrdering($this->getOrdering());
        $copyObj->setMetakey($this->getMetakey());
        $copyObj->setMetadesc($this->getMetadesc());
        $copyObj->setAccess($this->getAccess());
        $copyObj->setHits($this->getHits());
        $copyObj->setMetadata($this->getMetadata());
        $copyObj->setFeatured($this->getFeatured());
        $copyObj->setLanguage($this->getLanguage());
        $copyObj->setXreference($this->getXreference());
        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return Jm3Content Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return Jm3ContentPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new Jm3ContentPeer();
        }

        return self::$peer;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->asset_id = null;
        $this->title = null;
        $this->alias = null;
        $this->introtext = null;
        $this->fulltext = null;
        $this->state = null;
        $this->catid = null;
        $this->created = null;
        $this->created_by = null;
        $this->created_by_alias = null;
        $this->modified = null;
        $this->modified_by = null;
        $this->checked_out = null;
        $this->checked_out_time = null;
        $this->publish_up = null;
        $this->publish_down = null;
        $this->images = null;
        $this->urls = null;
        $this->attribs = null;
        $this->version = null;
        $this->ordering = null;
        $this->metakey = null;
        $this->metadesc = null;
        $this->access = null;
        $this->hits = null;
        $this->metadata = null;
        $this->featured = null;
        $this->language = null;
        $this->xreference = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(Jm3ContentPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
