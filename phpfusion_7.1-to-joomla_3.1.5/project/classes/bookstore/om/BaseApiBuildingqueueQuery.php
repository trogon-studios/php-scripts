<?php


/**
 * Base class that represents a query for the 'api_buildingQueue' table.
 *
 *
 *
 * @method ApiBuildingqueueQuery orderById($order = Criteria::ASC) Order by the id column
 * @method ApiBuildingqueueQuery orderByLevel($order = Criteria::ASC) Order by the level column
 * @method ApiBuildingqueueQuery orderByPositionx($order = Criteria::ASC) Order by the positionX column
 * @method ApiBuildingqueueQuery orderByPositiony($order = Criteria::ASC) Order by the positionY column
 * @method ApiBuildingqueueQuery orderByDateoffinish($order = Criteria::ASC) Order by the dateOfFinish column
 * @method ApiBuildingqueueQuery orderByBuildingqueueBuildingid($order = Criteria::ASC) Order by the buildingQueue_buildingId column
 * @method ApiBuildingqueueQuery orderByBuildingqueueBuildingtype($order = Criteria::ASC) Order by the buildingQueue_buildingType column
 * @method ApiBuildingqueueQuery orderByBuildingqueuePlayer($order = Criteria::ASC) Order by the buildingQueue_player column
 * @method ApiBuildingqueueQuery orderByBuildingqueuePlot($order = Criteria::ASC) Order by the buildingQueue_plot column
 *
 * @method ApiBuildingqueueQuery groupById() Group by the id column
 * @method ApiBuildingqueueQuery groupByLevel() Group by the level column
 * @method ApiBuildingqueueQuery groupByPositionx() Group by the positionX column
 * @method ApiBuildingqueueQuery groupByPositiony() Group by the positionY column
 * @method ApiBuildingqueueQuery groupByDateoffinish() Group by the dateOfFinish column
 * @method ApiBuildingqueueQuery groupByBuildingqueueBuildingid() Group by the buildingQueue_buildingId column
 * @method ApiBuildingqueueQuery groupByBuildingqueueBuildingtype() Group by the buildingQueue_buildingType column
 * @method ApiBuildingqueueQuery groupByBuildingqueuePlayer() Group by the buildingQueue_player column
 * @method ApiBuildingqueueQuery groupByBuildingqueuePlot() Group by the buildingQueue_plot column
 *
 * @method ApiBuildingqueueQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method ApiBuildingqueueQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method ApiBuildingqueueQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method ApiBuildingqueue findOne(PropelPDO $con = null) Return the first ApiBuildingqueue matching the query
 * @method ApiBuildingqueue findOneOrCreate(PropelPDO $con = null) Return the first ApiBuildingqueue matching the query, or a new ApiBuildingqueue object populated from the query conditions when no match is found
 *
 * @method ApiBuildingqueue findOneByLevel(int $level) Return the first ApiBuildingqueue filtered by the level column
 * @method ApiBuildingqueue findOneByPositionx(int $positionX) Return the first ApiBuildingqueue filtered by the positionX column
 * @method ApiBuildingqueue findOneByPositiony(int $positionY) Return the first ApiBuildingqueue filtered by the positionY column
 * @method ApiBuildingqueue findOneByDateoffinish(string $dateOfFinish) Return the first ApiBuildingqueue filtered by the dateOfFinish column
 * @method ApiBuildingqueue findOneByBuildingqueueBuildingid(string $buildingQueue_buildingId) Return the first ApiBuildingqueue filtered by the buildingQueue_buildingId column
 * @method ApiBuildingqueue findOneByBuildingqueueBuildingtype(int $buildingQueue_buildingType) Return the first ApiBuildingqueue filtered by the buildingQueue_buildingType column
 * @method ApiBuildingqueue findOneByBuildingqueuePlayer(string $buildingQueue_player) Return the first ApiBuildingqueue filtered by the buildingQueue_player column
 * @method ApiBuildingqueue findOneByBuildingqueuePlot(string $buildingQueue_plot) Return the first ApiBuildingqueue filtered by the buildingQueue_plot column
 *
 * @method array findById(string $id) Return ApiBuildingqueue objects filtered by the id column
 * @method array findByLevel(int $level) Return ApiBuildingqueue objects filtered by the level column
 * @method array findByPositionx(int $positionX) Return ApiBuildingqueue objects filtered by the positionX column
 * @method array findByPositiony(int $positionY) Return ApiBuildingqueue objects filtered by the positionY column
 * @method array findByDateoffinish(string $dateOfFinish) Return ApiBuildingqueue objects filtered by the dateOfFinish column
 * @method array findByBuildingqueueBuildingid(string $buildingQueue_buildingId) Return ApiBuildingqueue objects filtered by the buildingQueue_buildingId column
 * @method array findByBuildingqueueBuildingtype(int $buildingQueue_buildingType) Return ApiBuildingqueue objects filtered by the buildingQueue_buildingType column
 * @method array findByBuildingqueuePlayer(string $buildingQueue_player) Return ApiBuildingqueue objects filtered by the buildingQueue_player column
 * @method array findByBuildingqueuePlot(string $buildingQueue_plot) Return ApiBuildingqueue objects filtered by the buildingQueue_plot column
 *
 * @package    propel.generator.bookstore.om
 */
abstract class BaseApiBuildingqueueQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseApiBuildingqueueQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'bookstore';
        }
        if (null === $modelName) {
            $modelName = 'ApiBuildingqueue';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ApiBuildingqueueQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   ApiBuildingqueueQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return ApiBuildingqueueQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof ApiBuildingqueueQuery) {
            return $criteria;
        }
        $query = new ApiBuildingqueueQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   ApiBuildingqueue|ApiBuildingqueue[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = ApiBuildingqueuePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(ApiBuildingqueuePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 ApiBuildingqueue A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 ApiBuildingqueue A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `level`, `positionX`, `positionY`, `dateOfFinish`, `buildingQueue_buildingId`, `buildingQueue_buildingType`, `buildingQueue_player`, `buildingQueue_plot` FROM `api_buildingQueue` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new ApiBuildingqueue();
            $obj->hydrate($row);
            ApiBuildingqueuePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return ApiBuildingqueue|ApiBuildingqueue[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|ApiBuildingqueue[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return ApiBuildingqueueQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ApiBuildingqueuePeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return ApiBuildingqueueQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ApiBuildingqueuePeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiBuildingqueueQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ApiBuildingqueuePeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ApiBuildingqueuePeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiBuildingqueuePeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the level column
     *
     * Example usage:
     * <code>
     * $query->filterByLevel(1234); // WHERE level = 1234
     * $query->filterByLevel(array(12, 34)); // WHERE level IN (12, 34)
     * $query->filterByLevel(array('min' => 12)); // WHERE level >= 12
     * $query->filterByLevel(array('max' => 12)); // WHERE level <= 12
     * </code>
     *
     * @param     mixed $level The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiBuildingqueueQuery The current query, for fluid interface
     */
    public function filterByLevel($level = null, $comparison = null)
    {
        if (is_array($level)) {
            $useMinMax = false;
            if (isset($level['min'])) {
                $this->addUsingAlias(ApiBuildingqueuePeer::LEVEL, $level['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($level['max'])) {
                $this->addUsingAlias(ApiBuildingqueuePeer::LEVEL, $level['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiBuildingqueuePeer::LEVEL, $level, $comparison);
    }

    /**
     * Filter the query on the positionX column
     *
     * Example usage:
     * <code>
     * $query->filterByPositionx(1234); // WHERE positionX = 1234
     * $query->filterByPositionx(array(12, 34)); // WHERE positionX IN (12, 34)
     * $query->filterByPositionx(array('min' => 12)); // WHERE positionX >= 12
     * $query->filterByPositionx(array('max' => 12)); // WHERE positionX <= 12
     * </code>
     *
     * @param     mixed $positionx The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiBuildingqueueQuery The current query, for fluid interface
     */
    public function filterByPositionx($positionx = null, $comparison = null)
    {
        if (is_array($positionx)) {
            $useMinMax = false;
            if (isset($positionx['min'])) {
                $this->addUsingAlias(ApiBuildingqueuePeer::POSITIONX, $positionx['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($positionx['max'])) {
                $this->addUsingAlias(ApiBuildingqueuePeer::POSITIONX, $positionx['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiBuildingqueuePeer::POSITIONX, $positionx, $comparison);
    }

    /**
     * Filter the query on the positionY column
     *
     * Example usage:
     * <code>
     * $query->filterByPositiony(1234); // WHERE positionY = 1234
     * $query->filterByPositiony(array(12, 34)); // WHERE positionY IN (12, 34)
     * $query->filterByPositiony(array('min' => 12)); // WHERE positionY >= 12
     * $query->filterByPositiony(array('max' => 12)); // WHERE positionY <= 12
     * </code>
     *
     * @param     mixed $positiony The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiBuildingqueueQuery The current query, for fluid interface
     */
    public function filterByPositiony($positiony = null, $comparison = null)
    {
        if (is_array($positiony)) {
            $useMinMax = false;
            if (isset($positiony['min'])) {
                $this->addUsingAlias(ApiBuildingqueuePeer::POSITIONY, $positiony['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($positiony['max'])) {
                $this->addUsingAlias(ApiBuildingqueuePeer::POSITIONY, $positiony['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiBuildingqueuePeer::POSITIONY, $positiony, $comparison);
    }

    /**
     * Filter the query on the dateOfFinish column
     *
     * Example usage:
     * <code>
     * $query->filterByDateoffinish('2011-03-14'); // WHERE dateOfFinish = '2011-03-14'
     * $query->filterByDateoffinish('now'); // WHERE dateOfFinish = '2011-03-14'
     * $query->filterByDateoffinish(array('max' => 'yesterday')); // WHERE dateOfFinish < '2011-03-13'
     * </code>
     *
     * @param     mixed $dateoffinish The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiBuildingqueueQuery The current query, for fluid interface
     */
    public function filterByDateoffinish($dateoffinish = null, $comparison = null)
    {
        if (is_array($dateoffinish)) {
            $useMinMax = false;
            if (isset($dateoffinish['min'])) {
                $this->addUsingAlias(ApiBuildingqueuePeer::DATEOFFINISH, $dateoffinish['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateoffinish['max'])) {
                $this->addUsingAlias(ApiBuildingqueuePeer::DATEOFFINISH, $dateoffinish['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiBuildingqueuePeer::DATEOFFINISH, $dateoffinish, $comparison);
    }

    /**
     * Filter the query on the buildingQueue_buildingId column
     *
     * Example usage:
     * <code>
     * $query->filterByBuildingqueueBuildingid(1234); // WHERE buildingQueue_buildingId = 1234
     * $query->filterByBuildingqueueBuildingid(array(12, 34)); // WHERE buildingQueue_buildingId IN (12, 34)
     * $query->filterByBuildingqueueBuildingid(array('min' => 12)); // WHERE buildingQueue_buildingId >= 12
     * $query->filterByBuildingqueueBuildingid(array('max' => 12)); // WHERE buildingQueue_buildingId <= 12
     * </code>
     *
     * @param     mixed $buildingqueueBuildingid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiBuildingqueueQuery The current query, for fluid interface
     */
    public function filterByBuildingqueueBuildingid($buildingqueueBuildingid = null, $comparison = null)
    {
        if (is_array($buildingqueueBuildingid)) {
            $useMinMax = false;
            if (isset($buildingqueueBuildingid['min'])) {
                $this->addUsingAlias(ApiBuildingqueuePeer::BUILDINGQUEUE_BUILDINGID, $buildingqueueBuildingid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($buildingqueueBuildingid['max'])) {
                $this->addUsingAlias(ApiBuildingqueuePeer::BUILDINGQUEUE_BUILDINGID, $buildingqueueBuildingid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiBuildingqueuePeer::BUILDINGQUEUE_BUILDINGID, $buildingqueueBuildingid, $comparison);
    }

    /**
     * Filter the query on the buildingQueue_buildingType column
     *
     * Example usage:
     * <code>
     * $query->filterByBuildingqueueBuildingtype(1234); // WHERE buildingQueue_buildingType = 1234
     * $query->filterByBuildingqueueBuildingtype(array(12, 34)); // WHERE buildingQueue_buildingType IN (12, 34)
     * $query->filterByBuildingqueueBuildingtype(array('min' => 12)); // WHERE buildingQueue_buildingType >= 12
     * $query->filterByBuildingqueueBuildingtype(array('max' => 12)); // WHERE buildingQueue_buildingType <= 12
     * </code>
     *
     * @param     mixed $buildingqueueBuildingtype The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiBuildingqueueQuery The current query, for fluid interface
     */
    public function filterByBuildingqueueBuildingtype($buildingqueueBuildingtype = null, $comparison = null)
    {
        if (is_array($buildingqueueBuildingtype)) {
            $useMinMax = false;
            if (isset($buildingqueueBuildingtype['min'])) {
                $this->addUsingAlias(ApiBuildingqueuePeer::BUILDINGQUEUE_BUILDINGTYPE, $buildingqueueBuildingtype['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($buildingqueueBuildingtype['max'])) {
                $this->addUsingAlias(ApiBuildingqueuePeer::BUILDINGQUEUE_BUILDINGTYPE, $buildingqueueBuildingtype['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiBuildingqueuePeer::BUILDINGQUEUE_BUILDINGTYPE, $buildingqueueBuildingtype, $comparison);
    }

    /**
     * Filter the query on the buildingQueue_player column
     *
     * Example usage:
     * <code>
     * $query->filterByBuildingqueuePlayer(1234); // WHERE buildingQueue_player = 1234
     * $query->filterByBuildingqueuePlayer(array(12, 34)); // WHERE buildingQueue_player IN (12, 34)
     * $query->filterByBuildingqueuePlayer(array('min' => 12)); // WHERE buildingQueue_player >= 12
     * $query->filterByBuildingqueuePlayer(array('max' => 12)); // WHERE buildingQueue_player <= 12
     * </code>
     *
     * @param     mixed $buildingqueuePlayer The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiBuildingqueueQuery The current query, for fluid interface
     */
    public function filterByBuildingqueuePlayer($buildingqueuePlayer = null, $comparison = null)
    {
        if (is_array($buildingqueuePlayer)) {
            $useMinMax = false;
            if (isset($buildingqueuePlayer['min'])) {
                $this->addUsingAlias(ApiBuildingqueuePeer::BUILDINGQUEUE_PLAYER, $buildingqueuePlayer['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($buildingqueuePlayer['max'])) {
                $this->addUsingAlias(ApiBuildingqueuePeer::BUILDINGQUEUE_PLAYER, $buildingqueuePlayer['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiBuildingqueuePeer::BUILDINGQUEUE_PLAYER, $buildingqueuePlayer, $comparison);
    }

    /**
     * Filter the query on the buildingQueue_plot column
     *
     * Example usage:
     * <code>
     * $query->filterByBuildingqueuePlot(1234); // WHERE buildingQueue_plot = 1234
     * $query->filterByBuildingqueuePlot(array(12, 34)); // WHERE buildingQueue_plot IN (12, 34)
     * $query->filterByBuildingqueuePlot(array('min' => 12)); // WHERE buildingQueue_plot >= 12
     * $query->filterByBuildingqueuePlot(array('max' => 12)); // WHERE buildingQueue_plot <= 12
     * </code>
     *
     * @param     mixed $buildingqueuePlot The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiBuildingqueueQuery The current query, for fluid interface
     */
    public function filterByBuildingqueuePlot($buildingqueuePlot = null, $comparison = null)
    {
        if (is_array($buildingqueuePlot)) {
            $useMinMax = false;
            if (isset($buildingqueuePlot['min'])) {
                $this->addUsingAlias(ApiBuildingqueuePeer::BUILDINGQUEUE_PLOT, $buildingqueuePlot['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($buildingqueuePlot['max'])) {
                $this->addUsingAlias(ApiBuildingqueuePeer::BUILDINGQUEUE_PLOT, $buildingqueuePlot['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiBuildingqueuePeer::BUILDINGQUEUE_PLOT, $buildingqueuePlot, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ApiBuildingqueue $apiBuildingqueue Object to remove from the list of results
     *
     * @return ApiBuildingqueueQuery The current query, for fluid interface
     */
    public function prune($apiBuildingqueue = null)
    {
        if ($apiBuildingqueue) {
            $this->addUsingAlias(ApiBuildingqueuePeer::ID, $apiBuildingqueue->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
