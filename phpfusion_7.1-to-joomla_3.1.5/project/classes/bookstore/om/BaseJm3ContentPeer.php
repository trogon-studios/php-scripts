<?php


/**
 * Base static class for performing query and update operations on the 'jm3_content' table.
 *
 *
 *
 * @package propel.generator.bookstore.om
 */
abstract class BaseJm3ContentPeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'bookstore';

    /** the table name for this class */
    const TABLE_NAME = 'jm3_content';

    /** the related Propel class for this table */
    const OM_CLASS = 'Jm3Content';

    /** the related TableMap class for this table */
    const TM_CLASS = 'Jm3ContentTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 30;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 30;

    /** the column name for the id field */
    const ID = 'jm3_content.id';

    /** the column name for the asset_id field */
    const ASSET_ID = 'jm3_content.asset_id';

    /** the column name for the title field */
    const TITLE = 'jm3_content.title';

    /** the column name for the alias field */
    const ALIAS = 'jm3_content.alias';

    /** the column name for the introtext field */
    const INTROTEXT = 'jm3_content.introtext';

    /** the column name for the fulltext field */
    const FULLTEXT = 'jm3_content.fulltext';

    /** the column name for the state field */
    const STATE = 'jm3_content.state';

    /** the column name for the catid field */
    const CATID = 'jm3_content.catid';

    /** the column name for the created field */
    const CREATED = 'jm3_content.created';

    /** the column name for the created_by field */
    const CREATED_BY = 'jm3_content.created_by';

    /** the column name for the created_by_alias field */
    const CREATED_BY_ALIAS = 'jm3_content.created_by_alias';

    /** the column name for the modified field */
    const MODIFIED = 'jm3_content.modified';

    /** the column name for the modified_by field */
    const MODIFIED_BY = 'jm3_content.modified_by';

    /** the column name for the checked_out field */
    const CHECKED_OUT = 'jm3_content.checked_out';

    /** the column name for the checked_out_time field */
    const CHECKED_OUT_TIME = 'jm3_content.checked_out_time';

    /** the column name for the publish_up field */
    const PUBLISH_UP = 'jm3_content.publish_up';

    /** the column name for the publish_down field */
    const PUBLISH_DOWN = 'jm3_content.publish_down';

    /** the column name for the images field */
    const IMAGES = 'jm3_content.images';

    /** the column name for the urls field */
    const URLS = 'jm3_content.urls';

    /** the column name for the attribs field */
    const ATTRIBS = 'jm3_content.attribs';

    /** the column name for the version field */
    const VERSION = 'jm3_content.version';

    /** the column name for the ordering field */
    const ORDERING = 'jm3_content.ordering';

    /** the column name for the metakey field */
    const METAKEY = 'jm3_content.metakey';

    /** the column name for the metadesc field */
    const METADESC = 'jm3_content.metadesc';

    /** the column name for the access field */
    const ACCESS = 'jm3_content.access';

    /** the column name for the hits field */
    const HITS = 'jm3_content.hits';

    /** the column name for the metadata field */
    const METADATA = 'jm3_content.metadata';

    /** the column name for the featured field */
    const FEATURED = 'jm3_content.featured';

    /** the column name for the language field */
    const LANGUAGE = 'jm3_content.language';

    /** the column name for the xreference field */
    const XREFERENCE = 'jm3_content.xreference';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identity map to hold any loaded instances of Jm3Content objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array Jm3Content[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. Jm3ContentPeer::$fieldNames[Jm3ContentPeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('Id', 'AssetId', 'Title', 'Alias', 'Introtext', 'Fulltext', 'State', 'Catid', 'Created', 'CreatedBy', 'CreatedByAlias', 'Modified', 'ModifiedBy', 'CheckedOut', 'CheckedOutTime', 'PublishUp', 'PublishDown', 'Images', 'Urls', 'Attribs', 'Version', 'Ordering', 'Metakey', 'Metadesc', 'Access', 'Hits', 'Metadata', 'Featured', 'Language', 'Xreference', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id', 'assetId', 'title', 'alias', 'introtext', 'fulltext', 'state', 'catid', 'created', 'createdBy', 'createdByAlias', 'modified', 'modifiedBy', 'checkedOut', 'checkedOutTime', 'publishUp', 'publishDown', 'images', 'urls', 'attribs', 'version', 'ordering', 'metakey', 'metadesc', 'access', 'hits', 'metadata', 'featured', 'language', 'xreference', ),
        BasePeer::TYPE_COLNAME => array (Jm3ContentPeer::ID, Jm3ContentPeer::ASSET_ID, Jm3ContentPeer::TITLE, Jm3ContentPeer::ALIAS, Jm3ContentPeer::INTROTEXT, Jm3ContentPeer::FULLTEXT, Jm3ContentPeer::STATE, Jm3ContentPeer::CATID, Jm3ContentPeer::CREATED, Jm3ContentPeer::CREATED_BY, Jm3ContentPeer::CREATED_BY_ALIAS, Jm3ContentPeer::MODIFIED, Jm3ContentPeer::MODIFIED_BY, Jm3ContentPeer::CHECKED_OUT, Jm3ContentPeer::CHECKED_OUT_TIME, Jm3ContentPeer::PUBLISH_UP, Jm3ContentPeer::PUBLISH_DOWN, Jm3ContentPeer::IMAGES, Jm3ContentPeer::URLS, Jm3ContentPeer::ATTRIBS, Jm3ContentPeer::VERSION, Jm3ContentPeer::ORDERING, Jm3ContentPeer::METAKEY, Jm3ContentPeer::METADESC, Jm3ContentPeer::ACCESS, Jm3ContentPeer::HITS, Jm3ContentPeer::METADATA, Jm3ContentPeer::FEATURED, Jm3ContentPeer::LANGUAGE, Jm3ContentPeer::XREFERENCE, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID', 'ASSET_ID', 'TITLE', 'ALIAS', 'INTROTEXT', 'FULLTEXT', 'STATE', 'CATID', 'CREATED', 'CREATED_BY', 'CREATED_BY_ALIAS', 'MODIFIED', 'MODIFIED_BY', 'CHECKED_OUT', 'CHECKED_OUT_TIME', 'PUBLISH_UP', 'PUBLISH_DOWN', 'IMAGES', 'URLS', 'ATTRIBS', 'VERSION', 'ORDERING', 'METAKEY', 'METADESC', 'ACCESS', 'HITS', 'METADATA', 'FEATURED', 'LANGUAGE', 'XREFERENCE', ),
        BasePeer::TYPE_FIELDNAME => array ('id', 'asset_id', 'title', 'alias', 'introtext', 'fulltext', 'state', 'catid', 'created', 'created_by', 'created_by_alias', 'modified', 'modified_by', 'checked_out', 'checked_out_time', 'publish_up', 'publish_down', 'images', 'urls', 'attribs', 'version', 'ordering', 'metakey', 'metadesc', 'access', 'hits', 'metadata', 'featured', 'language', 'xreference', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. Jm3ContentPeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'AssetId' => 1, 'Title' => 2, 'Alias' => 3, 'Introtext' => 4, 'Fulltext' => 5, 'State' => 6, 'Catid' => 7, 'Created' => 8, 'CreatedBy' => 9, 'CreatedByAlias' => 10, 'Modified' => 11, 'ModifiedBy' => 12, 'CheckedOut' => 13, 'CheckedOutTime' => 14, 'PublishUp' => 15, 'PublishDown' => 16, 'Images' => 17, 'Urls' => 18, 'Attribs' => 19, 'Version' => 20, 'Ordering' => 21, 'Metakey' => 22, 'Metadesc' => 23, 'Access' => 24, 'Hits' => 25, 'Metadata' => 26, 'Featured' => 27, 'Language' => 28, 'Xreference' => 29, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id' => 0, 'assetId' => 1, 'title' => 2, 'alias' => 3, 'introtext' => 4, 'fulltext' => 5, 'state' => 6, 'catid' => 7, 'created' => 8, 'createdBy' => 9, 'createdByAlias' => 10, 'modified' => 11, 'modifiedBy' => 12, 'checkedOut' => 13, 'checkedOutTime' => 14, 'publishUp' => 15, 'publishDown' => 16, 'images' => 17, 'urls' => 18, 'attribs' => 19, 'version' => 20, 'ordering' => 21, 'metakey' => 22, 'metadesc' => 23, 'access' => 24, 'hits' => 25, 'metadata' => 26, 'featured' => 27, 'language' => 28, 'xreference' => 29, ),
        BasePeer::TYPE_COLNAME => array (Jm3ContentPeer::ID => 0, Jm3ContentPeer::ASSET_ID => 1, Jm3ContentPeer::TITLE => 2, Jm3ContentPeer::ALIAS => 3, Jm3ContentPeer::INTROTEXT => 4, Jm3ContentPeer::FULLTEXT => 5, Jm3ContentPeer::STATE => 6, Jm3ContentPeer::CATID => 7, Jm3ContentPeer::CREATED => 8, Jm3ContentPeer::CREATED_BY => 9, Jm3ContentPeer::CREATED_BY_ALIAS => 10, Jm3ContentPeer::MODIFIED => 11, Jm3ContentPeer::MODIFIED_BY => 12, Jm3ContentPeer::CHECKED_OUT => 13, Jm3ContentPeer::CHECKED_OUT_TIME => 14, Jm3ContentPeer::PUBLISH_UP => 15, Jm3ContentPeer::PUBLISH_DOWN => 16, Jm3ContentPeer::IMAGES => 17, Jm3ContentPeer::URLS => 18, Jm3ContentPeer::ATTRIBS => 19, Jm3ContentPeer::VERSION => 20, Jm3ContentPeer::ORDERING => 21, Jm3ContentPeer::METAKEY => 22, Jm3ContentPeer::METADESC => 23, Jm3ContentPeer::ACCESS => 24, Jm3ContentPeer::HITS => 25, Jm3ContentPeer::METADATA => 26, Jm3ContentPeer::FEATURED => 27, Jm3ContentPeer::LANGUAGE => 28, Jm3ContentPeer::XREFERENCE => 29, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID' => 0, 'ASSET_ID' => 1, 'TITLE' => 2, 'ALIAS' => 3, 'INTROTEXT' => 4, 'FULLTEXT' => 5, 'STATE' => 6, 'CATID' => 7, 'CREATED' => 8, 'CREATED_BY' => 9, 'CREATED_BY_ALIAS' => 10, 'MODIFIED' => 11, 'MODIFIED_BY' => 12, 'CHECKED_OUT' => 13, 'CHECKED_OUT_TIME' => 14, 'PUBLISH_UP' => 15, 'PUBLISH_DOWN' => 16, 'IMAGES' => 17, 'URLS' => 18, 'ATTRIBS' => 19, 'VERSION' => 20, 'ORDERING' => 21, 'METAKEY' => 22, 'METADESC' => 23, 'ACCESS' => 24, 'HITS' => 25, 'METADATA' => 26, 'FEATURED' => 27, 'LANGUAGE' => 28, 'XREFERENCE' => 29, ),
        BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'asset_id' => 1, 'title' => 2, 'alias' => 3, 'introtext' => 4, 'fulltext' => 5, 'state' => 6, 'catid' => 7, 'created' => 8, 'created_by' => 9, 'created_by_alias' => 10, 'modified' => 11, 'modified_by' => 12, 'checked_out' => 13, 'checked_out_time' => 14, 'publish_up' => 15, 'publish_down' => 16, 'images' => 17, 'urls' => 18, 'attribs' => 19, 'version' => 20, 'ordering' => 21, 'metakey' => 22, 'metadesc' => 23, 'access' => 24, 'hits' => 25, 'metadata' => 26, 'featured' => 27, 'language' => 28, 'xreference' => 29, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, )
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = Jm3ContentPeer::getFieldNames($toType);
        $key = isset(Jm3ContentPeer::$fieldKeys[$fromType][$name]) ? Jm3ContentPeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(Jm3ContentPeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, Jm3ContentPeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return Jm3ContentPeer::$fieldNames[$type];
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. Jm3ContentPeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(Jm3ContentPeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(Jm3ContentPeer::ID);
            $criteria->addSelectColumn(Jm3ContentPeer::ASSET_ID);
            $criteria->addSelectColumn(Jm3ContentPeer::TITLE);
            $criteria->addSelectColumn(Jm3ContentPeer::ALIAS);
            $criteria->addSelectColumn(Jm3ContentPeer::INTROTEXT);
            $criteria->addSelectColumn(Jm3ContentPeer::FULLTEXT);
            $criteria->addSelectColumn(Jm3ContentPeer::STATE);
            $criteria->addSelectColumn(Jm3ContentPeer::CATID);
            $criteria->addSelectColumn(Jm3ContentPeer::CREATED);
            $criteria->addSelectColumn(Jm3ContentPeer::CREATED_BY);
            $criteria->addSelectColumn(Jm3ContentPeer::CREATED_BY_ALIAS);
            $criteria->addSelectColumn(Jm3ContentPeer::MODIFIED);
            $criteria->addSelectColumn(Jm3ContentPeer::MODIFIED_BY);
            $criteria->addSelectColumn(Jm3ContentPeer::CHECKED_OUT);
            $criteria->addSelectColumn(Jm3ContentPeer::CHECKED_OUT_TIME);
            $criteria->addSelectColumn(Jm3ContentPeer::PUBLISH_UP);
            $criteria->addSelectColumn(Jm3ContentPeer::PUBLISH_DOWN);
            $criteria->addSelectColumn(Jm3ContentPeer::IMAGES);
            $criteria->addSelectColumn(Jm3ContentPeer::URLS);
            $criteria->addSelectColumn(Jm3ContentPeer::ATTRIBS);
            $criteria->addSelectColumn(Jm3ContentPeer::VERSION);
            $criteria->addSelectColumn(Jm3ContentPeer::ORDERING);
            $criteria->addSelectColumn(Jm3ContentPeer::METAKEY);
            $criteria->addSelectColumn(Jm3ContentPeer::METADESC);
            $criteria->addSelectColumn(Jm3ContentPeer::ACCESS);
            $criteria->addSelectColumn(Jm3ContentPeer::HITS);
            $criteria->addSelectColumn(Jm3ContentPeer::METADATA);
            $criteria->addSelectColumn(Jm3ContentPeer::FEATURED);
            $criteria->addSelectColumn(Jm3ContentPeer::LANGUAGE);
            $criteria->addSelectColumn(Jm3ContentPeer::XREFERENCE);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.asset_id');
            $criteria->addSelectColumn($alias . '.title');
            $criteria->addSelectColumn($alias . '.alias');
            $criteria->addSelectColumn($alias . '.introtext');
            $criteria->addSelectColumn($alias . '.fulltext');
            $criteria->addSelectColumn($alias . '.state');
            $criteria->addSelectColumn($alias . '.catid');
            $criteria->addSelectColumn($alias . '.created');
            $criteria->addSelectColumn($alias . '.created_by');
            $criteria->addSelectColumn($alias . '.created_by_alias');
            $criteria->addSelectColumn($alias . '.modified');
            $criteria->addSelectColumn($alias . '.modified_by');
            $criteria->addSelectColumn($alias . '.checked_out');
            $criteria->addSelectColumn($alias . '.checked_out_time');
            $criteria->addSelectColumn($alias . '.publish_up');
            $criteria->addSelectColumn($alias . '.publish_down');
            $criteria->addSelectColumn($alias . '.images');
            $criteria->addSelectColumn($alias . '.urls');
            $criteria->addSelectColumn($alias . '.attribs');
            $criteria->addSelectColumn($alias . '.version');
            $criteria->addSelectColumn($alias . '.ordering');
            $criteria->addSelectColumn($alias . '.metakey');
            $criteria->addSelectColumn($alias . '.metadesc');
            $criteria->addSelectColumn($alias . '.access');
            $criteria->addSelectColumn($alias . '.hits');
            $criteria->addSelectColumn($alias . '.metadata');
            $criteria->addSelectColumn($alias . '.featured');
            $criteria->addSelectColumn($alias . '.language');
            $criteria->addSelectColumn($alias . '.xreference');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(Jm3ContentPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            Jm3ContentPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(Jm3ContentPeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(Jm3ContentPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return Jm3Content
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = Jm3ContentPeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return Jm3ContentPeer::populateObjects(Jm3ContentPeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(Jm3ContentPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            Jm3ContentPeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(Jm3ContentPeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param Jm3Content $obj A Jm3Content object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getId();
            } // if key === null
            Jm3ContentPeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A Jm3Content object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof Jm3Content) {
                $key = (string) $value->getId();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or Jm3Content object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(Jm3ContentPeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return Jm3Content Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(Jm3ContentPeer::$instances[$key])) {
                return Jm3ContentPeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }

    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references) {
        foreach (Jm3ContentPeer::$instances as $instance) {
          $instance->clearAllReferences(true);
        }
      }
        Jm3ContentPeer::$instances = array();
    }

    /**
     * Method to invalidate the instance pool of all tables related to jm3_content
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null) {
            return null;
        }

        return (string) $row[$startcol];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (int) $row[$startcol];
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = Jm3ContentPeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = Jm3ContentPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = Jm3ContentPeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                Jm3ContentPeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (Jm3Content object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = Jm3ContentPeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = Jm3ContentPeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + Jm3ContentPeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = Jm3ContentPeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            Jm3ContentPeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(Jm3ContentPeer::DATABASE_NAME)->getTable(Jm3ContentPeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseJm3ContentPeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseJm3ContentPeer::TABLE_NAME)) {
        $dbMap->addTableObject(new Jm3ContentTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass($row = 0, $colnum = 0)
    {
        return Jm3ContentPeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a Jm3Content or Criteria object.
     *
     * @param      mixed $values Criteria or Jm3Content object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(Jm3ContentPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from Jm3Content object
        }

        if ($criteria->containsKey(Jm3ContentPeer::ID) && $criteria->keyContainsValue(Jm3ContentPeer::ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.Jm3ContentPeer::ID.')');
        }


        // Set the correct dbName
        $criteria->setDbName(Jm3ContentPeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a Jm3Content or Criteria object.
     *
     * @param      mixed $values Criteria or Jm3Content object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(Jm3ContentPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(Jm3ContentPeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(Jm3ContentPeer::ID);
            $value = $criteria->remove(Jm3ContentPeer::ID);
            if ($value) {
                $selectCriteria->add(Jm3ContentPeer::ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(Jm3ContentPeer::TABLE_NAME);
            }

        } else { // $values is Jm3Content object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(Jm3ContentPeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the jm3_content table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(Jm3ContentPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(Jm3ContentPeer::TABLE_NAME, $con, Jm3ContentPeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            Jm3ContentPeer::clearInstancePool();
            Jm3ContentPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a Jm3Content or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or Jm3Content object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(Jm3ContentPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            Jm3ContentPeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof Jm3Content) { // it's a model object
            // invalidate the cache for this single object
            Jm3ContentPeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(Jm3ContentPeer::DATABASE_NAME);
            $criteria->add(Jm3ContentPeer::ID, (array) $values, Criteria::IN);
            // invalidate the cache for this object(s)
            foreach ((array) $values as $singleval) {
                Jm3ContentPeer::removeInstanceFromPool($singleval);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(Jm3ContentPeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();

            $affectedRows += BasePeer::doDelete($criteria, $con);
            Jm3ContentPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given Jm3Content object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param Jm3Content $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(Jm3ContentPeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(Jm3ContentPeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(Jm3ContentPeer::DATABASE_NAME, Jm3ContentPeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param int $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return Jm3Content
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = Jm3ContentPeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(Jm3ContentPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(Jm3ContentPeer::DATABASE_NAME);
        $criteria->add(Jm3ContentPeer::ID, $pk);

        $v = Jm3ContentPeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return Jm3Content[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(Jm3ContentPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(Jm3ContentPeer::DATABASE_NAME);
            $criteria->add(Jm3ContentPeer::ID, $pks, Criteria::IN);
            $objs = Jm3ContentPeer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BaseJm3ContentPeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseJm3ContentPeer::buildTableMap();

