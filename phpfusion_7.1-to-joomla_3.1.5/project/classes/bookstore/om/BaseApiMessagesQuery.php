<?php


/**
 * Base class that represents a query for the 'api_messages' table.
 *
 *
 *
 * @method ApiMessagesQuery orderById($order = Criteria::ASC) Order by the id column
 * @method ApiMessagesQuery orderBySenderid($order = Criteria::ASC) Order by the senderId column
 * @method ApiMessagesQuery orderByAddresseeid($order = Criteria::ASC) Order by the addresseeId column
 * @method ApiMessagesQuery orderByDate($order = Criteria::ASC) Order by the date column
 * @method ApiMessagesQuery orderByReceiptdate($order = Criteria::ASC) Order by the receiptDate column
 * @method ApiMessagesQuery orderByContent($order = Criteria::ASC) Order by the content column
 *
 * @method ApiMessagesQuery groupById() Group by the id column
 * @method ApiMessagesQuery groupBySenderid() Group by the senderId column
 * @method ApiMessagesQuery groupByAddresseeid() Group by the addresseeId column
 * @method ApiMessagesQuery groupByDate() Group by the date column
 * @method ApiMessagesQuery groupByReceiptdate() Group by the receiptDate column
 * @method ApiMessagesQuery groupByContent() Group by the content column
 *
 * @method ApiMessagesQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method ApiMessagesQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method ApiMessagesQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method ApiMessages findOne(PropelPDO $con = null) Return the first ApiMessages matching the query
 * @method ApiMessages findOneOrCreate(PropelPDO $con = null) Return the first ApiMessages matching the query, or a new ApiMessages object populated from the query conditions when no match is found
 *
 * @method ApiMessages findOneBySenderid(string $senderId) Return the first ApiMessages filtered by the senderId column
 * @method ApiMessages findOneByAddresseeid(string $addresseeId) Return the first ApiMessages filtered by the addresseeId column
 * @method ApiMessages findOneByDate(string $date) Return the first ApiMessages filtered by the date column
 * @method ApiMessages findOneByReceiptdate(string $receiptDate) Return the first ApiMessages filtered by the receiptDate column
 * @method ApiMessages findOneByContent(string $content) Return the first ApiMessages filtered by the content column
 *
 * @method array findById(string $id) Return ApiMessages objects filtered by the id column
 * @method array findBySenderid(string $senderId) Return ApiMessages objects filtered by the senderId column
 * @method array findByAddresseeid(string $addresseeId) Return ApiMessages objects filtered by the addresseeId column
 * @method array findByDate(string $date) Return ApiMessages objects filtered by the date column
 * @method array findByReceiptdate(string $receiptDate) Return ApiMessages objects filtered by the receiptDate column
 * @method array findByContent(string $content) Return ApiMessages objects filtered by the content column
 *
 * @package    propel.generator.bookstore.om
 */
abstract class BaseApiMessagesQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseApiMessagesQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'bookstore';
        }
        if (null === $modelName) {
            $modelName = 'ApiMessages';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ApiMessagesQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   ApiMessagesQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return ApiMessagesQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof ApiMessagesQuery) {
            return $criteria;
        }
        $query = new ApiMessagesQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   ApiMessages|ApiMessages[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = ApiMessagesPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(ApiMessagesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 ApiMessages A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 ApiMessages A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `senderId`, `addresseeId`, `date`, `receiptDate`, `content` FROM `api_messages` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new ApiMessages();
            $obj->hydrate($row);
            ApiMessagesPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return ApiMessages|ApiMessages[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|ApiMessages[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return ApiMessagesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ApiMessagesPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return ApiMessagesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ApiMessagesPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiMessagesQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ApiMessagesPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ApiMessagesPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiMessagesPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the senderId column
     *
     * Example usage:
     * <code>
     * $query->filterBySenderid(1234); // WHERE senderId = 1234
     * $query->filterBySenderid(array(12, 34)); // WHERE senderId IN (12, 34)
     * $query->filterBySenderid(array('min' => 12)); // WHERE senderId >= 12
     * $query->filterBySenderid(array('max' => 12)); // WHERE senderId <= 12
     * </code>
     *
     * @param     mixed $senderid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiMessagesQuery The current query, for fluid interface
     */
    public function filterBySenderid($senderid = null, $comparison = null)
    {
        if (is_array($senderid)) {
            $useMinMax = false;
            if (isset($senderid['min'])) {
                $this->addUsingAlias(ApiMessagesPeer::SENDERID, $senderid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($senderid['max'])) {
                $this->addUsingAlias(ApiMessagesPeer::SENDERID, $senderid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiMessagesPeer::SENDERID, $senderid, $comparison);
    }

    /**
     * Filter the query on the addresseeId column
     *
     * Example usage:
     * <code>
     * $query->filterByAddresseeid(1234); // WHERE addresseeId = 1234
     * $query->filterByAddresseeid(array(12, 34)); // WHERE addresseeId IN (12, 34)
     * $query->filterByAddresseeid(array('min' => 12)); // WHERE addresseeId >= 12
     * $query->filterByAddresseeid(array('max' => 12)); // WHERE addresseeId <= 12
     * </code>
     *
     * @param     mixed $addresseeid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiMessagesQuery The current query, for fluid interface
     */
    public function filterByAddresseeid($addresseeid = null, $comparison = null)
    {
        if (is_array($addresseeid)) {
            $useMinMax = false;
            if (isset($addresseeid['min'])) {
                $this->addUsingAlias(ApiMessagesPeer::ADDRESSEEID, $addresseeid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($addresseeid['max'])) {
                $this->addUsingAlias(ApiMessagesPeer::ADDRESSEEID, $addresseeid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiMessagesPeer::ADDRESSEEID, $addresseeid, $comparison);
    }

    /**
     * Filter the query on the date column
     *
     * Example usage:
     * <code>
     * $query->filterByDate('2011-03-14'); // WHERE date = '2011-03-14'
     * $query->filterByDate('now'); // WHERE date = '2011-03-14'
     * $query->filterByDate(array('max' => 'yesterday')); // WHERE date < '2011-03-13'
     * </code>
     *
     * @param     mixed $date The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiMessagesQuery The current query, for fluid interface
     */
    public function filterByDate($date = null, $comparison = null)
    {
        if (is_array($date)) {
            $useMinMax = false;
            if (isset($date['min'])) {
                $this->addUsingAlias(ApiMessagesPeer::DATE, $date['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($date['max'])) {
                $this->addUsingAlias(ApiMessagesPeer::DATE, $date['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiMessagesPeer::DATE, $date, $comparison);
    }

    /**
     * Filter the query on the receiptDate column
     *
     * Example usage:
     * <code>
     * $query->filterByReceiptdate('2011-03-14'); // WHERE receiptDate = '2011-03-14'
     * $query->filterByReceiptdate('now'); // WHERE receiptDate = '2011-03-14'
     * $query->filterByReceiptdate(array('max' => 'yesterday')); // WHERE receiptDate < '2011-03-13'
     * </code>
     *
     * @param     mixed $receiptdate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiMessagesQuery The current query, for fluid interface
     */
    public function filterByReceiptdate($receiptdate = null, $comparison = null)
    {
        if (is_array($receiptdate)) {
            $useMinMax = false;
            if (isset($receiptdate['min'])) {
                $this->addUsingAlias(ApiMessagesPeer::RECEIPTDATE, $receiptdate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($receiptdate['max'])) {
                $this->addUsingAlias(ApiMessagesPeer::RECEIPTDATE, $receiptdate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiMessagesPeer::RECEIPTDATE, $receiptdate, $comparison);
    }

    /**
     * Filter the query on the content column
     *
     * Example usage:
     * <code>
     * $query->filterByContent('fooValue');   // WHERE content = 'fooValue'
     * $query->filterByContent('%fooValue%'); // WHERE content LIKE '%fooValue%'
     * </code>
     *
     * @param     string $content The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiMessagesQuery The current query, for fluid interface
     */
    public function filterByContent($content = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($content)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $content)) {
                $content = str_replace('*', '%', $content);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ApiMessagesPeer::CONTENT, $content, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ApiMessages $apiMessages Object to remove from the list of results
     *
     * @return ApiMessagesQuery The current query, for fluid interface
     */
    public function prune($apiMessages = null)
    {
        if ($apiMessages) {
            $this->addUsingAlias(ApiMessagesPeer::ID, $apiMessages->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
