<?php


/**
 * Base class that represents a query for the 'api_buildings' table.
 *
 *
 *
 * @method ApiBuildingsQuery orderById($order = Criteria::ASC) Order by the id column
 * @method ApiBuildingsQuery orderByLevel($order = Criteria::ASC) Order by the level column
 * @method ApiBuildingsQuery orderByPositionx($order = Criteria::ASC) Order by the positionX column
 * @method ApiBuildingsQuery orderByPositiony($order = Criteria::ASC) Order by the positionY column
 * @method ApiBuildingsQuery orderByHealth($order = Criteria::ASC) Order by the health column
 * @method ApiBuildingsQuery orderByBuildingBuildingtype($order = Criteria::ASC) Order by the building_buildingType column
 * @method ApiBuildingsQuery orderByBuildingPlot($order = Criteria::ASC) Order by the building_plot column
 *
 * @method ApiBuildingsQuery groupById() Group by the id column
 * @method ApiBuildingsQuery groupByLevel() Group by the level column
 * @method ApiBuildingsQuery groupByPositionx() Group by the positionX column
 * @method ApiBuildingsQuery groupByPositiony() Group by the positionY column
 * @method ApiBuildingsQuery groupByHealth() Group by the health column
 * @method ApiBuildingsQuery groupByBuildingBuildingtype() Group by the building_buildingType column
 * @method ApiBuildingsQuery groupByBuildingPlot() Group by the building_plot column
 *
 * @method ApiBuildingsQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method ApiBuildingsQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method ApiBuildingsQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method ApiBuildings findOne(PropelPDO $con = null) Return the first ApiBuildings matching the query
 * @method ApiBuildings findOneOrCreate(PropelPDO $con = null) Return the first ApiBuildings matching the query, or a new ApiBuildings object populated from the query conditions when no match is found
 *
 * @method ApiBuildings findOneByLevel(int $level) Return the first ApiBuildings filtered by the level column
 * @method ApiBuildings findOneByPositionx(int $positionX) Return the first ApiBuildings filtered by the positionX column
 * @method ApiBuildings findOneByPositiony(int $positionY) Return the first ApiBuildings filtered by the positionY column
 * @method ApiBuildings findOneByHealth(int $health) Return the first ApiBuildings filtered by the health column
 * @method ApiBuildings findOneByBuildingBuildingtype(int $building_buildingType) Return the first ApiBuildings filtered by the building_buildingType column
 * @method ApiBuildings findOneByBuildingPlot(string $building_plot) Return the first ApiBuildings filtered by the building_plot column
 *
 * @method array findById(string $id) Return ApiBuildings objects filtered by the id column
 * @method array findByLevel(int $level) Return ApiBuildings objects filtered by the level column
 * @method array findByPositionx(int $positionX) Return ApiBuildings objects filtered by the positionX column
 * @method array findByPositiony(int $positionY) Return ApiBuildings objects filtered by the positionY column
 * @method array findByHealth(int $health) Return ApiBuildings objects filtered by the health column
 * @method array findByBuildingBuildingtype(int $building_buildingType) Return ApiBuildings objects filtered by the building_buildingType column
 * @method array findByBuildingPlot(string $building_plot) Return ApiBuildings objects filtered by the building_plot column
 *
 * @package    propel.generator.bookstore.om
 */
abstract class BaseApiBuildingsQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseApiBuildingsQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'bookstore';
        }
        if (null === $modelName) {
            $modelName = 'ApiBuildings';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ApiBuildingsQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   ApiBuildingsQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return ApiBuildingsQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof ApiBuildingsQuery) {
            return $criteria;
        }
        $query = new ApiBuildingsQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   ApiBuildings|ApiBuildings[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = ApiBuildingsPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(ApiBuildingsPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 ApiBuildings A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 ApiBuildings A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `level`, `positionX`, `positionY`, `health`, `building_buildingType`, `building_plot` FROM `api_buildings` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new ApiBuildings();
            $obj->hydrate($row);
            ApiBuildingsPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return ApiBuildings|ApiBuildings[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|ApiBuildings[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return ApiBuildingsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ApiBuildingsPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return ApiBuildingsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ApiBuildingsPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiBuildingsQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ApiBuildingsPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ApiBuildingsPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiBuildingsPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the level column
     *
     * Example usage:
     * <code>
     * $query->filterByLevel(1234); // WHERE level = 1234
     * $query->filterByLevel(array(12, 34)); // WHERE level IN (12, 34)
     * $query->filterByLevel(array('min' => 12)); // WHERE level >= 12
     * $query->filterByLevel(array('max' => 12)); // WHERE level <= 12
     * </code>
     *
     * @param     mixed $level The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiBuildingsQuery The current query, for fluid interface
     */
    public function filterByLevel($level = null, $comparison = null)
    {
        if (is_array($level)) {
            $useMinMax = false;
            if (isset($level['min'])) {
                $this->addUsingAlias(ApiBuildingsPeer::LEVEL, $level['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($level['max'])) {
                $this->addUsingAlias(ApiBuildingsPeer::LEVEL, $level['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiBuildingsPeer::LEVEL, $level, $comparison);
    }

    /**
     * Filter the query on the positionX column
     *
     * Example usage:
     * <code>
     * $query->filterByPositionx(1234); // WHERE positionX = 1234
     * $query->filterByPositionx(array(12, 34)); // WHERE positionX IN (12, 34)
     * $query->filterByPositionx(array('min' => 12)); // WHERE positionX >= 12
     * $query->filterByPositionx(array('max' => 12)); // WHERE positionX <= 12
     * </code>
     *
     * @param     mixed $positionx The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiBuildingsQuery The current query, for fluid interface
     */
    public function filterByPositionx($positionx = null, $comparison = null)
    {
        if (is_array($positionx)) {
            $useMinMax = false;
            if (isset($positionx['min'])) {
                $this->addUsingAlias(ApiBuildingsPeer::POSITIONX, $positionx['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($positionx['max'])) {
                $this->addUsingAlias(ApiBuildingsPeer::POSITIONX, $positionx['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiBuildingsPeer::POSITIONX, $positionx, $comparison);
    }

    /**
     * Filter the query on the positionY column
     *
     * Example usage:
     * <code>
     * $query->filterByPositiony(1234); // WHERE positionY = 1234
     * $query->filterByPositiony(array(12, 34)); // WHERE positionY IN (12, 34)
     * $query->filterByPositiony(array('min' => 12)); // WHERE positionY >= 12
     * $query->filterByPositiony(array('max' => 12)); // WHERE positionY <= 12
     * </code>
     *
     * @param     mixed $positiony The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiBuildingsQuery The current query, for fluid interface
     */
    public function filterByPositiony($positiony = null, $comparison = null)
    {
        if (is_array($positiony)) {
            $useMinMax = false;
            if (isset($positiony['min'])) {
                $this->addUsingAlias(ApiBuildingsPeer::POSITIONY, $positiony['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($positiony['max'])) {
                $this->addUsingAlias(ApiBuildingsPeer::POSITIONY, $positiony['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiBuildingsPeer::POSITIONY, $positiony, $comparison);
    }

    /**
     * Filter the query on the health column
     *
     * Example usage:
     * <code>
     * $query->filterByHealth(1234); // WHERE health = 1234
     * $query->filterByHealth(array(12, 34)); // WHERE health IN (12, 34)
     * $query->filterByHealth(array('min' => 12)); // WHERE health >= 12
     * $query->filterByHealth(array('max' => 12)); // WHERE health <= 12
     * </code>
     *
     * @param     mixed $health The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiBuildingsQuery The current query, for fluid interface
     */
    public function filterByHealth($health = null, $comparison = null)
    {
        if (is_array($health)) {
            $useMinMax = false;
            if (isset($health['min'])) {
                $this->addUsingAlias(ApiBuildingsPeer::HEALTH, $health['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($health['max'])) {
                $this->addUsingAlias(ApiBuildingsPeer::HEALTH, $health['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiBuildingsPeer::HEALTH, $health, $comparison);
    }

    /**
     * Filter the query on the building_buildingType column
     *
     * Example usage:
     * <code>
     * $query->filterByBuildingBuildingtype(1234); // WHERE building_buildingType = 1234
     * $query->filterByBuildingBuildingtype(array(12, 34)); // WHERE building_buildingType IN (12, 34)
     * $query->filterByBuildingBuildingtype(array('min' => 12)); // WHERE building_buildingType >= 12
     * $query->filterByBuildingBuildingtype(array('max' => 12)); // WHERE building_buildingType <= 12
     * </code>
     *
     * @param     mixed $buildingBuildingtype The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiBuildingsQuery The current query, for fluid interface
     */
    public function filterByBuildingBuildingtype($buildingBuildingtype = null, $comparison = null)
    {
        if (is_array($buildingBuildingtype)) {
            $useMinMax = false;
            if (isset($buildingBuildingtype['min'])) {
                $this->addUsingAlias(ApiBuildingsPeer::BUILDING_BUILDINGTYPE, $buildingBuildingtype['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($buildingBuildingtype['max'])) {
                $this->addUsingAlias(ApiBuildingsPeer::BUILDING_BUILDINGTYPE, $buildingBuildingtype['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiBuildingsPeer::BUILDING_BUILDINGTYPE, $buildingBuildingtype, $comparison);
    }

    /**
     * Filter the query on the building_plot column
     *
     * Example usage:
     * <code>
     * $query->filterByBuildingPlot(1234); // WHERE building_plot = 1234
     * $query->filterByBuildingPlot(array(12, 34)); // WHERE building_plot IN (12, 34)
     * $query->filterByBuildingPlot(array('min' => 12)); // WHERE building_plot >= 12
     * $query->filterByBuildingPlot(array('max' => 12)); // WHERE building_plot <= 12
     * </code>
     *
     * @param     mixed $buildingPlot The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiBuildingsQuery The current query, for fluid interface
     */
    public function filterByBuildingPlot($buildingPlot = null, $comparison = null)
    {
        if (is_array($buildingPlot)) {
            $useMinMax = false;
            if (isset($buildingPlot['min'])) {
                $this->addUsingAlias(ApiBuildingsPeer::BUILDING_PLOT, $buildingPlot['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($buildingPlot['max'])) {
                $this->addUsingAlias(ApiBuildingsPeer::BUILDING_PLOT, $buildingPlot['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiBuildingsPeer::BUILDING_PLOT, $buildingPlot, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ApiBuildings $apiBuildings Object to remove from the list of results
     *
     * @return ApiBuildingsQuery The current query, for fluid interface
     */
    public function prune($apiBuildings = null)
    {
        if ($apiBuildings) {
            $this->addUsingAlias(ApiBuildingsPeer::ID, $apiBuildings->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
