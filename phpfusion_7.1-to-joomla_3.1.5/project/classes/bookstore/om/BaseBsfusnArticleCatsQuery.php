<?php


/**
 * Base class that represents a query for the 'bsfusn_article_cats' table.
 *
 *
 *
 * @method BsfusnArticleCatsQuery orderByArticleCatId($order = Criteria::ASC) Order by the article_cat_id column
 * @method BsfusnArticleCatsQuery orderByArticleCatName($order = Criteria::ASC) Order by the article_cat_name column
 * @method BsfusnArticleCatsQuery orderByArticleCatDescription($order = Criteria::ASC) Order by the article_cat_description column
 * @method BsfusnArticleCatsQuery orderByArticleCatSorting($order = Criteria::ASC) Order by the article_cat_sorting column
 * @method BsfusnArticleCatsQuery orderByArticleCatAccess($order = Criteria::ASC) Order by the article_cat_access column
 *
 * @method BsfusnArticleCatsQuery groupByArticleCatId() Group by the article_cat_id column
 * @method BsfusnArticleCatsQuery groupByArticleCatName() Group by the article_cat_name column
 * @method BsfusnArticleCatsQuery groupByArticleCatDescription() Group by the article_cat_description column
 * @method BsfusnArticleCatsQuery groupByArticleCatSorting() Group by the article_cat_sorting column
 * @method BsfusnArticleCatsQuery groupByArticleCatAccess() Group by the article_cat_access column
 *
 * @method BsfusnArticleCatsQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method BsfusnArticleCatsQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method BsfusnArticleCatsQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method BsfusnArticleCats findOne(PropelPDO $con = null) Return the first BsfusnArticleCats matching the query
 * @method BsfusnArticleCats findOneOrCreate(PropelPDO $con = null) Return the first BsfusnArticleCats matching the query, or a new BsfusnArticleCats object populated from the query conditions when no match is found
 *
 * @method BsfusnArticleCats findOneByArticleCatName(string $article_cat_name) Return the first BsfusnArticleCats filtered by the article_cat_name column
 * @method BsfusnArticleCats findOneByArticleCatDescription(string $article_cat_description) Return the first BsfusnArticleCats filtered by the article_cat_description column
 * @method BsfusnArticleCats findOneByArticleCatSorting(string $article_cat_sorting) Return the first BsfusnArticleCats filtered by the article_cat_sorting column
 * @method BsfusnArticleCats findOneByArticleCatAccess(int $article_cat_access) Return the first BsfusnArticleCats filtered by the article_cat_access column
 *
 * @method array findByArticleCatId(int $article_cat_id) Return BsfusnArticleCats objects filtered by the article_cat_id column
 * @method array findByArticleCatName(string $article_cat_name) Return BsfusnArticleCats objects filtered by the article_cat_name column
 * @method array findByArticleCatDescription(string $article_cat_description) Return BsfusnArticleCats objects filtered by the article_cat_description column
 * @method array findByArticleCatSorting(string $article_cat_sorting) Return BsfusnArticleCats objects filtered by the article_cat_sorting column
 * @method array findByArticleCatAccess(int $article_cat_access) Return BsfusnArticleCats objects filtered by the article_cat_access column
 *
 * @package    propel.generator.bookstore.om
 */
abstract class BaseBsfusnArticleCatsQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseBsfusnArticleCatsQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'bookstore';
        }
        if (null === $modelName) {
            $modelName = 'BsfusnArticleCats';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new BsfusnArticleCatsQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   BsfusnArticleCatsQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return BsfusnArticleCatsQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof BsfusnArticleCatsQuery) {
            return $criteria;
        }
        $query = new BsfusnArticleCatsQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   BsfusnArticleCats|BsfusnArticleCats[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = BsfusnArticleCatsPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(BsfusnArticleCatsPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 BsfusnArticleCats A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByArticleCatId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 BsfusnArticleCats A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `article_cat_id`, `article_cat_name`, `article_cat_description`, `article_cat_sorting`, `article_cat_access` FROM `bsfusn_article_cats` WHERE `article_cat_id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new BsfusnArticleCats();
            $obj->hydrate($row);
            BsfusnArticleCatsPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return BsfusnArticleCats|BsfusnArticleCats[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|BsfusnArticleCats[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return BsfusnArticleCatsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(BsfusnArticleCatsPeer::ARTICLE_CAT_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return BsfusnArticleCatsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(BsfusnArticleCatsPeer::ARTICLE_CAT_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the article_cat_id column
     *
     * Example usage:
     * <code>
     * $query->filterByArticleCatId(1234); // WHERE article_cat_id = 1234
     * $query->filterByArticleCatId(array(12, 34)); // WHERE article_cat_id IN (12, 34)
     * $query->filterByArticleCatId(array('min' => 12)); // WHERE article_cat_id >= 12
     * $query->filterByArticleCatId(array('max' => 12)); // WHERE article_cat_id <= 12
     * </code>
     *
     * @param     mixed $articleCatId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BsfusnArticleCatsQuery The current query, for fluid interface
     */
    public function filterByArticleCatId($articleCatId = null, $comparison = null)
    {
        if (is_array($articleCatId)) {
            $useMinMax = false;
            if (isset($articleCatId['min'])) {
                $this->addUsingAlias(BsfusnArticleCatsPeer::ARTICLE_CAT_ID, $articleCatId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($articleCatId['max'])) {
                $this->addUsingAlias(BsfusnArticleCatsPeer::ARTICLE_CAT_ID, $articleCatId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BsfusnArticleCatsPeer::ARTICLE_CAT_ID, $articleCatId, $comparison);
    }

    /**
     * Filter the query on the article_cat_name column
     *
     * Example usage:
     * <code>
     * $query->filterByArticleCatName('fooValue');   // WHERE article_cat_name = 'fooValue'
     * $query->filterByArticleCatName('%fooValue%'); // WHERE article_cat_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $articleCatName The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BsfusnArticleCatsQuery The current query, for fluid interface
     */
    public function filterByArticleCatName($articleCatName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($articleCatName)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $articleCatName)) {
                $articleCatName = str_replace('*', '%', $articleCatName);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(BsfusnArticleCatsPeer::ARTICLE_CAT_NAME, $articleCatName, $comparison);
    }

    /**
     * Filter the query on the article_cat_description column
     *
     * Example usage:
     * <code>
     * $query->filterByArticleCatDescription('fooValue');   // WHERE article_cat_description = 'fooValue'
     * $query->filterByArticleCatDescription('%fooValue%'); // WHERE article_cat_description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $articleCatDescription The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BsfusnArticleCatsQuery The current query, for fluid interface
     */
    public function filterByArticleCatDescription($articleCatDescription = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($articleCatDescription)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $articleCatDescription)) {
                $articleCatDescription = str_replace('*', '%', $articleCatDescription);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(BsfusnArticleCatsPeer::ARTICLE_CAT_DESCRIPTION, $articleCatDescription, $comparison);
    }

    /**
     * Filter the query on the article_cat_sorting column
     *
     * Example usage:
     * <code>
     * $query->filterByArticleCatSorting('fooValue');   // WHERE article_cat_sorting = 'fooValue'
     * $query->filterByArticleCatSorting('%fooValue%'); // WHERE article_cat_sorting LIKE '%fooValue%'
     * </code>
     *
     * @param     string $articleCatSorting The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BsfusnArticleCatsQuery The current query, for fluid interface
     */
    public function filterByArticleCatSorting($articleCatSorting = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($articleCatSorting)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $articleCatSorting)) {
                $articleCatSorting = str_replace('*', '%', $articleCatSorting);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(BsfusnArticleCatsPeer::ARTICLE_CAT_SORTING, $articleCatSorting, $comparison);
    }

    /**
     * Filter the query on the article_cat_access column
     *
     * Example usage:
     * <code>
     * $query->filterByArticleCatAccess(1234); // WHERE article_cat_access = 1234
     * $query->filterByArticleCatAccess(array(12, 34)); // WHERE article_cat_access IN (12, 34)
     * $query->filterByArticleCatAccess(array('min' => 12)); // WHERE article_cat_access >= 12
     * $query->filterByArticleCatAccess(array('max' => 12)); // WHERE article_cat_access <= 12
     * </code>
     *
     * @param     mixed $articleCatAccess The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BsfusnArticleCatsQuery The current query, for fluid interface
     */
    public function filterByArticleCatAccess($articleCatAccess = null, $comparison = null)
    {
        if (is_array($articleCatAccess)) {
            $useMinMax = false;
            if (isset($articleCatAccess['min'])) {
                $this->addUsingAlias(BsfusnArticleCatsPeer::ARTICLE_CAT_ACCESS, $articleCatAccess['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($articleCatAccess['max'])) {
                $this->addUsingAlias(BsfusnArticleCatsPeer::ARTICLE_CAT_ACCESS, $articleCatAccess['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BsfusnArticleCatsPeer::ARTICLE_CAT_ACCESS, $articleCatAccess, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   BsfusnArticleCats $bsfusnArticleCats Object to remove from the list of results
     *
     * @return BsfusnArticleCatsQuery The current query, for fluid interface
     */
    public function prune($bsfusnArticleCats = null)
    {
        if ($bsfusnArticleCats) {
            $this->addUsingAlias(BsfusnArticleCatsPeer::ARTICLE_CAT_ID, $bsfusnArticleCats->getArticleCatId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
