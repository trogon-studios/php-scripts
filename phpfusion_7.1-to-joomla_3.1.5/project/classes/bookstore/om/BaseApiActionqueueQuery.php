<?php


/**
 * Base class that represents a query for the 'api_actionQueue' table.
 *
 *
 *
 * @method ApiActionqueueQuery orderById($order = Criteria::ASC) Order by the id column
 * @method ApiActionqueueQuery orderByActionqueuePlayer($order = Criteria::ASC) Order by the actionQueue_player column
 *
 * @method ApiActionqueueQuery groupById() Group by the id column
 * @method ApiActionqueueQuery groupByActionqueuePlayer() Group by the actionQueue_player column
 *
 * @method ApiActionqueueQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method ApiActionqueueQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method ApiActionqueueQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method ApiActionqueue findOne(PropelPDO $con = null) Return the first ApiActionqueue matching the query
 * @method ApiActionqueue findOneOrCreate(PropelPDO $con = null) Return the first ApiActionqueue matching the query, or a new ApiActionqueue object populated from the query conditions when no match is found
 *
 * @method ApiActionqueue findOneByActionqueuePlayer(string $actionQueue_player) Return the first ApiActionqueue filtered by the actionQueue_player column
 *
 * @method array findById(string $id) Return ApiActionqueue objects filtered by the id column
 * @method array findByActionqueuePlayer(string $actionQueue_player) Return ApiActionqueue objects filtered by the actionQueue_player column
 *
 * @package    propel.generator.bookstore.om
 */
abstract class BaseApiActionqueueQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseApiActionqueueQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'bookstore';
        }
        if (null === $modelName) {
            $modelName = 'ApiActionqueue';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ApiActionqueueQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   ApiActionqueueQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return ApiActionqueueQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof ApiActionqueueQuery) {
            return $criteria;
        }
        $query = new ApiActionqueueQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   ApiActionqueue|ApiActionqueue[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = ApiActionqueuePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(ApiActionqueuePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 ApiActionqueue A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 ApiActionqueue A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `actionQueue_player` FROM `api_actionQueue` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new ApiActionqueue();
            $obj->hydrate($row);
            ApiActionqueuePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return ApiActionqueue|ApiActionqueue[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|ApiActionqueue[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return ApiActionqueueQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ApiActionqueuePeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return ApiActionqueueQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ApiActionqueuePeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiActionqueueQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ApiActionqueuePeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ApiActionqueuePeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiActionqueuePeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the actionQueue_player column
     *
     * Example usage:
     * <code>
     * $query->filterByActionqueuePlayer(1234); // WHERE actionQueue_player = 1234
     * $query->filterByActionqueuePlayer(array(12, 34)); // WHERE actionQueue_player IN (12, 34)
     * $query->filterByActionqueuePlayer(array('min' => 12)); // WHERE actionQueue_player >= 12
     * $query->filterByActionqueuePlayer(array('max' => 12)); // WHERE actionQueue_player <= 12
     * </code>
     *
     * @param     mixed $actionqueuePlayer The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiActionqueueQuery The current query, for fluid interface
     */
    public function filterByActionqueuePlayer($actionqueuePlayer = null, $comparison = null)
    {
        if (is_array($actionqueuePlayer)) {
            $useMinMax = false;
            if (isset($actionqueuePlayer['min'])) {
                $this->addUsingAlias(ApiActionqueuePeer::ACTIONQUEUE_PLAYER, $actionqueuePlayer['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($actionqueuePlayer['max'])) {
                $this->addUsingAlias(ApiActionqueuePeer::ACTIONQUEUE_PLAYER, $actionqueuePlayer['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiActionqueuePeer::ACTIONQUEUE_PLAYER, $actionqueuePlayer, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ApiActionqueue $apiActionqueue Object to remove from the list of results
     *
     * @return ApiActionqueueQuery The current query, for fluid interface
     */
    public function prune($apiActionqueue = null)
    {
        if ($apiActionqueue) {
            $this->addUsingAlias(ApiActionqueuePeer::ID, $apiActionqueue->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
