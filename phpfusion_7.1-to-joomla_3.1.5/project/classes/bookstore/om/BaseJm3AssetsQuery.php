<?php


/**
 * Base class that represents a query for the 'jm3_assets' table.
 *
 *
 *
 * @method Jm3AssetsQuery orderById($order = Criteria::ASC) Order by the id column
 * @method Jm3AssetsQuery orderByParentId($order = Criteria::ASC) Order by the parent_id column
 * @method Jm3AssetsQuery orderByLft($order = Criteria::ASC) Order by the lft column
 * @method Jm3AssetsQuery orderByRgt($order = Criteria::ASC) Order by the rgt column
 * @method Jm3AssetsQuery orderByLevel($order = Criteria::ASC) Order by the level column
 * @method Jm3AssetsQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method Jm3AssetsQuery orderByTitle($order = Criteria::ASC) Order by the title column
 * @method Jm3AssetsQuery orderByRules($order = Criteria::ASC) Order by the rules column
 *
 * @method Jm3AssetsQuery groupById() Group by the id column
 * @method Jm3AssetsQuery groupByParentId() Group by the parent_id column
 * @method Jm3AssetsQuery groupByLft() Group by the lft column
 * @method Jm3AssetsQuery groupByRgt() Group by the rgt column
 * @method Jm3AssetsQuery groupByLevel() Group by the level column
 * @method Jm3AssetsQuery groupByName() Group by the name column
 * @method Jm3AssetsQuery groupByTitle() Group by the title column
 * @method Jm3AssetsQuery groupByRules() Group by the rules column
 *
 * @method Jm3AssetsQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method Jm3AssetsQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method Jm3AssetsQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method Jm3Assets findOne(PropelPDO $con = null) Return the first Jm3Assets matching the query
 * @method Jm3Assets findOneOrCreate(PropelPDO $con = null) Return the first Jm3Assets matching the query, or a new Jm3Assets object populated from the query conditions when no match is found
 *
 * @method Jm3Assets findOneByParentId(int $parent_id) Return the first Jm3Assets filtered by the parent_id column
 * @method Jm3Assets findOneByLft(int $lft) Return the first Jm3Assets filtered by the lft column
 * @method Jm3Assets findOneByRgt(int $rgt) Return the first Jm3Assets filtered by the rgt column
 * @method Jm3Assets findOneByLevel(int $level) Return the first Jm3Assets filtered by the level column
 * @method Jm3Assets findOneByName(string $name) Return the first Jm3Assets filtered by the name column
 * @method Jm3Assets findOneByTitle(string $title) Return the first Jm3Assets filtered by the title column
 * @method Jm3Assets findOneByRules(string $rules) Return the first Jm3Assets filtered by the rules column
 *
 * @method array findById(int $id) Return Jm3Assets objects filtered by the id column
 * @method array findByParentId(int $parent_id) Return Jm3Assets objects filtered by the parent_id column
 * @method array findByLft(int $lft) Return Jm3Assets objects filtered by the lft column
 * @method array findByRgt(int $rgt) Return Jm3Assets objects filtered by the rgt column
 * @method array findByLevel(int $level) Return Jm3Assets objects filtered by the level column
 * @method array findByName(string $name) Return Jm3Assets objects filtered by the name column
 * @method array findByTitle(string $title) Return Jm3Assets objects filtered by the title column
 * @method array findByRules(string $rules) Return Jm3Assets objects filtered by the rules column
 *
 * @package    propel.generator.bookstore.om
 */
abstract class BaseJm3AssetsQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseJm3AssetsQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'bookstore';
        }
        if (null === $modelName) {
            $modelName = 'Jm3Assets';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new Jm3AssetsQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   Jm3AssetsQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return Jm3AssetsQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof Jm3AssetsQuery) {
            return $criteria;
        }
        $query = new Jm3AssetsQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Jm3Assets|Jm3Assets[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = Jm3AssetsPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(Jm3AssetsPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Jm3Assets A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Jm3Assets A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `parent_id`, `lft`, `rgt`, `level`, `name`, `title`, `rules` FROM `jm3_assets` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Jm3Assets();
            $obj->hydrate($row);
            Jm3AssetsPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Jm3Assets|Jm3Assets[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Jm3Assets[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return Jm3AssetsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(Jm3AssetsPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return Jm3AssetsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(Jm3AssetsPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3AssetsQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(Jm3AssetsPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(Jm3AssetsPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Jm3AssetsPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the parent_id column
     *
     * Example usage:
     * <code>
     * $query->filterByParentId(1234); // WHERE parent_id = 1234
     * $query->filterByParentId(array(12, 34)); // WHERE parent_id IN (12, 34)
     * $query->filterByParentId(array('min' => 12)); // WHERE parent_id >= 12
     * $query->filterByParentId(array('max' => 12)); // WHERE parent_id <= 12
     * </code>
     *
     * @param     mixed $parentId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3AssetsQuery The current query, for fluid interface
     */
    public function filterByParentId($parentId = null, $comparison = null)
    {
        if (is_array($parentId)) {
            $useMinMax = false;
            if (isset($parentId['min'])) {
                $this->addUsingAlias(Jm3AssetsPeer::PARENT_ID, $parentId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($parentId['max'])) {
                $this->addUsingAlias(Jm3AssetsPeer::PARENT_ID, $parentId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Jm3AssetsPeer::PARENT_ID, $parentId, $comparison);
    }

    /**
     * Filter the query on the lft column
     *
     * Example usage:
     * <code>
     * $query->filterByLft(1234); // WHERE lft = 1234
     * $query->filterByLft(array(12, 34)); // WHERE lft IN (12, 34)
     * $query->filterByLft(array('min' => 12)); // WHERE lft >= 12
     * $query->filterByLft(array('max' => 12)); // WHERE lft <= 12
     * </code>
     *
     * @param     mixed $lft The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3AssetsQuery The current query, for fluid interface
     */
    public function filterByLft($lft = null, $comparison = null)
    {
        if (is_array($lft)) {
            $useMinMax = false;
            if (isset($lft['min'])) {
                $this->addUsingAlias(Jm3AssetsPeer::LFT, $lft['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lft['max'])) {
                $this->addUsingAlias(Jm3AssetsPeer::LFT, $lft['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Jm3AssetsPeer::LFT, $lft, $comparison);
    }

    /**
     * Filter the query on the rgt column
     *
     * Example usage:
     * <code>
     * $query->filterByRgt(1234); // WHERE rgt = 1234
     * $query->filterByRgt(array(12, 34)); // WHERE rgt IN (12, 34)
     * $query->filterByRgt(array('min' => 12)); // WHERE rgt >= 12
     * $query->filterByRgt(array('max' => 12)); // WHERE rgt <= 12
     * </code>
     *
     * @param     mixed $rgt The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3AssetsQuery The current query, for fluid interface
     */
    public function filterByRgt($rgt = null, $comparison = null)
    {
        if (is_array($rgt)) {
            $useMinMax = false;
            if (isset($rgt['min'])) {
                $this->addUsingAlias(Jm3AssetsPeer::RGT, $rgt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rgt['max'])) {
                $this->addUsingAlias(Jm3AssetsPeer::RGT, $rgt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Jm3AssetsPeer::RGT, $rgt, $comparison);
    }

    /**
     * Filter the query on the level column
     *
     * Example usage:
     * <code>
     * $query->filterByLevel(1234); // WHERE level = 1234
     * $query->filterByLevel(array(12, 34)); // WHERE level IN (12, 34)
     * $query->filterByLevel(array('min' => 12)); // WHERE level >= 12
     * $query->filterByLevel(array('max' => 12)); // WHERE level <= 12
     * </code>
     *
     * @param     mixed $level The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3AssetsQuery The current query, for fluid interface
     */
    public function filterByLevel($level = null, $comparison = null)
    {
        if (is_array($level)) {
            $useMinMax = false;
            if (isset($level['min'])) {
                $this->addUsingAlias(Jm3AssetsPeer::LEVEL, $level['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($level['max'])) {
                $this->addUsingAlias(Jm3AssetsPeer::LEVEL, $level['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Jm3AssetsPeer::LEVEL, $level, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3AssetsQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $name)) {
                $name = str_replace('*', '%', $name);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(Jm3AssetsPeer::NAME, $name, $comparison);
    }

    /**
     * Filter the query on the title column
     *
     * Example usage:
     * <code>
     * $query->filterByTitle('fooValue');   // WHERE title = 'fooValue'
     * $query->filterByTitle('%fooValue%'); // WHERE title LIKE '%fooValue%'
     * </code>
     *
     * @param     string $title The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3AssetsQuery The current query, for fluid interface
     */
    public function filterByTitle($title = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($title)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $title)) {
                $title = str_replace('*', '%', $title);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(Jm3AssetsPeer::TITLE, $title, $comparison);
    }

    /**
     * Filter the query on the rules column
     *
     * Example usage:
     * <code>
     * $query->filterByRules('fooValue');   // WHERE rules = 'fooValue'
     * $query->filterByRules('%fooValue%'); // WHERE rules LIKE '%fooValue%'
     * </code>
     *
     * @param     string $rules The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return Jm3AssetsQuery The current query, for fluid interface
     */
    public function filterByRules($rules = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($rules)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $rules)) {
                $rules = str_replace('*', '%', $rules);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(Jm3AssetsPeer::RULES, $rules, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   Jm3Assets $jm3Assets Object to remove from the list of results
     *
     * @return Jm3AssetsQuery The current query, for fluid interface
     */
    public function prune($jm3Assets = null)
    {
        if ($jm3Assets) {
            $this->addUsingAlias(Jm3AssetsPeer::ID, $jm3Assets->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
