<?php


/**
 * Base class that represents a query for the 'api_daemon' table.
 *
 *
 *
 * @method ApiDaemonQuery orderById($order = Criteria::ASC) Order by the id column
 * @method ApiDaemonQuery orderByStamp($order = Criteria::ASC) Order by the stamp column
 * @method ApiDaemonQuery orderByLastupdate($order = Criteria::ASC) Order by the lastUpdate column
 *
 * @method ApiDaemonQuery groupById() Group by the id column
 * @method ApiDaemonQuery groupByStamp() Group by the stamp column
 * @method ApiDaemonQuery groupByLastupdate() Group by the lastUpdate column
 *
 * @method ApiDaemonQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method ApiDaemonQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method ApiDaemonQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method ApiDaemon findOne(PropelPDO $con = null) Return the first ApiDaemon matching the query
 * @method ApiDaemon findOneOrCreate(PropelPDO $con = null) Return the first ApiDaemon matching the query, or a new ApiDaemon object populated from the query conditions when no match is found
 *
 * @method ApiDaemon findOneByStamp(string $stamp) Return the first ApiDaemon filtered by the stamp column
 * @method ApiDaemon findOneByLastupdate(string $lastUpdate) Return the first ApiDaemon filtered by the lastUpdate column
 *
 * @method array findById(int $id) Return ApiDaemon objects filtered by the id column
 * @method array findByStamp(string $stamp) Return ApiDaemon objects filtered by the stamp column
 * @method array findByLastupdate(string $lastUpdate) Return ApiDaemon objects filtered by the lastUpdate column
 *
 * @package    propel.generator.bookstore.om
 */
abstract class BaseApiDaemonQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseApiDaemonQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'bookstore';
        }
        if (null === $modelName) {
            $modelName = 'ApiDaemon';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ApiDaemonQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   ApiDaemonQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return ApiDaemonQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof ApiDaemonQuery) {
            return $criteria;
        }
        $query = new ApiDaemonQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   ApiDaemon|ApiDaemon[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = ApiDaemonPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(ApiDaemonPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 ApiDaemon A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 ApiDaemon A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `stamp`, `lastUpdate` FROM `api_daemon` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new ApiDaemon();
            $obj->hydrate($row);
            ApiDaemonPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return ApiDaemon|ApiDaemon[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|ApiDaemon[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return ApiDaemonQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ApiDaemonPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return ApiDaemonQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ApiDaemonPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiDaemonQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ApiDaemonPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ApiDaemonPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiDaemonPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the stamp column
     *
     * Example usage:
     * <code>
     * $query->filterByStamp('fooValue');   // WHERE stamp = 'fooValue'
     * $query->filterByStamp('%fooValue%'); // WHERE stamp LIKE '%fooValue%'
     * </code>
     *
     * @param     string $stamp The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiDaemonQuery The current query, for fluid interface
     */
    public function filterByStamp($stamp = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($stamp)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $stamp)) {
                $stamp = str_replace('*', '%', $stamp);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ApiDaemonPeer::STAMP, $stamp, $comparison);
    }

    /**
     * Filter the query on the lastUpdate column
     *
     * Example usage:
     * <code>
     * $query->filterByLastupdate('2011-03-14'); // WHERE lastUpdate = '2011-03-14'
     * $query->filterByLastupdate('now'); // WHERE lastUpdate = '2011-03-14'
     * $query->filterByLastupdate(array('max' => 'yesterday')); // WHERE lastUpdate < '2011-03-13'
     * </code>
     *
     * @param     mixed $lastupdate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiDaemonQuery The current query, for fluid interface
     */
    public function filterByLastupdate($lastupdate = null, $comparison = null)
    {
        if (is_array($lastupdate)) {
            $useMinMax = false;
            if (isset($lastupdate['min'])) {
                $this->addUsingAlias(ApiDaemonPeer::LASTUPDATE, $lastupdate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastupdate['max'])) {
                $this->addUsingAlias(ApiDaemonPeer::LASTUPDATE, $lastupdate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiDaemonPeer::LASTUPDATE, $lastupdate, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ApiDaemon $apiDaemon Object to remove from the list of results
     *
     * @return ApiDaemonQuery The current query, for fluid interface
     */
    public function prune($apiDaemon = null)
    {
        if ($apiDaemon) {
            $this->addUsingAlias(ApiDaemonPeer::ID, $apiDaemon->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
