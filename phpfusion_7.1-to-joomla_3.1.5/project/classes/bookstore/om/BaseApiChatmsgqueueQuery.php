<?php


/**
 * Base class that represents a query for the 'api_chatMsgQueue' table.
 *
 *
 *
 * @method ApiChatmsgqueueQuery orderBySenderid($order = Criteria::ASC) Order by the senderId column
 * @method ApiChatmsgqueueQuery orderByAddresseeid($order = Criteria::ASC) Order by the addresseeId column
 * @method ApiChatmsgqueueQuery orderByDate($order = Criteria::ASC) Order by the date column
 * @method ApiChatmsgqueueQuery orderByContent($order = Criteria::ASC) Order by the content column
 *
 * @method ApiChatmsgqueueQuery groupBySenderid() Group by the senderId column
 * @method ApiChatmsgqueueQuery groupByAddresseeid() Group by the addresseeId column
 * @method ApiChatmsgqueueQuery groupByDate() Group by the date column
 * @method ApiChatmsgqueueQuery groupByContent() Group by the content column
 *
 * @method ApiChatmsgqueueQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method ApiChatmsgqueueQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method ApiChatmsgqueueQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method ApiChatmsgqueue findOne(PropelPDO $con = null) Return the first ApiChatmsgqueue matching the query
 * @method ApiChatmsgqueue findOneOrCreate(PropelPDO $con = null) Return the first ApiChatmsgqueue matching the query, or a new ApiChatmsgqueue object populated from the query conditions when no match is found
 *
 * @method ApiChatmsgqueue findOneBySenderid(string $senderId) Return the first ApiChatmsgqueue filtered by the senderId column
 * @method ApiChatmsgqueue findOneByAddresseeid(string $addresseeId) Return the first ApiChatmsgqueue filtered by the addresseeId column
 * @method ApiChatmsgqueue findOneByDate(string $date) Return the first ApiChatmsgqueue filtered by the date column
 * @method ApiChatmsgqueue findOneByContent(string $content) Return the first ApiChatmsgqueue filtered by the content column
 *
 * @method array findBySenderid(string $senderId) Return ApiChatmsgqueue objects filtered by the senderId column
 * @method array findByAddresseeid(string $addresseeId) Return ApiChatmsgqueue objects filtered by the addresseeId column
 * @method array findByDate(string $date) Return ApiChatmsgqueue objects filtered by the date column
 * @method array findByContent(string $content) Return ApiChatmsgqueue objects filtered by the content column
 *
 * @package    propel.generator.bookstore.om
 */
abstract class BaseApiChatmsgqueueQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseApiChatmsgqueueQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'bookstore';
        }
        if (null === $modelName) {
            $modelName = 'ApiChatmsgqueue';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ApiChatmsgqueueQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   ApiChatmsgqueueQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return ApiChatmsgqueueQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof ApiChatmsgqueueQuery) {
            return $criteria;
        }
        $query = new ApiChatmsgqueueQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34), $con);
     * </code>
     *
     * @param array $key Primary key to use for the query
                         A Primary key composition: [$senderId, $date]
     * @param     PropelPDO $con an optional connection object
     *
     * @return   ApiChatmsgqueue|ApiChatmsgqueue[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = ApiChatmsgqueuePeer::getInstanceFromPool(serialize(array((string) $key[0], (string) $key[1]))))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(ApiChatmsgqueuePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 ApiChatmsgqueue A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `senderId`, `addresseeId`, `date`, `content` FROM `api_chatMsgQueue` WHERE `senderId` = :p0 AND `date` = :p1';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key[0], PDO::PARAM_STR);
            $stmt->bindValue(':p1', $key[1], PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new ApiChatmsgqueue();
            $obj->hydrate($row);
            ApiChatmsgqueuePeer::addInstanceToPool($obj, serialize(array((string) $key[0], (string) $key[1])));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return ApiChatmsgqueue|ApiChatmsgqueue[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|ApiChatmsgqueue[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return ApiChatmsgqueueQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(ApiChatmsgqueuePeer::SENDERID, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(ApiChatmsgqueuePeer::DATE, $key[1], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return ApiChatmsgqueueQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(ApiChatmsgqueuePeer::SENDERID, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(ApiChatmsgqueuePeer::DATE, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the senderId column
     *
     * Example usage:
     * <code>
     * $query->filterBySenderid(1234); // WHERE senderId = 1234
     * $query->filterBySenderid(array(12, 34)); // WHERE senderId IN (12, 34)
     * $query->filterBySenderid(array('min' => 12)); // WHERE senderId >= 12
     * $query->filterBySenderid(array('max' => 12)); // WHERE senderId <= 12
     * </code>
     *
     * @param     mixed $senderid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiChatmsgqueueQuery The current query, for fluid interface
     */
    public function filterBySenderid($senderid = null, $comparison = null)
    {
        if (is_array($senderid)) {
            $useMinMax = false;
            if (isset($senderid['min'])) {
                $this->addUsingAlias(ApiChatmsgqueuePeer::SENDERID, $senderid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($senderid['max'])) {
                $this->addUsingAlias(ApiChatmsgqueuePeer::SENDERID, $senderid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiChatmsgqueuePeer::SENDERID, $senderid, $comparison);
    }

    /**
     * Filter the query on the addresseeId column
     *
     * Example usage:
     * <code>
     * $query->filterByAddresseeid(1234); // WHERE addresseeId = 1234
     * $query->filterByAddresseeid(array(12, 34)); // WHERE addresseeId IN (12, 34)
     * $query->filterByAddresseeid(array('min' => 12)); // WHERE addresseeId >= 12
     * $query->filterByAddresseeid(array('max' => 12)); // WHERE addresseeId <= 12
     * </code>
     *
     * @param     mixed $addresseeid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiChatmsgqueueQuery The current query, for fluid interface
     */
    public function filterByAddresseeid($addresseeid = null, $comparison = null)
    {
        if (is_array($addresseeid)) {
            $useMinMax = false;
            if (isset($addresseeid['min'])) {
                $this->addUsingAlias(ApiChatmsgqueuePeer::ADDRESSEEID, $addresseeid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($addresseeid['max'])) {
                $this->addUsingAlias(ApiChatmsgqueuePeer::ADDRESSEEID, $addresseeid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiChatmsgqueuePeer::ADDRESSEEID, $addresseeid, $comparison);
    }

    /**
     * Filter the query on the date column
     *
     * Example usage:
     * <code>
     * $query->filterByDate('2011-03-14'); // WHERE date = '2011-03-14'
     * $query->filterByDate('now'); // WHERE date = '2011-03-14'
     * $query->filterByDate(array('max' => 'yesterday')); // WHERE date < '2011-03-13'
     * </code>
     *
     * @param     mixed $date The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiChatmsgqueueQuery The current query, for fluid interface
     */
    public function filterByDate($date = null, $comparison = null)
    {
        if (is_array($date)) {
            $useMinMax = false;
            if (isset($date['min'])) {
                $this->addUsingAlias(ApiChatmsgqueuePeer::DATE, $date['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($date['max'])) {
                $this->addUsingAlias(ApiChatmsgqueuePeer::DATE, $date['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiChatmsgqueuePeer::DATE, $date, $comparison);
    }

    /**
     * Filter the query on the content column
     *
     * Example usage:
     * <code>
     * $query->filterByContent('fooValue');   // WHERE content = 'fooValue'
     * $query->filterByContent('%fooValue%'); // WHERE content LIKE '%fooValue%'
     * </code>
     *
     * @param     string $content The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiChatmsgqueueQuery The current query, for fluid interface
     */
    public function filterByContent($content = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($content)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $content)) {
                $content = str_replace('*', '%', $content);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ApiChatmsgqueuePeer::CONTENT, $content, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ApiChatmsgqueue $apiChatmsgqueue Object to remove from the list of results
     *
     * @return ApiChatmsgqueueQuery The current query, for fluid interface
     */
    public function prune($apiChatmsgqueue = null)
    {
        if ($apiChatmsgqueue) {
            $this->addCond('pruneCond0', $this->getAliasedColName(ApiChatmsgqueuePeer::SENDERID), $apiChatmsgqueue->getSenderid(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(ApiChatmsgqueuePeer::DATE), $apiChatmsgqueue->getDate(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

}
