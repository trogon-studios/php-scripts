<?php


/**
 * Base class that represents a query for the 'api_lines' table.
 *
 *
 *
 * @method ApiLinesQuery orderByFromPoint($order = Criteria::ASC) Order by the from_point column
 * @method ApiLinesQuery orderByToPoint($order = Criteria::ASC) Order by the to_point column
 *
 * @method ApiLinesQuery groupByFromPoint() Group by the from_point column
 * @method ApiLinesQuery groupByToPoint() Group by the to_point column
 *
 * @method ApiLinesQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method ApiLinesQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method ApiLinesQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method ApiLines findOne(PropelPDO $con = null) Return the first ApiLines matching the query
 * @method ApiLines findOneOrCreate(PropelPDO $con = null) Return the first ApiLines matching the query, or a new ApiLines object populated from the query conditions when no match is found
 *
 * @method ApiLines findOneByFromPoint(string $from_point) Return the first ApiLines filtered by the from_point column
 * @method ApiLines findOneByToPoint(string $to_point) Return the first ApiLines filtered by the to_point column
 *
 * @method array findByFromPoint(string $from_point) Return ApiLines objects filtered by the from_point column
 * @method array findByToPoint(string $to_point) Return ApiLines objects filtered by the to_point column
 *
 * @package    propel.generator.bookstore.om
 */
abstract class BaseApiLinesQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseApiLinesQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'bookstore';
        }
        if (null === $modelName) {
            $modelName = 'ApiLines';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ApiLinesQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   ApiLinesQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return ApiLinesQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof ApiLinesQuery) {
            return $criteria;
        }
        $query = new ApiLinesQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34), $con);
     * </code>
     *
     * @param array $key Primary key to use for the query
                         A Primary key composition: [$from_point, $to_point]
     * @param     PropelPDO $con an optional connection object
     *
     * @return   ApiLines|ApiLines[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = ApiLinesPeer::getInstanceFromPool(serialize(array((string) $key[0], (string) $key[1]))))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(ApiLinesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 ApiLines A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `from_point`, `to_point` FROM `api_lines` WHERE `from_point` = :p0 AND `to_point` = :p1';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key[0], PDO::PARAM_STR);
            $stmt->bindValue(':p1', $key[1], PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new ApiLines();
            $obj->hydrate($row);
            ApiLinesPeer::addInstanceToPool($obj, serialize(array((string) $key[0], (string) $key[1])));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return ApiLines|ApiLines[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|ApiLines[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return ApiLinesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(ApiLinesPeer::FROM_POINT, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(ApiLinesPeer::TO_POINT, $key[1], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return ApiLinesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(ApiLinesPeer::FROM_POINT, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(ApiLinesPeer::TO_POINT, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the from_point column
     *
     * Example usage:
     * <code>
     * $query->filterByFromPoint(1234); // WHERE from_point = 1234
     * $query->filterByFromPoint(array(12, 34)); // WHERE from_point IN (12, 34)
     * $query->filterByFromPoint(array('min' => 12)); // WHERE from_point >= 12
     * $query->filterByFromPoint(array('max' => 12)); // WHERE from_point <= 12
     * </code>
     *
     * @param     mixed $fromPoint The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiLinesQuery The current query, for fluid interface
     */
    public function filterByFromPoint($fromPoint = null, $comparison = null)
    {
        if (is_array($fromPoint)) {
            $useMinMax = false;
            if (isset($fromPoint['min'])) {
                $this->addUsingAlias(ApiLinesPeer::FROM_POINT, $fromPoint['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fromPoint['max'])) {
                $this->addUsingAlias(ApiLinesPeer::FROM_POINT, $fromPoint['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiLinesPeer::FROM_POINT, $fromPoint, $comparison);
    }

    /**
     * Filter the query on the to_point column
     *
     * Example usage:
     * <code>
     * $query->filterByToPoint(1234); // WHERE to_point = 1234
     * $query->filterByToPoint(array(12, 34)); // WHERE to_point IN (12, 34)
     * $query->filterByToPoint(array('min' => 12)); // WHERE to_point >= 12
     * $query->filterByToPoint(array('max' => 12)); // WHERE to_point <= 12
     * </code>
     *
     * @param     mixed $toPoint The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApiLinesQuery The current query, for fluid interface
     */
    public function filterByToPoint($toPoint = null, $comparison = null)
    {
        if (is_array($toPoint)) {
            $useMinMax = false;
            if (isset($toPoint['min'])) {
                $this->addUsingAlias(ApiLinesPeer::TO_POINT, $toPoint['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($toPoint['max'])) {
                $this->addUsingAlias(ApiLinesPeer::TO_POINT, $toPoint['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApiLinesPeer::TO_POINT, $toPoint, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ApiLines $apiLines Object to remove from the list of results
     *
     * @return ApiLinesQuery The current query, for fluid interface
     */
    public function prune($apiLines = null)
    {
        if ($apiLines) {
            $this->addCond('pruneCond0', $this->getAliasedColName(ApiLinesPeer::FROM_POINT), $apiLines->getFromPoint(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(ApiLinesPeer::TO_POINT), $apiLines->getToPoint(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

}
