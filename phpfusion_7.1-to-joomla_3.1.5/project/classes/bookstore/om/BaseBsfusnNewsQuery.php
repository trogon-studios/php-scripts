<?php


/**
 * Base class that represents a query for the 'bsfusn_news' table.
 *
 *
 *
 * @method BsfusnNewsQuery orderByNewsId($order = Criteria::ASC) Order by the news_id column
 * @method BsfusnNewsQuery orderByNewsSubject($order = Criteria::ASC) Order by the news_subject column
 * @method BsfusnNewsQuery orderByNewsCat($order = Criteria::ASC) Order by the news_cat column
 * @method BsfusnNewsQuery orderByNewsImage($order = Criteria::ASC) Order by the news_image column
 * @method BsfusnNewsQuery orderByNewsImageT1($order = Criteria::ASC) Order by the news_image_t1 column
 * @method BsfusnNewsQuery orderByNewsImageT2($order = Criteria::ASC) Order by the news_image_t2 column
 * @method BsfusnNewsQuery orderByNewsNews($order = Criteria::ASC) Order by the news_news column
 * @method BsfusnNewsQuery orderByNewsExtended($order = Criteria::ASC) Order by the news_extended column
 * @method BsfusnNewsQuery orderByNewsBreaks($order = Criteria::ASC) Order by the news_breaks column
 * @method BsfusnNewsQuery orderByNewsName($order = Criteria::ASC) Order by the news_name column
 * @method BsfusnNewsQuery orderByNewsDatestamp($order = Criteria::ASC) Order by the news_datestamp column
 * @method BsfusnNewsQuery orderByNewsStart($order = Criteria::ASC) Order by the news_start column
 * @method BsfusnNewsQuery orderByNewsEnd($order = Criteria::ASC) Order by the news_end column
 * @method BsfusnNewsQuery orderByNewsVisibility($order = Criteria::ASC) Order by the news_visibility column
 * @method BsfusnNewsQuery orderByNewsReads($order = Criteria::ASC) Order by the news_reads column
 * @method BsfusnNewsQuery orderByNewsDraft($order = Criteria::ASC) Order by the news_draft column
 * @method BsfusnNewsQuery orderByNewsSticky($order = Criteria::ASC) Order by the news_sticky column
 * @method BsfusnNewsQuery orderByNewsAllowComments($order = Criteria::ASC) Order by the news_allow_comments column
 * @method BsfusnNewsQuery orderByNewsAllowRatings($order = Criteria::ASC) Order by the news_allow_ratings column
 *
 * @method BsfusnNewsQuery groupByNewsId() Group by the news_id column
 * @method BsfusnNewsQuery groupByNewsSubject() Group by the news_subject column
 * @method BsfusnNewsQuery groupByNewsCat() Group by the news_cat column
 * @method BsfusnNewsQuery groupByNewsImage() Group by the news_image column
 * @method BsfusnNewsQuery groupByNewsImageT1() Group by the news_image_t1 column
 * @method BsfusnNewsQuery groupByNewsImageT2() Group by the news_image_t2 column
 * @method BsfusnNewsQuery groupByNewsNews() Group by the news_news column
 * @method BsfusnNewsQuery groupByNewsExtended() Group by the news_extended column
 * @method BsfusnNewsQuery groupByNewsBreaks() Group by the news_breaks column
 * @method BsfusnNewsQuery groupByNewsName() Group by the news_name column
 * @method BsfusnNewsQuery groupByNewsDatestamp() Group by the news_datestamp column
 * @method BsfusnNewsQuery groupByNewsStart() Group by the news_start column
 * @method BsfusnNewsQuery groupByNewsEnd() Group by the news_end column
 * @method BsfusnNewsQuery groupByNewsVisibility() Group by the news_visibility column
 * @method BsfusnNewsQuery groupByNewsReads() Group by the news_reads column
 * @method BsfusnNewsQuery groupByNewsDraft() Group by the news_draft column
 * @method BsfusnNewsQuery groupByNewsSticky() Group by the news_sticky column
 * @method BsfusnNewsQuery groupByNewsAllowComments() Group by the news_allow_comments column
 * @method BsfusnNewsQuery groupByNewsAllowRatings() Group by the news_allow_ratings column
 *
 * @method BsfusnNewsQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method BsfusnNewsQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method BsfusnNewsQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method BsfusnNews findOne(PropelPDO $con = null) Return the first BsfusnNews matching the query
 * @method BsfusnNews findOneOrCreate(PropelPDO $con = null) Return the first BsfusnNews matching the query, or a new BsfusnNews object populated from the query conditions when no match is found
 *
 * @method BsfusnNews findOneByNewsSubject(string $news_subject) Return the first BsfusnNews filtered by the news_subject column
 * @method BsfusnNews findOneByNewsCat(int $news_cat) Return the first BsfusnNews filtered by the news_cat column
 * @method BsfusnNews findOneByNewsImage(string $news_image) Return the first BsfusnNews filtered by the news_image column
 * @method BsfusnNews findOneByNewsImageT1(string $news_image_t1) Return the first BsfusnNews filtered by the news_image_t1 column
 * @method BsfusnNews findOneByNewsImageT2(string $news_image_t2) Return the first BsfusnNews filtered by the news_image_t2 column
 * @method BsfusnNews findOneByNewsNews(string $news_news) Return the first BsfusnNews filtered by the news_news column
 * @method BsfusnNews findOneByNewsExtended(string $news_extended) Return the first BsfusnNews filtered by the news_extended column
 * @method BsfusnNews findOneByNewsBreaks(string $news_breaks) Return the first BsfusnNews filtered by the news_breaks column
 * @method BsfusnNews findOneByNewsName(int $news_name) Return the first BsfusnNews filtered by the news_name column
 * @method BsfusnNews findOneByNewsDatestamp(int $news_datestamp) Return the first BsfusnNews filtered by the news_datestamp column
 * @method BsfusnNews findOneByNewsStart(int $news_start) Return the first BsfusnNews filtered by the news_start column
 * @method BsfusnNews findOneByNewsEnd(int $news_end) Return the first BsfusnNews filtered by the news_end column
 * @method BsfusnNews findOneByNewsVisibility(int $news_visibility) Return the first BsfusnNews filtered by the news_visibility column
 * @method BsfusnNews findOneByNewsReads(int $news_reads) Return the first BsfusnNews filtered by the news_reads column
 * @method BsfusnNews findOneByNewsDraft(boolean $news_draft) Return the first BsfusnNews filtered by the news_draft column
 * @method BsfusnNews findOneByNewsSticky(boolean $news_sticky) Return the first BsfusnNews filtered by the news_sticky column
 * @method BsfusnNews findOneByNewsAllowComments(boolean $news_allow_comments) Return the first BsfusnNews filtered by the news_allow_comments column
 * @method BsfusnNews findOneByNewsAllowRatings(boolean $news_allow_ratings) Return the first BsfusnNews filtered by the news_allow_ratings column
 *
 * @method array findByNewsId(int $news_id) Return BsfusnNews objects filtered by the news_id column
 * @method array findByNewsSubject(string $news_subject) Return BsfusnNews objects filtered by the news_subject column
 * @method array findByNewsCat(int $news_cat) Return BsfusnNews objects filtered by the news_cat column
 * @method array findByNewsImage(string $news_image) Return BsfusnNews objects filtered by the news_image column
 * @method array findByNewsImageT1(string $news_image_t1) Return BsfusnNews objects filtered by the news_image_t1 column
 * @method array findByNewsImageT2(string $news_image_t2) Return BsfusnNews objects filtered by the news_image_t2 column
 * @method array findByNewsNews(string $news_news) Return BsfusnNews objects filtered by the news_news column
 * @method array findByNewsExtended(string $news_extended) Return BsfusnNews objects filtered by the news_extended column
 * @method array findByNewsBreaks(string $news_breaks) Return BsfusnNews objects filtered by the news_breaks column
 * @method array findByNewsName(int $news_name) Return BsfusnNews objects filtered by the news_name column
 * @method array findByNewsDatestamp(int $news_datestamp) Return BsfusnNews objects filtered by the news_datestamp column
 * @method array findByNewsStart(int $news_start) Return BsfusnNews objects filtered by the news_start column
 * @method array findByNewsEnd(int $news_end) Return BsfusnNews objects filtered by the news_end column
 * @method array findByNewsVisibility(int $news_visibility) Return BsfusnNews objects filtered by the news_visibility column
 * @method array findByNewsReads(int $news_reads) Return BsfusnNews objects filtered by the news_reads column
 * @method array findByNewsDraft(boolean $news_draft) Return BsfusnNews objects filtered by the news_draft column
 * @method array findByNewsSticky(boolean $news_sticky) Return BsfusnNews objects filtered by the news_sticky column
 * @method array findByNewsAllowComments(boolean $news_allow_comments) Return BsfusnNews objects filtered by the news_allow_comments column
 * @method array findByNewsAllowRatings(boolean $news_allow_ratings) Return BsfusnNews objects filtered by the news_allow_ratings column
 *
 * @package    propel.generator.bookstore.om
 */
abstract class BaseBsfusnNewsQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseBsfusnNewsQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'bookstore';
        }
        if (null === $modelName) {
            $modelName = 'BsfusnNews';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new BsfusnNewsQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   BsfusnNewsQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return BsfusnNewsQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof BsfusnNewsQuery) {
            return $criteria;
        }
        $query = new BsfusnNewsQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   BsfusnNews|BsfusnNews[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = BsfusnNewsPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(BsfusnNewsPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 BsfusnNews A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByNewsId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 BsfusnNews A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `news_id`, `news_subject`, `news_cat`, `news_image`, `news_image_t1`, `news_image_t2`, `news_news`, `news_extended`, `news_breaks`, `news_name`, `news_datestamp`, `news_start`, `news_end`, `news_visibility`, `news_reads`, `news_draft`, `news_sticky`, `news_allow_comments`, `news_allow_ratings` FROM `bsfusn_news` WHERE `news_id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new BsfusnNews();
            $obj->hydrate($row);
            BsfusnNewsPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return BsfusnNews|BsfusnNews[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|BsfusnNews[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return BsfusnNewsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(BsfusnNewsPeer::NEWS_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return BsfusnNewsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(BsfusnNewsPeer::NEWS_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the news_id column
     *
     * Example usage:
     * <code>
     * $query->filterByNewsId(1234); // WHERE news_id = 1234
     * $query->filterByNewsId(array(12, 34)); // WHERE news_id IN (12, 34)
     * $query->filterByNewsId(array('min' => 12)); // WHERE news_id >= 12
     * $query->filterByNewsId(array('max' => 12)); // WHERE news_id <= 12
     * </code>
     *
     * @param     mixed $newsId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BsfusnNewsQuery The current query, for fluid interface
     */
    public function filterByNewsId($newsId = null, $comparison = null)
    {
        if (is_array($newsId)) {
            $useMinMax = false;
            if (isset($newsId['min'])) {
                $this->addUsingAlias(BsfusnNewsPeer::NEWS_ID, $newsId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($newsId['max'])) {
                $this->addUsingAlias(BsfusnNewsPeer::NEWS_ID, $newsId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BsfusnNewsPeer::NEWS_ID, $newsId, $comparison);
    }

    /**
     * Filter the query on the news_subject column
     *
     * Example usage:
     * <code>
     * $query->filterByNewsSubject('fooValue');   // WHERE news_subject = 'fooValue'
     * $query->filterByNewsSubject('%fooValue%'); // WHERE news_subject LIKE '%fooValue%'
     * </code>
     *
     * @param     string $newsSubject The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BsfusnNewsQuery The current query, for fluid interface
     */
    public function filterByNewsSubject($newsSubject = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($newsSubject)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $newsSubject)) {
                $newsSubject = str_replace('*', '%', $newsSubject);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(BsfusnNewsPeer::NEWS_SUBJECT, $newsSubject, $comparison);
    }

    /**
     * Filter the query on the news_cat column
     *
     * Example usage:
     * <code>
     * $query->filterByNewsCat(1234); // WHERE news_cat = 1234
     * $query->filterByNewsCat(array(12, 34)); // WHERE news_cat IN (12, 34)
     * $query->filterByNewsCat(array('min' => 12)); // WHERE news_cat >= 12
     * $query->filterByNewsCat(array('max' => 12)); // WHERE news_cat <= 12
     * </code>
     *
     * @param     mixed $newsCat The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BsfusnNewsQuery The current query, for fluid interface
     */
    public function filterByNewsCat($newsCat = null, $comparison = null)
    {
        if (is_array($newsCat)) {
            $useMinMax = false;
            if (isset($newsCat['min'])) {
                $this->addUsingAlias(BsfusnNewsPeer::NEWS_CAT, $newsCat['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($newsCat['max'])) {
                $this->addUsingAlias(BsfusnNewsPeer::NEWS_CAT, $newsCat['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BsfusnNewsPeer::NEWS_CAT, $newsCat, $comparison);
    }

    /**
     * Filter the query on the news_image column
     *
     * Example usage:
     * <code>
     * $query->filterByNewsImage('fooValue');   // WHERE news_image = 'fooValue'
     * $query->filterByNewsImage('%fooValue%'); // WHERE news_image LIKE '%fooValue%'
     * </code>
     *
     * @param     string $newsImage The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BsfusnNewsQuery The current query, for fluid interface
     */
    public function filterByNewsImage($newsImage = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($newsImage)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $newsImage)) {
                $newsImage = str_replace('*', '%', $newsImage);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(BsfusnNewsPeer::NEWS_IMAGE, $newsImage, $comparison);
    }

    /**
     * Filter the query on the news_image_t1 column
     *
     * Example usage:
     * <code>
     * $query->filterByNewsImageT1('fooValue');   // WHERE news_image_t1 = 'fooValue'
     * $query->filterByNewsImageT1('%fooValue%'); // WHERE news_image_t1 LIKE '%fooValue%'
     * </code>
     *
     * @param     string $newsImageT1 The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BsfusnNewsQuery The current query, for fluid interface
     */
    public function filterByNewsImageT1($newsImageT1 = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($newsImageT1)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $newsImageT1)) {
                $newsImageT1 = str_replace('*', '%', $newsImageT1);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(BsfusnNewsPeer::NEWS_IMAGE_T1, $newsImageT1, $comparison);
    }

    /**
     * Filter the query on the news_image_t2 column
     *
     * Example usage:
     * <code>
     * $query->filterByNewsImageT2('fooValue');   // WHERE news_image_t2 = 'fooValue'
     * $query->filterByNewsImageT2('%fooValue%'); // WHERE news_image_t2 LIKE '%fooValue%'
     * </code>
     *
     * @param     string $newsImageT2 The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BsfusnNewsQuery The current query, for fluid interface
     */
    public function filterByNewsImageT2($newsImageT2 = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($newsImageT2)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $newsImageT2)) {
                $newsImageT2 = str_replace('*', '%', $newsImageT2);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(BsfusnNewsPeer::NEWS_IMAGE_T2, $newsImageT2, $comparison);
    }

    /**
     * Filter the query on the news_news column
     *
     * Example usage:
     * <code>
     * $query->filterByNewsNews('fooValue');   // WHERE news_news = 'fooValue'
     * $query->filterByNewsNews('%fooValue%'); // WHERE news_news LIKE '%fooValue%'
     * </code>
     *
     * @param     string $newsNews The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BsfusnNewsQuery The current query, for fluid interface
     */
    public function filterByNewsNews($newsNews = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($newsNews)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $newsNews)) {
                $newsNews = str_replace('*', '%', $newsNews);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(BsfusnNewsPeer::NEWS_NEWS, $newsNews, $comparison);
    }

    /**
     * Filter the query on the news_extended column
     *
     * Example usage:
     * <code>
     * $query->filterByNewsExtended('fooValue');   // WHERE news_extended = 'fooValue'
     * $query->filterByNewsExtended('%fooValue%'); // WHERE news_extended LIKE '%fooValue%'
     * </code>
     *
     * @param     string $newsExtended The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BsfusnNewsQuery The current query, for fluid interface
     */
    public function filterByNewsExtended($newsExtended = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($newsExtended)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $newsExtended)) {
                $newsExtended = str_replace('*', '%', $newsExtended);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(BsfusnNewsPeer::NEWS_EXTENDED, $newsExtended, $comparison);
    }

    /**
     * Filter the query on the news_breaks column
     *
     * Example usage:
     * <code>
     * $query->filterByNewsBreaks('fooValue');   // WHERE news_breaks = 'fooValue'
     * $query->filterByNewsBreaks('%fooValue%'); // WHERE news_breaks LIKE '%fooValue%'
     * </code>
     *
     * @param     string $newsBreaks The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BsfusnNewsQuery The current query, for fluid interface
     */
    public function filterByNewsBreaks($newsBreaks = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($newsBreaks)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $newsBreaks)) {
                $newsBreaks = str_replace('*', '%', $newsBreaks);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(BsfusnNewsPeer::NEWS_BREAKS, $newsBreaks, $comparison);
    }

    /**
     * Filter the query on the news_name column
     *
     * Example usage:
     * <code>
     * $query->filterByNewsName(1234); // WHERE news_name = 1234
     * $query->filterByNewsName(array(12, 34)); // WHERE news_name IN (12, 34)
     * $query->filterByNewsName(array('min' => 12)); // WHERE news_name >= 12
     * $query->filterByNewsName(array('max' => 12)); // WHERE news_name <= 12
     * </code>
     *
     * @param     mixed $newsName The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BsfusnNewsQuery The current query, for fluid interface
     */
    public function filterByNewsName($newsName = null, $comparison = null)
    {
        if (is_array($newsName)) {
            $useMinMax = false;
            if (isset($newsName['min'])) {
                $this->addUsingAlias(BsfusnNewsPeer::NEWS_NAME, $newsName['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($newsName['max'])) {
                $this->addUsingAlias(BsfusnNewsPeer::NEWS_NAME, $newsName['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BsfusnNewsPeer::NEWS_NAME, $newsName, $comparison);
    }

    /**
     * Filter the query on the news_datestamp column
     *
     * Example usage:
     * <code>
     * $query->filterByNewsDatestamp(1234); // WHERE news_datestamp = 1234
     * $query->filterByNewsDatestamp(array(12, 34)); // WHERE news_datestamp IN (12, 34)
     * $query->filterByNewsDatestamp(array('min' => 12)); // WHERE news_datestamp >= 12
     * $query->filterByNewsDatestamp(array('max' => 12)); // WHERE news_datestamp <= 12
     * </code>
     *
     * @param     mixed $newsDatestamp The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BsfusnNewsQuery The current query, for fluid interface
     */
    public function filterByNewsDatestamp($newsDatestamp = null, $comparison = null)
    {
        if (is_array($newsDatestamp)) {
            $useMinMax = false;
            if (isset($newsDatestamp['min'])) {
                $this->addUsingAlias(BsfusnNewsPeer::NEWS_DATESTAMP, $newsDatestamp['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($newsDatestamp['max'])) {
                $this->addUsingAlias(BsfusnNewsPeer::NEWS_DATESTAMP, $newsDatestamp['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BsfusnNewsPeer::NEWS_DATESTAMP, $newsDatestamp, $comparison);
    }

    /**
     * Filter the query on the news_start column
     *
     * Example usage:
     * <code>
     * $query->filterByNewsStart(1234); // WHERE news_start = 1234
     * $query->filterByNewsStart(array(12, 34)); // WHERE news_start IN (12, 34)
     * $query->filterByNewsStart(array('min' => 12)); // WHERE news_start >= 12
     * $query->filterByNewsStart(array('max' => 12)); // WHERE news_start <= 12
     * </code>
     *
     * @param     mixed $newsStart The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BsfusnNewsQuery The current query, for fluid interface
     */
    public function filterByNewsStart($newsStart = null, $comparison = null)
    {
        if (is_array($newsStart)) {
            $useMinMax = false;
            if (isset($newsStart['min'])) {
                $this->addUsingAlias(BsfusnNewsPeer::NEWS_START, $newsStart['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($newsStart['max'])) {
                $this->addUsingAlias(BsfusnNewsPeer::NEWS_START, $newsStart['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BsfusnNewsPeer::NEWS_START, $newsStart, $comparison);
    }

    /**
     * Filter the query on the news_end column
     *
     * Example usage:
     * <code>
     * $query->filterByNewsEnd(1234); // WHERE news_end = 1234
     * $query->filterByNewsEnd(array(12, 34)); // WHERE news_end IN (12, 34)
     * $query->filterByNewsEnd(array('min' => 12)); // WHERE news_end >= 12
     * $query->filterByNewsEnd(array('max' => 12)); // WHERE news_end <= 12
     * </code>
     *
     * @param     mixed $newsEnd The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BsfusnNewsQuery The current query, for fluid interface
     */
    public function filterByNewsEnd($newsEnd = null, $comparison = null)
    {
        if (is_array($newsEnd)) {
            $useMinMax = false;
            if (isset($newsEnd['min'])) {
                $this->addUsingAlias(BsfusnNewsPeer::NEWS_END, $newsEnd['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($newsEnd['max'])) {
                $this->addUsingAlias(BsfusnNewsPeer::NEWS_END, $newsEnd['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BsfusnNewsPeer::NEWS_END, $newsEnd, $comparison);
    }

    /**
     * Filter the query on the news_visibility column
     *
     * Example usage:
     * <code>
     * $query->filterByNewsVisibility(1234); // WHERE news_visibility = 1234
     * $query->filterByNewsVisibility(array(12, 34)); // WHERE news_visibility IN (12, 34)
     * $query->filterByNewsVisibility(array('min' => 12)); // WHERE news_visibility >= 12
     * $query->filterByNewsVisibility(array('max' => 12)); // WHERE news_visibility <= 12
     * </code>
     *
     * @param     mixed $newsVisibility The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BsfusnNewsQuery The current query, for fluid interface
     */
    public function filterByNewsVisibility($newsVisibility = null, $comparison = null)
    {
        if (is_array($newsVisibility)) {
            $useMinMax = false;
            if (isset($newsVisibility['min'])) {
                $this->addUsingAlias(BsfusnNewsPeer::NEWS_VISIBILITY, $newsVisibility['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($newsVisibility['max'])) {
                $this->addUsingAlias(BsfusnNewsPeer::NEWS_VISIBILITY, $newsVisibility['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BsfusnNewsPeer::NEWS_VISIBILITY, $newsVisibility, $comparison);
    }

    /**
     * Filter the query on the news_reads column
     *
     * Example usage:
     * <code>
     * $query->filterByNewsReads(1234); // WHERE news_reads = 1234
     * $query->filterByNewsReads(array(12, 34)); // WHERE news_reads IN (12, 34)
     * $query->filterByNewsReads(array('min' => 12)); // WHERE news_reads >= 12
     * $query->filterByNewsReads(array('max' => 12)); // WHERE news_reads <= 12
     * </code>
     *
     * @param     mixed $newsReads The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BsfusnNewsQuery The current query, for fluid interface
     */
    public function filterByNewsReads($newsReads = null, $comparison = null)
    {
        if (is_array($newsReads)) {
            $useMinMax = false;
            if (isset($newsReads['min'])) {
                $this->addUsingAlias(BsfusnNewsPeer::NEWS_READS, $newsReads['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($newsReads['max'])) {
                $this->addUsingAlias(BsfusnNewsPeer::NEWS_READS, $newsReads['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BsfusnNewsPeer::NEWS_READS, $newsReads, $comparison);
    }

    /**
     * Filter the query on the news_draft column
     *
     * Example usage:
     * <code>
     * $query->filterByNewsDraft(true); // WHERE news_draft = true
     * $query->filterByNewsDraft('yes'); // WHERE news_draft = true
     * </code>
     *
     * @param     boolean|string $newsDraft The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BsfusnNewsQuery The current query, for fluid interface
     */
    public function filterByNewsDraft($newsDraft = null, $comparison = null)
    {
        if (is_string($newsDraft)) {
            $newsDraft = in_array(strtolower($newsDraft), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(BsfusnNewsPeer::NEWS_DRAFT, $newsDraft, $comparison);
    }

    /**
     * Filter the query on the news_sticky column
     *
     * Example usage:
     * <code>
     * $query->filterByNewsSticky(true); // WHERE news_sticky = true
     * $query->filterByNewsSticky('yes'); // WHERE news_sticky = true
     * </code>
     *
     * @param     boolean|string $newsSticky The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BsfusnNewsQuery The current query, for fluid interface
     */
    public function filterByNewsSticky($newsSticky = null, $comparison = null)
    {
        if (is_string($newsSticky)) {
            $newsSticky = in_array(strtolower($newsSticky), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(BsfusnNewsPeer::NEWS_STICKY, $newsSticky, $comparison);
    }

    /**
     * Filter the query on the news_allow_comments column
     *
     * Example usage:
     * <code>
     * $query->filterByNewsAllowComments(true); // WHERE news_allow_comments = true
     * $query->filterByNewsAllowComments('yes'); // WHERE news_allow_comments = true
     * </code>
     *
     * @param     boolean|string $newsAllowComments The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BsfusnNewsQuery The current query, for fluid interface
     */
    public function filterByNewsAllowComments($newsAllowComments = null, $comparison = null)
    {
        if (is_string($newsAllowComments)) {
            $newsAllowComments = in_array(strtolower($newsAllowComments), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(BsfusnNewsPeer::NEWS_ALLOW_COMMENTS, $newsAllowComments, $comparison);
    }

    /**
     * Filter the query on the news_allow_ratings column
     *
     * Example usage:
     * <code>
     * $query->filterByNewsAllowRatings(true); // WHERE news_allow_ratings = true
     * $query->filterByNewsAllowRatings('yes'); // WHERE news_allow_ratings = true
     * </code>
     *
     * @param     boolean|string $newsAllowRatings The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return BsfusnNewsQuery The current query, for fluid interface
     */
    public function filterByNewsAllowRatings($newsAllowRatings = null, $comparison = null)
    {
        if (is_string($newsAllowRatings)) {
            $newsAllowRatings = in_array(strtolower($newsAllowRatings), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(BsfusnNewsPeer::NEWS_ALLOW_RATINGS, $newsAllowRatings, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   BsfusnNews $bsfusnNews Object to remove from the list of results
     *
     * @return BsfusnNewsQuery The current query, for fluid interface
     */
    public function prune($bsfusnNews = null)
    {
        if ($bsfusnNews) {
            $this->addUsingAlias(BsfusnNewsPeer::NEWS_ID, $bsfusnNews->getNewsId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
