<?php


/**
 * Base class that represents a row from the 'api_buildingQueue' table.
 *
 *
 *
 * @package    propel.generator.bookstore.om
 */
abstract class BaseApiBuildingqueue extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'ApiBuildingqueuePeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        ApiBuildingqueuePeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        string
     */
    protected $id;

    /**
     * The value for the level field.
     * @var        int
     */
    protected $level;

    /**
     * The value for the positionx field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $positionx;

    /**
     * The value for the positiony field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $positiony;

    /**
     * The value for the dateoffinish field.
     * @var        string
     */
    protected $dateoffinish;

    /**
     * The value for the buildingqueue_buildingid field.
     * @var        string
     */
    protected $buildingqueue_buildingid;

    /**
     * The value for the buildingqueue_buildingtype field.
     * @var        int
     */
    protected $buildingqueue_buildingtype;

    /**
     * The value for the buildingqueue_player field.
     * @var        string
     */
    protected $buildingqueue_player;

    /**
     * The value for the buildingqueue_plot field.
     * @var        string
     */
    protected $buildingqueue_plot;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->positionx = 0;
        $this->positiony = 0;
    }

    /**
     * Initializes internal state of BaseApiBuildingqueue object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [id] column value.
     *
     * @return string
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [level] column value.
     *
     * @return int
     */
    public function getLevel()
    {

        return $this->level;
    }

    /**
     * Get the [positionx] column value.
     *
     * @return int
     */
    public function getPositionx()
    {

        return $this->positionx;
    }

    /**
     * Get the [positiony] column value.
     *
     * @return int
     */
    public function getPositiony()
    {

        return $this->positiony;
    }

    /**
     * Get the [optionally formatted] temporal [dateoffinish] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateoffinish($format = 'Y-m-d H:i:s')
    {
        if ($this->dateoffinish === null) {
            return null;
        }

        if ($this->dateoffinish === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->dateoffinish);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->dateoffinish, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [buildingqueue_buildingid] column value.
     *
     * @return string
     */
    public function getBuildingqueueBuildingid()
    {

        return $this->buildingqueue_buildingid;
    }

    /**
     * Get the [buildingqueue_buildingtype] column value.
     *
     * @return int
     */
    public function getBuildingqueueBuildingtype()
    {

        return $this->buildingqueue_buildingtype;
    }

    /**
     * Get the [buildingqueue_player] column value.
     *
     * @return string
     */
    public function getBuildingqueuePlayer()
    {

        return $this->buildingqueue_player;
    }

    /**
     * Get the [buildingqueue_plot] column value.
     *
     * @return string
     */
    public function getBuildingqueuePlot()
    {

        return $this->buildingqueue_plot;
    }

    /**
     * Set the value of [id] column.
     *
     * @param  string $v new value
     * @return ApiBuildingqueue The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = ApiBuildingqueuePeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [level] column.
     *
     * @param  int $v new value
     * @return ApiBuildingqueue The current object (for fluent API support)
     */
    public function setLevel($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->level !== $v) {
            $this->level = $v;
            $this->modifiedColumns[] = ApiBuildingqueuePeer::LEVEL;
        }


        return $this;
    } // setLevel()

    /**
     * Set the value of [positionx] column.
     *
     * @param  int $v new value
     * @return ApiBuildingqueue The current object (for fluent API support)
     */
    public function setPositionx($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->positionx !== $v) {
            $this->positionx = $v;
            $this->modifiedColumns[] = ApiBuildingqueuePeer::POSITIONX;
        }


        return $this;
    } // setPositionx()

    /**
     * Set the value of [positiony] column.
     *
     * @param  int $v new value
     * @return ApiBuildingqueue The current object (for fluent API support)
     */
    public function setPositiony($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->positiony !== $v) {
            $this->positiony = $v;
            $this->modifiedColumns[] = ApiBuildingqueuePeer::POSITIONY;
        }


        return $this;
    } // setPositiony()

    /**
     * Sets the value of [dateoffinish] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return ApiBuildingqueue The current object (for fluent API support)
     */
    public function setDateoffinish($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->dateoffinish !== null || $dt !== null) {
            $currentDateAsString = ($this->dateoffinish !== null && $tmpDt = new DateTime($this->dateoffinish)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->dateoffinish = $newDateAsString;
                $this->modifiedColumns[] = ApiBuildingqueuePeer::DATEOFFINISH;
            }
        } // if either are not null


        return $this;
    } // setDateoffinish()

    /**
     * Set the value of [buildingqueue_buildingid] column.
     *
     * @param  string $v new value
     * @return ApiBuildingqueue The current object (for fluent API support)
     */
    public function setBuildingqueueBuildingid($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->buildingqueue_buildingid !== $v) {
            $this->buildingqueue_buildingid = $v;
            $this->modifiedColumns[] = ApiBuildingqueuePeer::BUILDINGQUEUE_BUILDINGID;
        }


        return $this;
    } // setBuildingqueueBuildingid()

    /**
     * Set the value of [buildingqueue_buildingtype] column.
     *
     * @param  int $v new value
     * @return ApiBuildingqueue The current object (for fluent API support)
     */
    public function setBuildingqueueBuildingtype($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->buildingqueue_buildingtype !== $v) {
            $this->buildingqueue_buildingtype = $v;
            $this->modifiedColumns[] = ApiBuildingqueuePeer::BUILDINGQUEUE_BUILDINGTYPE;
        }


        return $this;
    } // setBuildingqueueBuildingtype()

    /**
     * Set the value of [buildingqueue_player] column.
     *
     * @param  string $v new value
     * @return ApiBuildingqueue The current object (for fluent API support)
     */
    public function setBuildingqueuePlayer($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->buildingqueue_player !== $v) {
            $this->buildingqueue_player = $v;
            $this->modifiedColumns[] = ApiBuildingqueuePeer::BUILDINGQUEUE_PLAYER;
        }


        return $this;
    } // setBuildingqueuePlayer()

    /**
     * Set the value of [buildingqueue_plot] column.
     *
     * @param  string $v new value
     * @return ApiBuildingqueue The current object (for fluent API support)
     */
    public function setBuildingqueuePlot($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->buildingqueue_plot !== $v) {
            $this->buildingqueue_plot = $v;
            $this->modifiedColumns[] = ApiBuildingqueuePeer::BUILDINGQUEUE_PLOT;
        }


        return $this;
    } // setBuildingqueuePlot()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->positionx !== 0) {
                return false;
            }

            if ($this->positiony !== 0) {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (string) $row[$startcol + 0] : null;
            $this->level = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->positionx = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
            $this->positiony = ($row[$startcol + 3] !== null) ? (int) $row[$startcol + 3] : null;
            $this->dateoffinish = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->buildingqueue_buildingid = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->buildingqueue_buildingtype = ($row[$startcol + 6] !== null) ? (int) $row[$startcol + 6] : null;
            $this->buildingqueue_player = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->buildingqueue_plot = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 9; // 9 = ApiBuildingqueuePeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating ApiBuildingqueue object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(ApiBuildingqueuePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = ApiBuildingqueuePeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(ApiBuildingqueuePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = ApiBuildingqueueQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(ApiBuildingqueuePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                ApiBuildingqueuePeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = ApiBuildingqueuePeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . ApiBuildingqueuePeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(ApiBuildingqueuePeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(ApiBuildingqueuePeer::LEVEL)) {
            $modifiedColumns[':p' . $index++]  = '`level`';
        }
        if ($this->isColumnModified(ApiBuildingqueuePeer::POSITIONX)) {
            $modifiedColumns[':p' . $index++]  = '`positionX`';
        }
        if ($this->isColumnModified(ApiBuildingqueuePeer::POSITIONY)) {
            $modifiedColumns[':p' . $index++]  = '`positionY`';
        }
        if ($this->isColumnModified(ApiBuildingqueuePeer::DATEOFFINISH)) {
            $modifiedColumns[':p' . $index++]  = '`dateOfFinish`';
        }
        if ($this->isColumnModified(ApiBuildingqueuePeer::BUILDINGQUEUE_BUILDINGID)) {
            $modifiedColumns[':p' . $index++]  = '`buildingQueue_buildingId`';
        }
        if ($this->isColumnModified(ApiBuildingqueuePeer::BUILDINGQUEUE_BUILDINGTYPE)) {
            $modifiedColumns[':p' . $index++]  = '`buildingQueue_buildingType`';
        }
        if ($this->isColumnModified(ApiBuildingqueuePeer::BUILDINGQUEUE_PLAYER)) {
            $modifiedColumns[':p' . $index++]  = '`buildingQueue_player`';
        }
        if ($this->isColumnModified(ApiBuildingqueuePeer::BUILDINGQUEUE_PLOT)) {
            $modifiedColumns[':p' . $index++]  = '`buildingQueue_plot`';
        }

        $sql = sprintf(
            'INSERT INTO `api_buildingQueue` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_STR);
                        break;
                    case '`level`':
                        $stmt->bindValue($identifier, $this->level, PDO::PARAM_INT);
                        break;
                    case '`positionX`':
                        $stmt->bindValue($identifier, $this->positionx, PDO::PARAM_INT);
                        break;
                    case '`positionY`':
                        $stmt->bindValue($identifier, $this->positiony, PDO::PARAM_INT);
                        break;
                    case '`dateOfFinish`':
                        $stmt->bindValue($identifier, $this->dateoffinish, PDO::PARAM_STR);
                        break;
                    case '`buildingQueue_buildingId`':
                        $stmt->bindValue($identifier, $this->buildingqueue_buildingid, PDO::PARAM_STR);
                        break;
                    case '`buildingQueue_buildingType`':
                        $stmt->bindValue($identifier, $this->buildingqueue_buildingtype, PDO::PARAM_INT);
                        break;
                    case '`buildingQueue_player`':
                        $stmt->bindValue($identifier, $this->buildingqueue_player, PDO::PARAM_STR);
                        break;
                    case '`buildingQueue_plot`':
                        $stmt->bindValue($identifier, $this->buildingqueue_plot, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            if (($retval = ApiBuildingqueuePeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }



            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = ApiBuildingqueuePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getLevel();
                break;
            case 2:
                return $this->getPositionx();
                break;
            case 3:
                return $this->getPositiony();
                break;
            case 4:
                return $this->getDateoffinish();
                break;
            case 5:
                return $this->getBuildingqueueBuildingid();
                break;
            case 6:
                return $this->getBuildingqueueBuildingtype();
                break;
            case 7:
                return $this->getBuildingqueuePlayer();
                break;
            case 8:
                return $this->getBuildingqueuePlot();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array())
    {
        if (isset($alreadyDumpedObjects['ApiBuildingqueue'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['ApiBuildingqueue'][$this->getPrimaryKey()] = true;
        $keys = ApiBuildingqueuePeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getLevel(),
            $keys[2] => $this->getPositionx(),
            $keys[3] => $this->getPositiony(),
            $keys[4] => $this->getDateoffinish(),
            $keys[5] => $this->getBuildingqueueBuildingid(),
            $keys[6] => $this->getBuildingqueueBuildingtype(),
            $keys[7] => $this->getBuildingqueuePlayer(),
            $keys[8] => $this->getBuildingqueuePlot(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach($virtualColumns as $key => $virtualColumn)
        {
            $result[$key] = $virtualColumn;
        }


        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = ApiBuildingqueuePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setLevel($value);
                break;
            case 2:
                $this->setPositionx($value);
                break;
            case 3:
                $this->setPositiony($value);
                break;
            case 4:
                $this->setDateoffinish($value);
                break;
            case 5:
                $this->setBuildingqueueBuildingid($value);
                break;
            case 6:
                $this->setBuildingqueueBuildingtype($value);
                break;
            case 7:
                $this->setBuildingqueuePlayer($value);
                break;
            case 8:
                $this->setBuildingqueuePlot($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = ApiBuildingqueuePeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setLevel($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setPositionx($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setPositiony($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setDateoffinish($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setBuildingqueueBuildingid($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setBuildingqueueBuildingtype($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setBuildingqueuePlayer($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setBuildingqueuePlot($arr[$keys[8]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(ApiBuildingqueuePeer::DATABASE_NAME);

        if ($this->isColumnModified(ApiBuildingqueuePeer::ID)) $criteria->add(ApiBuildingqueuePeer::ID, $this->id);
        if ($this->isColumnModified(ApiBuildingqueuePeer::LEVEL)) $criteria->add(ApiBuildingqueuePeer::LEVEL, $this->level);
        if ($this->isColumnModified(ApiBuildingqueuePeer::POSITIONX)) $criteria->add(ApiBuildingqueuePeer::POSITIONX, $this->positionx);
        if ($this->isColumnModified(ApiBuildingqueuePeer::POSITIONY)) $criteria->add(ApiBuildingqueuePeer::POSITIONY, $this->positiony);
        if ($this->isColumnModified(ApiBuildingqueuePeer::DATEOFFINISH)) $criteria->add(ApiBuildingqueuePeer::DATEOFFINISH, $this->dateoffinish);
        if ($this->isColumnModified(ApiBuildingqueuePeer::BUILDINGQUEUE_BUILDINGID)) $criteria->add(ApiBuildingqueuePeer::BUILDINGQUEUE_BUILDINGID, $this->buildingqueue_buildingid);
        if ($this->isColumnModified(ApiBuildingqueuePeer::BUILDINGQUEUE_BUILDINGTYPE)) $criteria->add(ApiBuildingqueuePeer::BUILDINGQUEUE_BUILDINGTYPE, $this->buildingqueue_buildingtype);
        if ($this->isColumnModified(ApiBuildingqueuePeer::BUILDINGQUEUE_PLAYER)) $criteria->add(ApiBuildingqueuePeer::BUILDINGQUEUE_PLAYER, $this->buildingqueue_player);
        if ($this->isColumnModified(ApiBuildingqueuePeer::BUILDINGQUEUE_PLOT)) $criteria->add(ApiBuildingqueuePeer::BUILDINGQUEUE_PLOT, $this->buildingqueue_plot);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(ApiBuildingqueuePeer::DATABASE_NAME);
        $criteria->add(ApiBuildingqueuePeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of ApiBuildingqueue (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setLevel($this->getLevel());
        $copyObj->setPositionx($this->getPositionx());
        $copyObj->setPositiony($this->getPositiony());
        $copyObj->setDateoffinish($this->getDateoffinish());
        $copyObj->setBuildingqueueBuildingid($this->getBuildingqueueBuildingid());
        $copyObj->setBuildingqueueBuildingtype($this->getBuildingqueueBuildingtype());
        $copyObj->setBuildingqueuePlayer($this->getBuildingqueuePlayer());
        $copyObj->setBuildingqueuePlot($this->getBuildingqueuePlot());
        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return ApiBuildingqueue Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return ApiBuildingqueuePeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new ApiBuildingqueuePeer();
        }

        return self::$peer;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->level = null;
        $this->positionx = null;
        $this->positiony = null;
        $this->dateoffinish = null;
        $this->buildingqueue_buildingid = null;
        $this->buildingqueue_buildingtype = null;
        $this->buildingqueue_player = null;
        $this->buildingqueue_plot = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(ApiBuildingqueuePeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
