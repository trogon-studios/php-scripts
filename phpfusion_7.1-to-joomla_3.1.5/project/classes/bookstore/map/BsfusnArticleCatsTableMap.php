<?php



/**
 * This class defines the structure of the 'bsfusn_article_cats' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.bookstore.map
 */
class BsfusnArticleCatsTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'bookstore.map.BsfusnArticleCatsTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('bsfusn_article_cats');
        $this->setPhpName('BsfusnArticleCats');
        $this->setClassname('BsfusnArticleCats');
        $this->setPackage('bookstore');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('article_cat_id', 'ArticleCatId', 'SMALLINT', true, 8, null);
        $this->addColumn('article_cat_name', 'ArticleCatName', 'VARCHAR', true, 100, '');
        $this->addColumn('article_cat_description', 'ArticleCatDescription', 'VARCHAR', true, 200, '');
        $this->addColumn('article_cat_sorting', 'ArticleCatSorting', 'VARCHAR', true, 50, 'article_subject ASC');
        $this->addColumn('article_cat_access', 'ArticleCatAccess', 'TINYINT', true, 3, 0);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // BsfusnArticleCatsTableMap
