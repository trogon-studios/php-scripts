<?php



/**
 * This class defines the structure of the 'api_chatMsgQueue' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.bookstore.map
 */
class ApiChatmsgqueueTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'bookstore.map.ApiChatmsgqueueTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('api_chatMsgQueue');
        $this->setPhpName('ApiChatmsgqueue');
        $this->setClassname('ApiChatmsgqueue');
        $this->setPackage('bookstore');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('senderId', 'Senderid', 'BIGINT', true, null, null);
        $this->addColumn('addresseeId', 'Addresseeid', 'BIGINT', false, null, null);
        $this->addPrimaryKey('date', 'Date', 'TIMESTAMP', true, null, 'CURRENT_TIMESTAMP');
        $this->addColumn('content', 'Content', 'VARCHAR', true, 160, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // ApiChatmsgqueueTableMap
