<?php



/**
 * This class defines the structure of the 'api_users' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.bookstore.map
 */
class ApiUsersTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'bookstore.map.ApiUsersTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('api_users');
        $this->setPhpName('ApiUsers');
        $this->setClassname('ApiUsers');
        $this->setPackage('bookstore');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'BIGINT', true, null, null);
        $this->addColumn('username', 'Username', 'VARCHAR', true, 15, null);
        $this->addColumn('password', 'Password', 'VARCHAR', true, 64, null);
        $this->addColumn('email', 'Email', 'VARCHAR', true, 40, null);
        $this->addColumn('birthdate', 'Birthdate', 'DATE', true, null, null);
        $this->addColumn('playername', 'Playername', 'VARCHAR', true, 30, null);
        $this->addColumn('playerage', 'Playerage', 'INTEGER', true, 10, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // ApiUsersTableMap
