<?php



/**
 * This class defines the structure of the 'api_techonolgyQueue' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.bookstore.map
 */
class ApiTechonolgyqueueTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'bookstore.map.ApiTechonolgyqueueTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('api_techonolgyQueue');
        $this->setPhpName('ApiTechonolgyqueue');
        $this->setClassname('ApiTechonolgyqueue');
        $this->setPackage('bookstore');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, 10, null);
        $this->addColumn('dateOfFinish', 'Dateoffinish', 'TIMESTAMP', true, null, null);
        $this->addColumn('techonolgyQueue_techonolgyType', 'TechonolgyqueueTechonolgytype', 'BIGINT', true, null, null);
        $this->addColumn('techonolgyQueue_player', 'TechonolgyqueuePlayer', 'BIGINT', true, null, null);
        $this->addColumn('techonolgyQueue_plot', 'TechonolgyqueuePlot', 'BIGINT', true, null, null);
        $this->addColumn('techonolgyQueue_building', 'TechonolgyqueueBuilding', 'BIGINT', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // ApiTechonolgyqueueTableMap
