<?php



/**
 * This class defines the structure of the 'api_buildings' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.bookstore.map
 */
class ApiBuildingsTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'bookstore.map.ApiBuildingsTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('api_buildings');
        $this->setPhpName('ApiBuildings');
        $this->setClassname('ApiBuildings');
        $this->setPackage('bookstore');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'BIGINT', true, null, null);
        $this->addColumn('level', 'Level', 'INTEGER', true, 10, null);
        $this->addColumn('positionX', 'Positionx', 'INTEGER', true, 10, null);
        $this->addColumn('positionY', 'Positiony', 'INTEGER', true, 10, null);
        $this->addColumn('health', 'Health', 'INTEGER', true, 10, null);
        $this->addColumn('building_buildingType', 'BuildingBuildingtype', 'INTEGER', true, 10, null);
        $this->addColumn('building_plot', 'BuildingPlot', 'BIGINT', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // ApiBuildingsTableMap
