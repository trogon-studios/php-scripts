<?php



/**
 * This class defines the structure of the 'bsfusn_news' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.bookstore.map
 */
class BsfusnNewsTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'bookstore.map.BsfusnNewsTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('bsfusn_news');
        $this->setPhpName('BsfusnNews');
        $this->setClassname('BsfusnNews');
        $this->setPackage('bookstore');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('news_id', 'NewsId', 'SMALLINT', true, 8, null);
        $this->addColumn('news_subject', 'NewsSubject', 'VARCHAR', true, 200, '');
        $this->addColumn('news_cat', 'NewsCat', 'SMALLINT', true, 8, 0);
        $this->addColumn('news_image', 'NewsImage', 'VARCHAR', true, 100, '');
        $this->addColumn('news_image_t1', 'NewsImageT1', 'VARCHAR', true, 100, '');
        $this->addColumn('news_image_t2', 'NewsImageT2', 'VARCHAR', true, 100, '');
        $this->addColumn('news_news', 'NewsNews', 'LONGVARCHAR', true, null, null);
        $this->addColumn('news_extended', 'NewsExtended', 'LONGVARCHAR', true, null, null);
        $this->addColumn('news_breaks', 'NewsBreaks', 'CHAR', true, null, '');
        $this->addColumn('news_name', 'NewsName', 'SMALLINT', true, 8, 1);
        $this->addColumn('news_datestamp', 'NewsDatestamp', 'INTEGER', true, 10, 0);
        $this->addColumn('news_start', 'NewsStart', 'INTEGER', true, 10, 0);
        $this->addColumn('news_end', 'NewsEnd', 'INTEGER', true, 10, 0);
        $this->addColumn('news_visibility', 'NewsVisibility', 'TINYINT', true, 3, 0);
        $this->addColumn('news_reads', 'NewsReads', 'INTEGER', true, 10, 0);
        $this->addColumn('news_draft', 'NewsDraft', 'BOOLEAN', true, 1, false);
        $this->addColumn('news_sticky', 'NewsSticky', 'BOOLEAN', true, 1, false);
        $this->addColumn('news_allow_comments', 'NewsAllowComments', 'BOOLEAN', true, 1, true);
        $this->addColumn('news_allow_ratings', 'NewsAllowRatings', 'BOOLEAN', true, 1, true);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // BsfusnNewsTableMap
