<?php



/**
 * This class defines the structure of the 'bsfusn_news_cats' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.bookstore.map
 */
class BsfusnNewsCatsTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'bookstore.map.BsfusnNewsCatsTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('bsfusn_news_cats');
        $this->setPhpName('BsfusnNewsCats');
        $this->setClassname('BsfusnNewsCats');
        $this->setPackage('bookstore');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('news_cat_id', 'NewsCatId', 'SMALLINT', true, 8, null);
        $this->addColumn('news_cat_name', 'NewsCatName', 'VARCHAR', true, 100, '');
        $this->addColumn('news_cat_image', 'NewsCatImage', 'VARCHAR', true, 100, '');
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // BsfusnNewsCatsTableMap
