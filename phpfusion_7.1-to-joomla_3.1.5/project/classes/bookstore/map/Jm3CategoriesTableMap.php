<?php



/**
 * This class defines the structure of the 'jm3_categories' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.bookstore.map
 */
class Jm3CategoriesTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'bookstore.map.Jm3CategoriesTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('jm3_categories');
        $this->setPhpName('Jm3Categories');
        $this->setClassname('Jm3Categories');
        $this->setPackage('bookstore');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('asset_id', 'AssetId', 'INTEGER', true, 10, 0);
        $this->addColumn('parent_id', 'ParentId', 'INTEGER', true, 10, 0);
        $this->addColumn('lft', 'Lft', 'INTEGER', true, null, 0);
        $this->addColumn('rgt', 'Rgt', 'INTEGER', true, null, 0);
        $this->addColumn('level', 'Level', 'INTEGER', true, 10, 0);
        $this->addColumn('path', 'Path', 'VARCHAR', true, 255, '');
        $this->addColumn('extension', 'Extension', 'VARCHAR', true, 50, '');
        $this->addColumn('title', 'Title', 'VARCHAR', true, 255, null);
        $this->addColumn('alias', 'Alias', 'VARCHAR', true, 255, '');
        $this->addColumn('note', 'Note', 'VARCHAR', true, 255, '');
        $this->addColumn('description', 'Description', 'LONGVARCHAR', true, null, null);
        $this->addColumn('published', 'Published', 'BOOLEAN', true, 1, false);
        $this->addColumn('checked_out', 'CheckedOut', 'INTEGER', true, null, 0);
        $this->addColumn('checked_out_time', 'CheckedOutTime', 'TIMESTAMP', true, null, '0000-00-00 00:00:00');
        $this->addColumn('access', 'Access', 'INTEGER', true, 10, 0);
        $this->addColumn('params', 'Params', 'LONGVARCHAR', true, null, null);
        $this->addColumn('metadesc', 'Metadesc', 'VARCHAR', true, 1024, null);
        $this->addColumn('metakey', 'Metakey', 'VARCHAR', true, 1024, null);
        $this->addColumn('metadata', 'Metadata', 'VARCHAR', true, 2048, null);
        $this->addColumn('created_user_id', 'CreatedUserId', 'INTEGER', true, 10, 0);
        $this->addColumn('created_time', 'CreatedTime', 'TIMESTAMP', true, null, '0000-00-00 00:00:00');
        $this->addColumn('modified_user_id', 'ModifiedUserId', 'INTEGER', true, 10, 0);
        $this->addColumn('modified_time', 'ModifiedTime', 'TIMESTAMP', true, null, '0000-00-00 00:00:00');
        $this->addColumn('hits', 'Hits', 'INTEGER', true, 10, 0);
        $this->addColumn('language', 'Language', 'CHAR', true, 7, null);
        $this->addColumn('version', 'Version', 'INTEGER', true, 10, 1);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // Jm3CategoriesTableMap
