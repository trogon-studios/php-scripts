<?php



/**
 * This class defines the structure of the 'jm3_content' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.bookstore.map
 */
class Jm3ContentTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'bookstore.map.Jm3ContentTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('jm3_content');
        $this->setPhpName('Jm3Content');
        $this->setClassname('Jm3Content');
        $this->setPackage('bookstore');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, 10, null);
        $this->addColumn('asset_id', 'AssetId', 'INTEGER', true, 10, 0);
        $this->addColumn('title', 'Title', 'VARCHAR', true, 255, '');
        $this->addColumn('alias', 'Alias', 'VARCHAR', true, 255, '');
        $this->addColumn('introtext', 'Introtext', 'LONGVARCHAR', true, null, null);
        $this->addColumn('fulltext', 'Fulltext', 'LONGVARCHAR', true, null, null);
        $this->addColumn('state', 'State', 'TINYINT', true, 3, 0);
        $this->addColumn('catid', 'Catid', 'INTEGER', true, 10, 0);
        $this->addColumn('created', 'Created', 'TIMESTAMP', true, null, '0000-00-00 00:00:00');
        $this->addColumn('created_by', 'CreatedBy', 'INTEGER', true, 10, 0);
        $this->addColumn('created_by_alias', 'CreatedByAlias', 'VARCHAR', true, 255, '');
        $this->addColumn('modified', 'Modified', 'TIMESTAMP', true, null, '0000-00-00 00:00:00');
        $this->addColumn('modified_by', 'ModifiedBy', 'INTEGER', true, 10, 0);
        $this->addColumn('checked_out', 'CheckedOut', 'INTEGER', true, 10, 0);
        $this->addColumn('checked_out_time', 'CheckedOutTime', 'TIMESTAMP', true, null, '0000-00-00 00:00:00');
        $this->addColumn('publish_up', 'PublishUp', 'TIMESTAMP', true, null, '0000-00-00 00:00:00');
        $this->addColumn('publish_down', 'PublishDown', 'TIMESTAMP', true, null, '0000-00-00 00:00:00');
        $this->addColumn('images', 'Images', 'LONGVARCHAR', true, null, null);
        $this->addColumn('urls', 'Urls', 'LONGVARCHAR', true, null, null);
        $this->addColumn('attribs', 'Attribs', 'VARCHAR', true, 5120, null);
        $this->addColumn('version', 'Version', 'INTEGER', true, 10, 1);
        $this->addColumn('ordering', 'Ordering', 'INTEGER', true, null, 0);
        $this->addColumn('metakey', 'Metakey', 'LONGVARCHAR', true, null, null);
        $this->addColumn('metadesc', 'Metadesc', 'LONGVARCHAR', true, null, null);
        $this->addColumn('access', 'Access', 'INTEGER', true, 10, 0);
        $this->addColumn('hits', 'Hits', 'INTEGER', true, 10, 0);
        $this->addColumn('metadata', 'Metadata', 'LONGVARCHAR', true, null, null);
        $this->addColumn('featured', 'Featured', 'TINYINT', true, 3, 0);
        $this->addColumn('language', 'Language', 'CHAR', true, 7, null);
        $this->addColumn('xreference', 'Xreference', 'VARCHAR', true, 50, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // Jm3ContentTableMap
