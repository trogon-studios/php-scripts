<?php



/**
 * This class defines the structure of the 'bsfusn_articles' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.bookstore.map
 */
class BsfusnArticlesTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'bookstore.map.BsfusnArticlesTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('bsfusn_articles');
        $this->setPhpName('BsfusnArticles');
        $this->setClassname('BsfusnArticles');
        $this->setPackage('bookstore');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('article_id', 'ArticleId', 'SMALLINT', true, 8, null);
        $this->addColumn('article_cat', 'ArticleCat', 'SMALLINT', true, 8, 0);
        $this->addColumn('article_subject', 'ArticleSubject', 'VARCHAR', true, 200, '');
        $this->addColumn('article_snippet', 'ArticleSnippet', 'LONGVARCHAR', true, null, null);
        $this->addColumn('article_article', 'ArticleArticle', 'LONGVARCHAR', true, null, null);
        $this->addColumn('article_draft', 'ArticleDraft', 'BOOLEAN', true, 1, false);
        $this->addColumn('article_breaks', 'ArticleBreaks', 'CHAR', true, null, '');
        $this->addColumn('article_name', 'ArticleName', 'SMALLINT', true, 8, 1);
        $this->addColumn('article_datestamp', 'ArticleDatestamp', 'INTEGER', true, 10, 0);
        $this->addColumn('article_reads', 'ArticleReads', 'SMALLINT', true, 8, 0);
        $this->addColumn('article_allow_comments', 'ArticleAllowComments', 'BOOLEAN', true, 1, true);
        $this->addColumn('article_allow_ratings', 'ArticleAllowRatings', 'BOOLEAN', true, 1, true);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // BsfusnArticlesTableMap
