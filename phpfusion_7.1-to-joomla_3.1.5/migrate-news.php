<?php
// Include the main Propel script
require_once('runtime/lib/Propel.php');
// Initialize Propel with the runtime configuration
Propel::init(__DIR__ . "/bookstore-conf.php");
// Add the generated 'classes' directory to the include path
set_include_path(__DIR__ . "/project/classes" . PATH_SEPARATOR . get_include_path());
// Show all errors
error_reporting(E_ALL);
ini_set('display.errors', 'On');
ini_set('display_errors', 'On');

/**
 * Migration news from PHP-Fusion 7.1 to Joomla 3.1.5
 */

$last = 0;
$step = 10;

if(isset($_GET['start']) && is_numeric($_GET['start']))
	$last = $_GET['start'];

echo("<meta http-equiv=\"refresh\" content=\"3;url=?start=".($last + $step)."\" />");

$newsesQuery = new BsfusnNewsQuery();
$newses = 
$newsesQuery
->orderByNewsId()
->offset($last)
->limit($step)
->find();

$newsParent = 36;

$assertQuery = new Jm3AssetsQuery();

foreach($newses as $news){

	$jContent = new Jm3Content();

	$jContent
	->setAssetId(-1)
	->setTitle($news->getNewsSubject())
	->setAlias(strtolower(substr(preg_replace("/([a-z0-9]+)([^a-z0-9]+)?/i", "$1-", $news->getNewsSubject()), 0, -1)))
	->setIntrotext($news->getNewsNews())
	->setFulltext($news->getNewsExtended())
	->setState(1)
	->setCatid(8)
	->setCreated(date("Y-m-d H:i:s", $news->getNewsDatestamp()))
	->setCreatedBy(964)
	->setCreatedByAlias("")
	//->setModified("0000-00-00 00:00:00")
	->setModifiedBy(0)
	->setCheckedOut(0)
	//->setCheckedOutTime("0000-00-00 00:00:00")
	->setPublishUp(date("Y-m-d H:i:s", $news->getNewsDatestamp()))
	//->setPublishDown("0000-00-01 00:00:00")
	->setImages('{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}')
	->setUrls('{"urla":false,"urlatext":"","targeta":"","urlb":false,"urlbtext":"","targetb":"","urlc":false,"urlctext":"","targetc":""}')
	->setAttribs('{"show_title":"","link_titles":"","show_tags":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}')
	->setVersion(1)
	->setOrdering(0)
	->setMetakey("")
	->setMetadesc("")
	->setAccess(1)
	->setHits(1)
	->setMetadata("")
	->setFeatured(0)
	->setLanguage("*")
	->setXreference("");

	$jContent->save();

	if($jContent->getId() != null && $jContent->getId() > 0 && $jContent->getAssetId() == -1){
		$parentAssert = $assertQuery->findPK($newsParent);

		$assert = new Jm3Assets();
		
		$assert
		->setParentId($newsParent)
		->setLft($parentAssert->getRgt())
		->setRgt($parentAssert->getRgt()+1)
		->setLevel(3)
		->setName("com_content.article.".$jContent->getId())
		->setTitle($jContent->getTitle())
		->setRules('{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}');
		
		$orderAsserts = $assertQuery->filterByRgt(array("min" => $parentAssert->getRgt()+1))->find();
		
		foreach($orderAsserts as $orederAssert){
			if($orederAssert->getLft() > $parentAssert->getRgt())
				$orederAssert->setLft($orederAssert->getLft() + 2);
			$orederAssert->setRgt($orederAssert->getRgt() + 2);
			$orederAssert->save();
		}

		$assert->save();
		
		$parentAssert->setRgt($parentAssert->getRgt() + 2);
		$parentAssert->save();
	}

	echo($news->getNewsId()." -> ".$jContent->getId()."<br/>\n");
}